//
//  RegistrationController.m
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "RegistrationController.h"
#import "KeyboardToolbar.h"

@interface RegistrationController () <UITextFieldDelegate> {
    
    KeyboardToolbar *toolbar;
    
    UIDatePicker *datePicker;
    NSDateFormatter *dateFormatter;
        
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *passwordTextField;
    IBOutlet UITextField *passwordConfirmTextField;
    
    IBOutlet UITextField *lastNameTextField;
    IBOutlet UITextField *firstNameTextField;
    IBOutlet UITextField *middleNameTextField;
    IBOutlet UITextField *birthdayTextField;
}

@end

@implementation RegistrationController

- (void)dealloc {
    // отписываемся от уведомлений
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Registration";
    self.navigationController.navigationBar.topItem.title = @"";
 
    // настроили KeyboardToolbar
    toolbar = [KeyboardToolbar create];
    toolbar.textFields = @[emailTextField, passwordTextField, passwordConfirmTextField, firstNameTextField, lastNameTextField, middleNameTextField, birthdayTextField];
    
    // подписались на уведомления от клавиатуры
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    // создание datePicker и настройка dateFormatter
    datePicker = [UIDatePicker new];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.date = [NSDate dateWithTimeIntervalSince1970:946684800];
    [datePicker addTarget:self action:@selector(datePickerDidChangeValue:) forControlEvents:UIControlEventValueChanged];
    birthdayTextField.inputView = datePicker;
    
    dateFormatter = [NSDateFormatter new];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // отображаем навигейшен бар с анимацией!!!
    [self.navigationController setNavigationBarHidden:NO
                                             animated:YES];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Actions

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)registerButtonClicked:(id)sender {
    
}

- (void)datePickerDidChangeValue:(id)sender {
    birthdayTextField.text = [dateFormatter stringFromDate:datePicker.date];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [toolbar returnButtonClicked];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    toolbar.currentTextField = textField;
    if (textField == birthdayTextField) {
        birthdayTextField.text = [dateFormatter stringFromDate:datePicker.date];
    }
}

#pragma mark - Notification

- (void)keyboardWillHide:(NSNotification *)notification {
    scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)keyboardWillChangeFrame:(NSNotification *)notification {
    
    NSValue *value = [notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect endRect = value.CGRectValue;
    
    scrollView.contentInset = UIEdgeInsetsMake(20, 0, endRect.size.height + 20, 0);
}

@end
