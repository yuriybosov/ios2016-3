//
//  KeyboardToolbar.h
//  lesson_41_SrcollView_TextField
//
//  Created by Yurii Bosov on 4/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardToolbar : UIToolbar

@property (nonatomic, weak) IBOutlet UIBarButtonItem *prevButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *nextButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButton;

@property (nonatomic, strong) NSArray <UITextField *> *textFields;
@property (nonatomic, strong) UITextField *currentTextField;

+ (instancetype)create;

- (void)returnButtonClicked;

@end
