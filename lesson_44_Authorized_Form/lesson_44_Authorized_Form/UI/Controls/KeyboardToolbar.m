//
//  KeyboardToolbar.m
//  lesson_41_SrcollView_TextField
//
//  Created by Yurii Bosov on 4/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "KeyboardToolbar.h"

@implementation KeyboardToolbar

+ (instancetype)create {
    return [[NSBundle mainBundle] loadNibNamed:@"KeyboardToolbar" owner:nil options:nil].firstObject;
}

- (void)setCurrentTextField:(UITextField *)currentTextField {
    _currentTextField = currentTextField;
    if (_currentTextField == _textFields.lastObject) {
        _nextButton.enabled = NO;
    } else {
        _nextButton.enabled = YES;
    }
    
    
    if (_currentTextField == _textFields.firstObject) {
        _prevButton.enabled = NO;
    } else{
        _prevButton.enabled = YES;
    }
}

- (void)setTextFields:(NSArray<UITextField *> *)textFields {
    _textFields = textFields;
    for (UITextField *textField in _textFields) {
        textField.inputAccessoryView = self;
        
        if (textField != _textFields.lastObject) {
            textField.returnKeyType = UIReturnKeyNext;
        } else {
            textField.returnKeyType = UIReturnKeyDone;
        }
    }
}

- (void)returnButtonClicked {
    [self nextButtonCliecked:nil];
}

#pragma mark - Action

- (IBAction)prevButtonCliecked:(id)sender {
    // получаем индекс текущего поля ввода
    NSUInteger index = [_textFields indexOfObject:_currentTextField];
    // проверяем что индекс валидный и что он не указывает на первый элемент
    if (index != NSNotFound &&
        index > 0) {
        [[_textFields objectAtIndex:index - 1] becomeFirstResponder];
    }
}

- (IBAction)nextButtonCliecked:(id)sender {
    // получаем индекс текущего поля ввода
    NSUInteger index = [_textFields indexOfObject:_currentTextField];
    // проверяем что индекс валидный и что он указывает не на послдений элемент
    if (index != NSNotFound &&
        index < _textFields.count - 1) {
        
        [[_textFields objectAtIndex:index + 1] becomeFirstResponder];
    } else {
        [_currentTextField resignFirstResponder];
    }
}

- (IBAction)doneCliecked:(id)sender {
    // по нажатию на кнопку done вызываем метод скрытия клавиатура у текущего активного поля ввода
    [_currentTextField resignFirstResponder];
}

@end
