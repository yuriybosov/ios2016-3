//
//  ForgotController.m
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ForgotController.h"
#import "UIConfigurator.h"

@interface ForgotController () <UITextFieldDelegate> {
    IBOutlet UITextField *emailTextField;
}

@end

@implementation ForgotController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Forgot Password";
    self.navigationController.navigationBar.topItem.title = @"";
    
    emailTextField.text = self.email;
    
    [UIConfigurator addErrorViewForTextField:emailTextField];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO
                                             animated:YES];

}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Actions

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)sendButtonClicked:(id)sender {
    [self validateForm];
}

- (void)popController {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Validations

- (void)validateForm {
    if (emailTextField.text.length > 0) {
        NSLog(@"%@", emailTextField.text);
        [self performSelector:@selector(popController) withObject:nil afterDelay:1];
    } else {
        emailTextField.rightViewMode = UITextFieldViewModeAlways;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self validateForm];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.rightViewMode = UITextFieldViewModeNever;
}

@end
