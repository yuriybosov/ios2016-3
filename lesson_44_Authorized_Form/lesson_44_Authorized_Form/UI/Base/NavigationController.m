//
//  NavigationController.m
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NavigationController.h"

@implementation NavigationController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end
