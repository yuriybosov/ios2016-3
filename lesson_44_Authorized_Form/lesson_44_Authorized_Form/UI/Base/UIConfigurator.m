//
//  UIConfigurator.m
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "UIConfigurator.h"

@implementation UIConfigurator

+ (void)addErrorViewForTextField:(UITextField *)textField {
    UIImage *img = [UIImage imageNamed:@"errorIcon"];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.frame = CGRectMake(0, 0, 40, 40);
    textField.rightView = imgView;
    textField.rightViewMode = UITextFieldViewModeNever;
}

@end
