//
//  UIConfigurator.h
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIConfigurator : NSObject

+ (void)addErrorViewForTextField:(UITextField *)textField;

@end
