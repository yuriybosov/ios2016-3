//
//  ProfileController.m
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/30/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ProfileController.h"

@implementation ProfileController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.title = @"Профиль";
}

#pragma mark - Action

- (IBAction)logoutButtonClicked:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Authorization" bundle:nil];
    UIViewController *controller = [storyboard instantiateInitialViewController];
    [UIApplication sharedApplication].keyWindow.rootViewController = controller;
}

@end
