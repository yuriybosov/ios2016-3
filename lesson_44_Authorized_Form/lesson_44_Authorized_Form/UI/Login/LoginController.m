//
//  LoginController.m
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "LoginController.h"
#import "UIConfigurator.h"
#import "ForgotController.h"

@interface LoginController () <UITextFieldDelegate> {
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *passwordTextField;
}

@end

@implementation LoginController

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // прячем навигейшен бар
    self.navigationController.navigationBarHidden = YES;
    
    //
    [UIConfigurator addErrorViewForTextField:emailTextField];
    [UIConfigurator addErrorViewForTextField:passwordTextField];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // прячем навигейшен бар с анимацией!!!
    [self.navigationController setNavigationBarHidden:YES
                                             animated:YES];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Actions

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)loginButtonClicked:(id)sender {
//    // после успешной валидации нужно выполнить переход на новый экран. в нашем случае это tabbar и мы полностью заменяем root contollet у app window
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Tabbar" bundle:nil];
    UIViewController *controller = [storyboard instantiateInitialViewController];
    [UIApplication sharedApplication].keyWindow.rootViewController = controller;
}

- (void)test:(UIViewController *)controller {
    [UIApplication sharedApplication].keyWindow.rootViewController = controller;
}

#pragma mark - Navigations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[ForgotController class]]) {
        
        ((ForgotController *)segue.destinationViewController).email = emailTextField.text;
    }
}

@end
