//
//  AppDelegate.h
//  lesson_44_Authorized_Form
//
//  Created by Yurii Bosov on 5/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

