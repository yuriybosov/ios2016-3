//
//  AppDelegate.h
//  lesson_73_GoogleMap_Route
//
//  Created by Yurii Bosov on 9/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

