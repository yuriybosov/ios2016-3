//
//  ViewController.m
//  lesson_73_GoogleMap_Route
//
//  Created by Yurii Bosov on 9/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <AFNetworking.h>
#import <MBProgressHUD.h>

@interface ViewController () <GMSMapViewDelegate>

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.delegate = self;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.myLocationEnabled = YES;
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    
    if (mapView.myLocation) {
        [self getRoadFrom:mapView.myLocation.coordinate
                       to:coordinate];
    }
}

#pragma mark - Fetch Data

- (void)getRoadFrom:(CLLocationCoordinate2D)origin
                 to:(CLLocationCoordinate2D)destination {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *originCoordinates = [NSString stringWithFormat:@"%f,%f", origin.latitude, origin.longitude];
    
    NSString *destinationCoordinates = [NSString stringWithFormat:@"%f,%f", destination.latitude, destination.longitude];
    
    __weak typeof(self) weakSelf = self;
    
    [[AFHTTPSessionManager manager] GET:@"https://maps.googleapis.com/maps/api/directions/json" parameters:@{@"origin":originCoordinates, @"destination":destinationCoordinates, @"key":@"AIzaSyCDZyMcAOqdImPE3DwztIoiVzfimYj3QVo"} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [weakSelf getRoadDidSuccess:responseObject];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [weakSelf getRoadDidFailure:error.localizedDescription];
        
    }];
}

- (void)getRoadDidSuccess:(NSDictionary *)data {
    
    [self.mapView clear];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([data isKindOfClass:[NSDictionary class]]) {
        NSArray *routes = data[@"routes"];
        NSDictionary *routData = routes.firstObject;
        
        //
        NSString *points = [routData valueForKeyPath:@"overview_polyline.points"];
        if (points) {
            GMSPath *path = [GMSPath pathFromEncodedPath:points];
            GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
            polyline.map = self.mapView;
        }
    }
        
}

- (void)getRoadDidFailure:(NSString *)errorMessage {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSLog(@"%@", errorMessage);
}

@end
