//
//  ATMCell.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ATMCell.h"

@implementation ATMCell

- (void)setModel:(ATMModel *)model {
    _model = model;
    
    self.address.text = _model.fullAddressRu;
}

@end
