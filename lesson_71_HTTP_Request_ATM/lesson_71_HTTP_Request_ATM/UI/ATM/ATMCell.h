//
//  ATMCell.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATMModel.h"

@interface ATMCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *address;

@property (nonatomic, strong) ATMModel *model;

@end
