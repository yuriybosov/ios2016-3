//
//  ATMDetailedController.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "BaseViewController.h"
#import "ATMModel.h"

@interface ATMDetailedController : BaseViewController

@property (nonatomic, strong) ATMModel *model;

@end
