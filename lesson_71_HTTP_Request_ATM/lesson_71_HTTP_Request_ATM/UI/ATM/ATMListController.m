//
//  ATMListController.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ATMListController.h"
#import "ATMCell.h"
#import "ATMDetailedController.h"
#import <MBProgressHUD.h>

@interface ATMListController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate> {
    NSArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@end

@implementation ATMListController

- (void)setup {
    self.title = @"ATM";
    self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"ATM" image:nil tag:0];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchBar.text = @"Днепр";
    [self fetchATMForCity:self.searchBar.text];
}

#pragma mark - FetchData

- (void)fetchATMForCity:(NSString *)cityName {
    // show activity indicator
    [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // start request
    
    __weak typeof(self) weakSelf = self;
    
    [ATMModel loadDataForCity:cityName complitionBlock:^(id data, NSString *errorMessage) {
        // пример как правильно использовать self в блоке
//        __strong typeof(self) strongSelf = weakSelf;
//        strongSelf->dataSource = data;
        
        // но лучше написать отдельный метод!!!
        [weakSelf fetchATMComplitedWith:data
                           errorMessage:errorMessage];
    }];
}

- (void)fetchATMComplitedWith:(NSArray *)data
                 errorMessage:(NSString *)message {
    dataSource = data;
    [self.tableView reloadData];
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.navigationController.view];
    // показать ошибку, если она была
    if (message) {

        hud.mode = MBProgressHUDModeText;
        hud.label.text = message;
        [hud hideAnimated:YES afterDelay:3];

    } else {
        if (dataSource.count) {
            [hud hideAnimated:YES];
        } else {
            hud.mode = MBProgressHUDModeText;
            hud.label.text = @"Ничего не найдено";
            [hud hideAnimated:YES afterDelay:3];
        }
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // open ATM detailed controller
    if ([sender isKindOfClass:[ATMCell class]] &&
        [segue.destinationViewController isKindOfClass:[ATMDetailedController class]]) {
        
        ((ATMDetailedController *)segue.destinationViewController).model = ((ATMCell *)sender).model;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ATMCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ATMCell"];
    cell.model = dataSource[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)  indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // start request
    [self fetchATMForCity:searchBar.text];
    
    // hide keyboard
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text = nil;
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:NO animated:YES];
}

@end
