//
//  ATMDetailedController.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ATMDetailedController.h"

@interface ATMDetailedController ()

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end

@implementation ATMDetailedController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.model.placeRu;
    
    // 1. создаем UIStackView для контента
    UIStackView *stackView = [[UIStackView alloc] init];
    stackView.axis = UILayoutConstraintAxisVertical;
    stackView.spacing = 20;
    stackView.layoutMargins = UIEdgeInsetsMake(20, 20, 20, 20);
    stackView.layoutMarginsRelativeArrangement = YES;

    // обязательно нужно добавить stackView на скролл до того, как мы начнем настраивать констрейты
    [self.scrollView addSubview:stackView];
    
    // настраиваем контстренты
    // выключаем авторисайзные макси
    stackView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // top constaint
    [NSLayoutConstraint constraintWithItem:stackView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeTop multiplier:1 constant:0].active = YES;
    
    // left constaint
    [NSLayoutConstraint constraintWithItem:stackView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeLeading multiplier:1 constant:0].active = YES;
    
    // bottom constaint
    [NSLayoutConstraint constraintWithItem:stackView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeBottom multiplier:1 constant:0].active = YES;
    
    // right constaint
    [NSLayoutConstraint constraintWithItem:stackView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeTrailing multiplier:1 constant:0].active = YES;
    
    // equal widht
    [NSLayoutConstraint constraintWithItem:stackView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeWidth multiplier:1 constant:0].active = YES;
    
    
    // 2. если у ATM есть места, выведим это место в виде UILabel и добавим его в stackView
    if (self.model.placeRu.length > 0) {
        UILabel *label = [UILabel new];
        label.text = self.model.placeRu;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont systemFontOfSize:24];
        label.numberOfLines = 0;
        [stackView addArrangedSubview:label];
    }
    
    // 3. если у ATM есть адрес, выведим этот адрес в виде UILabel и добавим его в stackView
    if (self.model.fullAddressRu.length > 0) {
        UILabel *label = [UILabel new];
        label.text = self.model.fullAddressRu;
        label.textColor = [UIColor grayColor];
        label.font = [UIFont systemFontOfSize:20];
        label.numberOfLines = 0;
        [stackView addArrangedSubview:label];
    }
    
    // 4. если у ATM есть время работы, выведим время работы в виде UILabel и добавим его в stackView
    NSString *timeString = [self.model.timeWork timeWorkString];
    if (timeString.length > 0) {
        UILabel *label = [UILabel new];
        label.text = timeString;
        label.textColor = [UIColor lightGrayColor];
        label.font = [UIFont systemFontOfSize:20];
        label.numberOfLines = 0;
        [stackView addArrangedSubview:label];
    }
}

@end
