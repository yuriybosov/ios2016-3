//
//  ServiceCell.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostamatsServiceModel.h"

@interface ServiceCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *serviceLogo;
@property (nonatomic, weak) IBOutlet UILabel *serviceTitle;

@property (nonatomic, strong) PostamatsServiceModel *model;

@end
