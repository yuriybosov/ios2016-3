//
//  ServiceCell.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ServiceCell.h"
#import <UIImageView+AFNetworking.h>

@implementation ServiceCell

- (void)setModel:(PostamatsServiceModel *)model {
    _model = model;
    
    self.serviceTitle.text = _model.title;
    self.serviceLogo.image = nil;
    [self.serviceLogo setImageWithURL:[NSURL URLWithString:_model.imagePath]];
}

@end
