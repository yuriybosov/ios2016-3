//
//  PostamatsListController.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PostamatsListController.h"
#import "PostamatsModel.h"

@interface PostamatsListController ()

@end

@implementation PostamatsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // TEST LOAD DATA
    [PostamatsModel loadDataFromServiceId:self.service.ID withComplitionBlock:^(id data, NSString *errorMessage) {
        
        NSLog(@"data %@", data);
        NSLog(@"error %@", errorMessage);
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

@end
