//
//  PostamatsServiceController.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PostamatsServiceController.h"
#import "PostamatsServiceModel.h"
#import "ServiceCell.h"
#import <MBProgressHUD.h>
#import "PostamatsListController.h"
#import <SVPullToRefresh.h>

@interface PostamatsServiceController () <UITableViewDataSource, UITableViewDelegate> {
    NSURLSessionDataTask *currentDataTask;
    NSArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation PostamatsServiceController

- (void)setup {
    self.title = @"Service List";
    self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Postamats" image:nil tag:0];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchData];
    
    __weak typeof(self) weakSelf = self;
    [self.tableView addPullToRefreshWithActionHandler:^{
        [weakSelf fetchData];
    }];
}

#pragma mark - Fetch Data

- (void)fetchData {
    
    if (currentDataTask == nil) {
        
        [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        __weak typeof(self) weakSelf = self;
        currentDataTask = [PostamatsServiceModel loadDataWithComplitionBlock:^(id data, NSString *errorMessage) {
            
            [weakSelf fetchDataComplitedWithData:data
                                    errorMessage:errorMessage];
        }];
    }
}

- (void)fetchDataComplitedWithData:(NSArray *)data
                      errorMessage:(NSString *)errorMessage {
    
    currentDataTask = nil;
    [self.tableView.pullToRefreshView stopAnimating];
    
    if (errorMessage) {
        
        MBProgressHUD *hud = [MBProgressHUD HUDForView:self.navigationController.view];
        hud.mode = MBProgressHUDModeText;
        hud.label.text = errorMessage;
        [hud hideAnimated:YES afterDelay:3];
        
    } else {
        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        dataSource = data;
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceCell"];
    cell.model = dataSource[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(ServiceCell *)sender {
    
    if ([segue.destinationViewController isKindOfClass:[PostamatsListController class]] && [sender isKindOfClass:[ServiceCell class]]) {
        
        ((PostamatsListController *)segue.destinationViewController).service = sender.model;
    }
}

@end
