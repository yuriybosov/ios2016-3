//
//  NavigationController.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NavigationController.h"

@implementation NavigationController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end
