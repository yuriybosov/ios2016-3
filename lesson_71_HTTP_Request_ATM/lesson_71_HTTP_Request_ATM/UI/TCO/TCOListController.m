//
//  TCOListController.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TCOListController.h"
#import "TCOModel.h"

@interface TCOListController ()

@end

@implementation TCOListController

- (void)setup {
    self.title = @"TCO";
    self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"TCO" image:nil tag:0];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [TCOModel loadDataForCity:@"Бердянск" complitionBlock:^(id data, NSString *errorMessage) {
        
        NSLog(@"errorMessage %@", errorMessage);
        NSLog(@"data %@", data);
        
    }];
}

@end
