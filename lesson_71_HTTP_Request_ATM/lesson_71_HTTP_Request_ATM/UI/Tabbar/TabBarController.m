//
//  TabBarController.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TabBarController.h"

@implementation TabBarController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
