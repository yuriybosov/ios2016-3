//
//  TCOModel.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TCOModel.h"
#import "AFNetworking.h"

@implementation TCOModel

+ (void)loadDataForCity:(NSString *)cityName complitionBlock:(URLComplitionBlock)complitionBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/infrastructure?json&tso", kBaseURL];
    
    [[AFHTTPSessionManager manager] GET:path parameters:@{@"city":cityName} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *error = nil;
        NSArray *result = [TCOModel arrayOfModelsFromDictionaries:responseObject[@"devices"] error:&error];
        
        if (complitionBlock) {
            complitionBlock(result, error.localizedDescription);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (complitionBlock) {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

@end
