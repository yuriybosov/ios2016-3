//
//  PostamatsModel.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PostamatsModel.h"
#import <AFNetworking.h>

@implementation PostamatsModel

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:
            @{@"ID":@"id",
              @"branch":@"branch",
              @"street":@"address_street",
              @"address":@"address",
              @"latitude":@"point.latitude",
              @"longitude":@"point.longitude"}];
}

+ (NSURLSessionDataTask *)loadDataFromServiceId:(NSString *)serviceId withComplitionBlock:(URLComplitionBlock)complitionBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/GetPostamatsByServiceId", kBasePostamatURL];
    
    return [[AFHTTPSessionManager manager] GET:path parameters:@{@"fmt":@"json",@"service":serviceId} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        NSError *error = nil;
        NSArray *result = [PostamatsModel arrayOfModelsFromDictionaries:responseObject[@"postamats"] error:&error];
        if(error) {
            if (complitionBlock) {
                complitionBlock(nil, error.localizedDescription);
            }
        } else {
            if (complitionBlock) {
                complitionBlock(result, nil);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (complitionBlock) {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

@end
