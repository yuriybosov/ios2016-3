//
//  PostamatsServiceModel.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "Constants.h"

@interface PostamatsServiceModel : JSONModel

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString <Optional> *title;
@property (nonatomic, strong) NSString <Optional> *imagePath;

+ (NSURLSessionDataTask *)loadDataWithComplitionBlock:(URLComplitionBlock)complitionBlock;

@end
