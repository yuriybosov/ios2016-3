//
//  PostamatsModel.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "Constants.h"

@interface PostamatsModel : JSONModel

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *branch;
@property (nonatomic, strong) NSString *street;
@property (nonatomic, strong) NSString *address;

@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) float longitude;

+ (NSURLSessionDataTask *)loadDataFromServiceId:(NSString *)serviceId withComplitionBlock:(URLComplitionBlock)complitionBlock;

@end
