//
//  PostamatsServiceModel.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "PostamatsServiceModel.h"
#import <AFNetworking.h>

@implementation PostamatsServiceModel

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"ID":@"id", @"title":@"title", @"imagePath":@"img"}];
}

+ (NSURLSessionDataTask *)loadDataWithComplitionBlock:(URLComplitionBlock)complitionBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/GetActiveServices", kBasePostamatURL];
    
    return [[AFHTTPSessionManager manager] GET:path parameters:@{@"fmt":@"json"} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSError *error = nil;
        NSArray *result = [PostamatsServiceModel arrayOfModelsFromDictionaries:responseObject[@"services"] error:&error];
        if(error) {
            if (complitionBlock) {
                complitionBlock(nil, error.localizedDescription);
            }
        } else {
            if (complitionBlock) {
                complitionBlock(result, nil);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (complitionBlock) {
            complitionBlock(nil, error.localizedDescription);
        }
    }];
}

@end
