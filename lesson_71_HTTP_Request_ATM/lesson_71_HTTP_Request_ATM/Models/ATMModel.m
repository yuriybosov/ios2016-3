//
//  ATMModel.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ATMModel.h"

@implementation ATMModel

+ (void)loadDataForCity:(NSString *)cityName
        complitionBlock:(URLComplitionBlock)complitionBlock {
    
    NSString *URLString = [NSString  stringWithFormat:@"%@/infrastructure?json&atm&city=%@", kBaseURL, [cityName stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLHostAllowedCharacterSet]]];
    
    NSURL *URL = [NSURL URLWithString:URLString];
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:URL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSString *errorMessage = error.localizedDescription;
        NSMutableArray *result = nil;
        
        if (data) {
            NSError *jsonError = nil;
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            if (jsonError) {
                errorMessage = jsonError.localizedDescription;
            } else {
                NSArray *devices = jsonData[@"devices"];
                result = [NSMutableArray new];
                
                for (NSDictionary *modelData in devices) {
                    [result addObject:[[ATMModel alloc] initWithDictionary:modelData]];
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (complitionBlock)
                complitionBlock(result, errorMessage);
        });
    }];
    
    [task resume];
}

- (instancetype)initWithDictionary:(NSDictionary *)data{
    self = [super init];
    if (self) {
        
        id value = data[@"type"];
        if ([value isKindOfClass:[NSString class]]) {
            self.type = value;
        }
        
        value = data[@"cityRU"];
        if ([value isKindOfClass:[NSString class]]) {
            self.cityRU = value;
        }
        
        value = data[@"fullAddressRu"];
        if ([value isKindOfClass:[NSString class]]) {
            self.fullAddressRu = value;
        }
        
        value = data[@"placeRu"];
        if ([value isKindOfClass:[NSString class]]) {
            self.placeRu = value;
        }
        
        value = data[@"latitude"];
        if ([value isKindOfClass:[NSString class]]) {
            self.latitude = @([value doubleValue]);
        }
        
        value = data[@"longitude"];
        if ([value isKindOfClass:[NSString class]]) {
            self.longitude = @([value doubleValue]);
        }
        
        value = data[@"tw"];
        if ([value isKindOfClass:[NSDictionary class]]){
            self.timeWork = [[TimeWorkModel alloc] initWithDictionary:value];
        }
    }
    return self;
}

@end
