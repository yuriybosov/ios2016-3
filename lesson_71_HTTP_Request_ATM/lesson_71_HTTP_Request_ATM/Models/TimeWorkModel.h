//
//  TimeWorkModel.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface TimeWorkModel : JSONModel

@property (nonatomic, strong) NSString *mon;
@property (nonatomic, strong) NSString *tue;
@property (nonatomic, strong) NSString *wed;
@property (nonatomic, strong) NSString *thu;
@property (nonatomic, strong) NSString *fri;
@property (nonatomic, strong) NSString *sat;
@property (nonatomic, strong) NSString *sun;
@property (nonatomic, strong) NSString *hol;

- (instancetype)initWithDictionary:(NSDictionary *)data;

- (NSString *)timeWorkString;

@end
