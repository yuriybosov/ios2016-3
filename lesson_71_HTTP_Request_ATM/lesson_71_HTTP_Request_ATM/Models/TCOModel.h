//
//  TCOModel.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

//#import <JSONModel/JSONModel.h>
#import "Constants.h"
#import "TimeWorkModel.h"
#import <CoreGraphics/CoreGraphics.h>

@interface TCOModel : JSONModel

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *cityRU;
@property (nonatomic, strong) NSString <Optional> *fullAddressRu;
@property (nonatomic, strong) NSString <Optional> *placeRu;

@property (nonatomic, assign) CGFloat latitude;
@property (nonatomic, assign) CGFloat longitude;

@property (nonatomic, strong) TimeWorkModel* tw;

+ (void)loadDataForCity:(NSString *)cityName
        complitionBlock:(URLComplitionBlock)complitionBlock;

@end
