//
//  TimeWorkModel.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TimeWorkModel.h"

@implementation TimeWorkModel

- (instancetype)initWithDictionary:(NSDictionary *)data {
    self = [super init];
    if (self) {
        
        id value = data[@"mon"];
        if ([value isKindOfClass:[NSString class]]) {
            self.mon = value;
        }
        
        value = data[@"tue"];
        if ([value isKindOfClass:[NSString class]]) {
            self.tue = value;
        }
        
        value = data[@"wed"];
        if ([value isKindOfClass:[NSString class]]) {
            self.wed = value;
        }
        
        value = data[@"thu"];
        if ([value isKindOfClass:[NSString class]]) {
            self.thu = value;
        }
        
        value = data[@"fri"];
        if ([value isKindOfClass:[NSString class]]) {
            self.fri = value;
        }
        
        value = data[@"sat"];
        if ([value isKindOfClass:[NSString class]]) {
            self.sat = value;
        }
        
        value = data[@"sun"];
        if ([value isKindOfClass:[NSString class]]) {
            self.sun = value;
        }
        
        value = data[@"hol"];
        if ([value isKindOfClass:[NSString class]]) {
            self.hol = value;
        }
    }
    return self;
}

- (NSString *)timeWorkString {
    return [NSString stringWithFormat:@"Время работы:\nпн.\t\t%@\nвт.\t\t%@\nср.\t\t%@\nчт.\t\t%@\nпт.\t\t%@\nсб.\t\t%@\nвс.\t\t%@", _mon, _tue, _wed, _thu, _fri, _sat, _sun];
}

@end
