//
//  ATMModel.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "TimeWorkModel.h"

@interface ATMModel : NSObject

@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *cityRU;
@property (nonatomic, strong) NSString *fullAddressRu;
@property (nonatomic, strong) NSString *placeRu;

@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;

@property (nonatomic, strong) TimeWorkModel* timeWork;

+ (void)loadDataForCity:(NSString *)cityName
        complitionBlock:(URLComplitionBlock)complitionBlock;

- (instancetype)initWithDictionary:(NSDictionary *)data;

@end
