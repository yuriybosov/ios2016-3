//
//  DateFormatter.m
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 9/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "DateFormatter.h"

@implementation DateFormatter

+ (NSDateFormatter *)ddMMyyyy {
    static NSDateFormatter* formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSDateFormatter new];
        formatter.dateFormat = @"dd.MM.yyyy";
    });
    
    return formatter;
}

+ (NSDateFormatter *)yyyy {
    static NSDateFormatter* formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSDateFormatter new];
        formatter.dateFormat = @"yyyy";
    });
    
    return formatter;
}


@end
