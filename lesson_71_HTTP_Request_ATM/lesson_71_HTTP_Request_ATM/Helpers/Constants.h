//
//  Constants.h
//  lesson_71_HTTP_Request_ATM
//
//  Created by Yurii Bosov on 8/27/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef Constants_h
#define Constants_h


typedef void(^URLComplitionBlock)(id data, NSString *errorMessage);


static NSString *const kBaseURL = @"https://api.privatbank.ua/p24api";
static NSString *const kBaseBuhURL = @"https://api.buh.privatbank.ua";
static NSString *const kBasePostamatURL = @"https://bpk-postamat.privatbank.ua/api";

#endif /* Constants_h */
