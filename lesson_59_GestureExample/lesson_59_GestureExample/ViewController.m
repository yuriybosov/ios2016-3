//
//  ViewController.m
//  lesson_59_GestureExample
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate> {
    IBOutlet UIView *rect;
    
    IBOutlet UIPanGestureRecognizer *pan;
    IBOutlet UIPinchGestureRecognizer *pich;
    IBOutlet UIRotationGestureRecognizer *rotation;
}

@end

@implementation ViewController

#pragma mark - Recognizer Actions

- (IBAction)doubleTap:(UITapGestureRecognizer *)sender {
    // сбросили все трансформации (вернули объект в начальное положение)
    rect.transform = CGAffineTransformMake(1, 0, 0, 1, 0, 0);
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    
    CGPoint point = [sender translationInView:rect];
    CGAffineTransform transform = CGAffineTransformTranslate(rect.transform, point.x, point.y);
    rect.transform = transform;
    [sender setTranslation:CGPointZero inView:rect];
}

- (IBAction)rotate:(UIRotationGestureRecognizer *)sender {
    CGAffineTransform transform = CGAffineTransformRotate(rect.transform, sender.rotation);
    sender.rotation = 0;
    rect.transform = transform;
}

- (IBAction)pich:(UIPinchGestureRecognizer *)sender {
    CGAffineTransform transform = CGAffineTransformScale(rect.transform, sender.scale, sender.scale);
    
    CGFloat newScale = sqrt(pow(transform.a, 2) + pow(transform.c, 2));
    if (newScale >= 1) {
        rect.transform = transform;
        sender.scale = 1;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}

@end
