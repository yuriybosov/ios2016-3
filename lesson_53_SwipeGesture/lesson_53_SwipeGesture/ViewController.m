//
//  ViewController.m
//  lesson_53_SwipeGesture
//
//  Created by Yurii Bosov on 6/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
    __block BOOL isAnimate;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // пример добавления жеста в коде
    // левый свайт
    // 1. создали жест
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    // 2. задали направление
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    // 3. добавляем жест на нужную нам вью
    [self.view addGestureRecognizer:leftSwipe];
    
    // правый свайп
    // 1. создали жест
    UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    // 2. задали направление
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    // 3. добавляем жест на нужную нам вью
    [self.view addGestureRecognizer:rightSwipe];
}

#pragma mark - Actions

- (IBAction)didSwipe:(UISwipeGestureRecognizer *)sender {
    
    if (isAnimate) {
        return;
    }
    
    CGRect frame = rect.frame;
    
    switch (sender.direction) {
        
        case UISwipeGestureRecognizerDirectionLeft:
            frame.origin.x = 0;
            break;
            
        case UISwipeGestureRecognizerDirectionRight:
            frame.origin.x = self.view.frame.size.width - rect.frame.size.width;
            break;
        
        case UISwipeGestureRecognizerDirectionUp:
            frame.origin.y = 0;
            break;
            
        case UISwipeGestureRecognizerDirectionDown:
            frame.origin.y = self.view.frame.size.height - rect.frame.size.height;
            break;
    }
    
    isAnimate = YES;
    [UIView animateWithDuration:0.7 animations:^{
        rect.frame = frame;
    } completion:^(BOOL finished) {
        isAnimate = NO;
    }];
}

@end
