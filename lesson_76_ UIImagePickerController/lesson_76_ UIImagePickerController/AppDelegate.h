//
//  AppDelegate.h
//  lesson_76_ UIImagePickerController
//
//  Created by Yurii Bosov on 10/8/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

