//
//  ClassB.h
//  lesson_13_Property_Attributes
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ClassA;

@interface ClassB : NSObject

@property (nonatomic, weak) ClassA *propertyA;

@end
