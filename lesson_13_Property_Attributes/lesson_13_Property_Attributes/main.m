//
//  main.m
//  lesson_13_Property_Attributes
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassA.h"
#import "ClassB.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        ClassA *obj1 = [ClassA new];
        obj1.property1 = @"text text";
        obj1.property2 = @"text text text text";
        [obj1 setProperty2:@"qwerqwerqwer"];
        
        NSLog(@"%@", [obj1 property2]);
        
        ClassB *obj2 = [ClassB new];
        NSLog(@"%@", obj2);
        
        
        obj1.propertyB = obj2;
        obj2.propertyA = obj1;
    }
    return 0;
}
