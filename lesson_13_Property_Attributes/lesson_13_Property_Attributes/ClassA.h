//
//  ClassA.h
//  lesson_13_Property_Attributes
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ClassB;

@interface ClassA : NSObject

// атрибуты для "сложных" типов:
// strong - сильная ссылка, доступна чтение и запись
@property (nonatomic, strong) NSString *property1;
// weak - слабая ссылка, доступна чтение и запись
@property (nonatomic, weak) NSString *property2;
// readonly - свойство доступно только для чтения
@property (nonatomic, readonly) NSString *property3;

// атрибуты для "простых" типов
// assign - доступна чтение и запись
@property (nonatomic, assign) CGFloat property4;
// readonly - доступно только для чтения
@property (nonatomic, readonly) BOOL property5;

@property (nonatomic, strong) ClassB *propertyB;

@end
