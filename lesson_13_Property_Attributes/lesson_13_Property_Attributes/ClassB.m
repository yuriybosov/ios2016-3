//
//  ClassB.m
//  lesson_13_Property_Attributes
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ClassB.h"
#import "ClassA.h"

@implementation ClassB

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
