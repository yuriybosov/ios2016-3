//
//  ClassA.m
//  lesson_13_Property_Attributes
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ClassA.h"
#import "ClassB.h"

@implementation ClassA

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// нельзя прееопределить setter и getter для одного и того же свойства!!!
// по этому, setter переопределим для свойства property1, а getter для свойства property2

// переопределение settera
- (void)setProperty1:(NSString *)property1 {
    NSLog(@"изменение значение для свойства property1");
    _property1 = property1;
}

// переопределение gettera
- (NSString *)property2 {
    NSLog(@"возвращаем значение для свойства property2");
    return _property2;
}

@end
