//
//  ViewController.m
//  lesson_74_AppleMap
//
//  Created by Yurii Bosov on 10/1/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>

@interface ViewController () <MKMapViewDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    
    // добавим пин на карту
    MKPointAnnotation *point = [MKPointAnnotation new];
    point.coordinate = CLLocationCoordinate2DMake(48.4638695, 35.04795);
    point.title = @"Dnipro";
    point.subtitle = @"my city";
    
    [self.mapView addAnnotation:point];
    
    // выполним запрос на разрешение использования координат (трекать локацию пользователя)
    // 1. проверим что геосервис включен на телефоне
    if ([CLLocationManager locationServicesEnabled]) {
        
        // 2. узнаем текущий статус авторизации на использовании геоданных для приложения
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        switch (status) {
            case kCLAuthorizationStatusNotDetermined:
                // приложение еще не запрашивало разрешение не использование локации
                // выполнним запрос на разрешение использоваться геоданные в случае, когда приложение активно
                [locationManager requestWhenInUseAuthorization];
                break;
                
            case kCLAuthorizationStatusAuthorizedAlways:
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                // пользователь разрешил трекать его локацию
                // можно "запустить" локейшен менеджер
                [locationManager startUpdatingLocation];
                break;
                
            case kCLAuthorizationStatusDenied:
            case kCLAuthorizationStatusRestricted:
                // show alert c напоминаем пользователю что он запретил трекать его локацию
                
                break;
        }
    } else {
        // show alert c напоминаем что геослужба отключена
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"геослужба отключена на вашем телефоне" preferredStyle:UIAlertControllerStyleAlert];
        
        [controller addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        
        [controller addAction:[UIAlertAction actionWithTitle:@"Настройки" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            // open phone settings
        }]];
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - Actions

- (IBAction)toMyLocation:(id)sender {
    if (self.mapView.userLocation) {
        MKCoordinateRegion region;
        region.center = self.mapView.userLocation.coordinate;
        region.span = self.mapView.region.span;
        
        [self.mapView setRegion:region animated:YES];
    }
}

- (IBAction)toDnipro:(id)sender {
    MKCoordinateRegion region;
    region.center = CLLocationCoordinate2DMake(48.4638695, 35.04795);
    region.span = MKCoordinateSpanMake(0.1, 0.1);
    [self.mapView setRegion:region animated:YES];
}


#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    MKAnnotationView *result = nil;
    
    if (![annotation isKindOfClass:[MKUserLocation class]]){
        result = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"custom_annotation_id"];
        
        UIImage *image = [UIImage imageNamed:@"mapPin"];
        
        result.image = image;
        result.centerOffset = CGPointMake(0,
                                          -image.size.height/2);
    }
    
    return result;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        // значит пользователь разрешил трекать его локацию
        [manager startUpdatingLocation];
    } else {
        // пользователь запретил трекать его локацию
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    NSLog(@"Update Location %@", locations.firstObject);
    
    //locations.firstObject.timestamp
    
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        MKCoordinateRegion region;
        region.center = locations.firstObject.coordinate;
        region.span = MKCoordinateSpanMake(0.08, 0.08);
        [self.mapView setRegion:region animated:YES];
    });
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Fail With Error %@", error);
}

@end
