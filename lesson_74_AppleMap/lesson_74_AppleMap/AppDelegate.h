//
//  AppDelegate.h
//  lesson_74_AppleMap
//
//  Created by Yurii Bosov on 10/1/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

