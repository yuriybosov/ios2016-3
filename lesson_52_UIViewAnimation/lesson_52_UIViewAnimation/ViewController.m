//
//  ViewController.m
//  lesson_52_UIViewAnimation
//
//  Created by Yurii Bosov on 6/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
    __block BOOL isAnimate;
}

@end

@implementation ViewController

#pragma mark - Actions

- (IBAction)startButtonClicked:(id)sender {
    
    if (isAnimate) {
        return;
    }
    
    CGRect frame = rect.frame;
    frame.origin.x = self.view.frame.size.width - rect.frame.size.width;
    
    isAnimate = YES;
    [UIView animateWithDuration:5 animations:^{
        // применяем новый фрейм для вью в блоке анимации
        rect.frame = frame;
    } completion:^(BOOL finished) {
        isAnimate = NO;
    }];
}

- (IBAction)backButtonClicked:(id)sender {

    if (isAnimate) {
        return;
    }
    
    CGRect frame = rect.frame;
    frame.origin.x = 0;
    
    isAnimate = YES;
    [UIView animateWithDuration:5 animations:^{
        // применяем новый фрейм для вью в блоке анимации
        rect.frame = frame;
    } completion:^(BOOL finished) {
        isAnimate = NO;
    }];
}

@end
