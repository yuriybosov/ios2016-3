//
//  User.h
//  lesson_79_NSUserDefault
//
//  Created by Yurii Bosov on 10/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

// NSCoding протокол нужен для того, что бы сохранить объект в UserDefaults

@interface User : NSObject <NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger age;

@end
