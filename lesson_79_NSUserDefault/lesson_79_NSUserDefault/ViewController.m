//
//  ViewController.m
//  lesson_79_NSUserDefault
//
//  Created by Yurii Bosov on 10/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "User.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)saveButtonClicked:(id)sender {
    // сохранить\перезаписать данные
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    // любой объект из стндартной библиотеки
    [ud setObject:@"test save object" forKey:@"key1"];
    // булевская переменная
    [ud setBool:YES forKey:@"key2"];
    // целочисленая переменная
    [ud setInteger:111 forKey:@"key3"];
    // флоат переменная
    [ud setFloat:123.45 forKey:@"key4"];
    
    // сохрание кастомного объекта
    User *user = [[User alloc] init];
    user.name = @"Yurii";
    user.age = 33;
    
    // что сохранить кастомный объект нужено его представить в виде массива бай (то есть в объект NSData)
    // User -> NSData -> cохраняем в UserDefaults
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
    [ud setObject:data forKey:@"key5"];
}

- (IBAction)loadButtonClicked:(id)sender {
    // вычитать данные
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    // вычитали объект
    id value = [ud objectForKey:@"key1"];
    NSLog(@"key1 : %@", value);
    
    // вычитали болевскую переменную
    BOOL b = [ud boolForKey:@"key2"];
    NSLog(@"key2 : %i", b);
    
    // вычитали интовую переменную
    NSInteger i = [ud integerForKey:@"key3"];
    NSLog(@"key3 : %li", i);
    
    // вычитали флоат переменную
    float f = [ud floatForKey:@"key4"];
    NSLog(@"key4 : %f", f);
    
    // вычитали кастомный объект
    NSData *data = [ud objectForKey:@"key5"];
    if (data) {
        User *user = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSLog(@"key5: %@",user);
    }
}

- (IBAction)clearButtonClicked:(id)sender {
    // очистить данные
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    // 1-й вариант удаления - удалить каждый ключ
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"key1"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"key2"];
    
    // 2-й вариант удаления - удалить все данный из UserDefaults
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[NSBundle mainBundle].bundleIdentifier];
}

@end
