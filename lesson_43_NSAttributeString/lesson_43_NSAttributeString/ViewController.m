//
//  ViewController.m
//  lesson_43_NSAttributeString
//
//  Created by Yurii Bosov on 5/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //1. "Name: Yurii Bosov"
    NSString *name = @"Yuii Bosov";
    NSString *textForLabel1 = [NSString stringWithFormat:@"Name: %@", name];
    
    NSRange range = [textForLabel1 rangeOfString:name];
    if (range.location != NSNotFound) {
        // 1. создаем Attributed String
        NSMutableAttributedString *attStr1 = [[NSMutableAttributedString alloc] initWithString:textForLabel1];
        // 2. задаем для диапазона нужные нам аттрибуты
        // 2.1 создаем словарь с аттрибутами, где NSForegroundColorAttributeName - цвет текста, NSBackgroundColorAttributeName - цвет "заливки"
        NSDictionary *attributes = @{NSForegroundColorAttributeName:[UIColor redColor], NSBackgroundColorAttributeName:[UIColor yellowColor], NSFontAttributeName: [UIFont boldSystemFontOfSize:30]};
        // 2.2. указываем для какого диапазона применить эти аттрибуты
        [attStr1 addAttributes:attributes range:range];
        // 3 задаем UILabel вместо текста аттрибут строку
        label1.attributedText = attStr1;
    }

    // 2. "Link: google.com.ua"
    NSString *link = @"google.com.ua";
    NSString *textForLabel2 = [NSString stringWithFormat:@"Link: %@", link];
    range = [textForLabel2 rangeOfString:link];
    if (range.location != NSNotFound) {
        NSMutableAttributedString *attStr2 = [[NSMutableAttributedString alloc] initWithString:textForLabel2];
        
        NSDictionary *attributes = @{NSForegroundColorAttributeName:[UIColor blueColor], NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)};
        
        [attStr2 addAttributes:attributes range:range];
        
        label2.attributedText = attStr2;
    }
}

@end
