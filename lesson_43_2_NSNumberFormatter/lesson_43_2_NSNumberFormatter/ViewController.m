//
//  ViewController.m
//  lesson_43_2_NSNumberFormatter
//
//  Created by Yurii Bosov on 5/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // текущaя локаль девайса
    NSLog(@"текущaя локаль девайса %@", [[NSLocale currentLocale] objectForKey:NSLocaleIdentifier]);
    
    //
    NSNumber *num = @(115);
    
    //
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"uk_UA"];
    NSLog(@"локаль форматтера %@", [numberFormatter.locale objectForKey:NSLocaleIdentifier]);
    
    ////////
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSLog(@"DecimalStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    NSLog(@"CurrencyStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterPercentStyle;
    NSLog(@"PercentStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterScientificStyle;
    NSLog(@"ScientificStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterSpellOutStyle;
    NSLog(@"SpellOutStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterOrdinalStyle;
    NSLog(@"OrdinalStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterCurrencyISOCodeStyle;
    NSLog(@"CurrencyISOCodeStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterCurrencyPluralStyle;
    NSLog(@"CurrencyPluralStyle - %@", [numberFormatter stringFromNumber:num]);
    
    numberFormatter.numberStyle = NSNumberFormatterCurrencyAccountingStyle;
    NSLog(@"CurrencyAccountingStyle - %@", [numberFormatter stringFromNumber:num]);
    
    ///
    NSLog(@"availableLocaleIdentifiers\n%@",[NSLocale availableLocaleIdentifiers]);
    NSLog(@"ISOCurrencyCodes\n%@",[NSLocale ISOCurrencyCodes]);
}


@end
