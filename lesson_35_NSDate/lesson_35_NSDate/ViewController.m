//
//  ViewController.m
//  lesson_35_NSDate
//
//  Created by Yurii Bosov on 4/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Для хранение времени используем переменну типа NSDate *
    
    // 1. получить текущее системное время (оно по гринвичу!!!!)
    NSDate *date = [NSDate date];
    NSLog(@"%@", date);
    
    // 2. получить время по таймштампу. где таймштамп это кол-во секунд, которое прошло с 01.01.1970 00:00:00
    // 495504000 - это кол-во секнд на 14.09.1985
    date = [NSDate dateWithTimeIntervalSince1970:495504000];
    NSLog(@"%@", date);
    
    // 3. получить таймштамп по времени, которое прошло с 01.01.1970 00:00:00
    NSTimeInterval interval = [NSDate date].timeIntervalSince1970;
    NSLog(@"%f", interval);
    
    //
    
}

@end
