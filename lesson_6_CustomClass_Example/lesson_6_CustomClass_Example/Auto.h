//
//  Auto.h
//  lesson_6_CustomClass_Example
//
//  Created by Yurii Bosov on 12/6/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegistrationData.h"

@interface Auto : NSObject

@property (nonatomic, strong) NSString *brandName;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *color;

@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) CGFloat energyV;

@property (nonatomic, strong) RegistrationData *regData;

@end
