//
//  RegistrationData.m
//  lesson_6_CustomClass_Example
//
//  Created by Yurii Bosov on 12/6/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "RegistrationData.h"

@implementation RegistrationData

- (instancetype)initWithOwnerName:(NSString *)name
                           number:(NSString *)number
                             date:(NSString *)date {
    self = [super init];
    if (self) {
        _ownerName = name;
        _number = number;
        _registrationDate = date;
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@, %@", _ownerName, _number, _registrationDate];
}

@end
