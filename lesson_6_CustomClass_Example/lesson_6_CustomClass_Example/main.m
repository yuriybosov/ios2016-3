//
//  main.m
//  lesson_6_CustomClass_Example
//
//  Created by Yurii Bosov on 12/6/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Auto.h"
#import "Truck.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Auto *avto1 = [Auto new];
        
        avto1.brandName = @"ZAZ";
        avto1.model = @"1102";
        avto1.color = @"Black";
        avto1.age = 10;
        avto1.energyV = 1.2;
        
        RegistrationData *data1 = [RegistrationData new];
        data1.ownerName = @"Вася Пупкин";
        data1.number = @"AE 1234 BH";
        data1.registrationDate = @"12.10.2016";
        
        avto1.regData = data1;
        
        NSLog(@"%@", avto1);
        
        Auto *avto2 = [Auto new];
        
        avto2.brandName = @"VOLVO";
        avto2.model = @"S 80";
        avto2.color = @"Green";
        avto2.age = 2;
        avto2.energyV = 2.2;
        avto2.regData = [RegistrationData new];
        avto2.regData.ownerName = @"Petya";
        avto2.regData.number = @"AE 1122 VH";
        avto2.regData.registrationDate = @"01.01.2016";
        
        avto2.regData = [[RegistrationData alloc] initWithOwnerName:@"Vasya" number:@"EE0000BB" date:@"1.1.2001"];
        
        NSLog(@"%@", avto2);
        
        Auto *avto3 = [Auto new];
        
        avto3.brandName = @"TESLA";
        avto3.model = @"Model X";
        avto3.color = @"White";
        avto3.age = 1;
        avto3.energyV = 0;
        
        NSLog(@"%@", avto3);
        
        Truck *truck1 = [Truck new];
        truck1.brandName = @"MAN";
        truck1.model = @"Super Truck #1";
        truck1.color = @"Gray";
        truck1.age = 1;
        truck1.energyV = 9.1;
        truck1.maxWeight = 200000;
        truck1.typeLoad = TypeLoadRef;
        NSLog(@"%@", truck1);
    }
    return 0;
}
