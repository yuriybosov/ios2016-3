//
//  Truck.m
//  lesson_6_CustomClass_Example
//
//  Created by Yurii Bosov on 12/11/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "Truck.h"

@implementation Truck

- (NSString *)description
{
    // в переменную superDescription сохраняем результат вызова метода родительсокого класса
    NSString *superDescription = [super description];
    
    return [NSString stringWithFormat:@"%@\nМаксимальный вес = %f, тип груза = %li", superDescription, self.maxWeight, self.typeLoad];
}

@end
