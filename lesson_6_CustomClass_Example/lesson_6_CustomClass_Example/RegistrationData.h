//
//  RegistrationData.h
//  lesson_6_CustomClass_Example
//
//  Created by Yurii Bosov on 12/6/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegistrationData : NSObject

@property (nonatomic, strong) NSString *ownerName;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *registrationDate;

- (instancetype)initWithOwnerName:(NSString *)name
                           number:(NSString *)number
                             date:(NSString *)date;

@end
