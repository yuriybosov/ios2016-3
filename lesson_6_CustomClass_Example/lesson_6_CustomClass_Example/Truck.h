//
//  Truck.h
//  lesson_6_CustomClass_Example
//
//  Created by Yurii Bosov on 12/11/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "Auto.h"

// создадим enum, который будет описывать типы перевозимых грузов
typedef enum : NSUInteger {
    TypeLoadOil,
    TypeLoadRef,
    TypeLoadTent,
    TypeLoadOther
} TypeLoad;


@interface Truck : Auto

@property (nonatomic, assign) CGFloat maxWeight;
@property (nonatomic, assign) TypeLoad typeLoad;

@end
