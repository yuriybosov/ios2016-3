//
//  Auto.m
//  lesson_6_CustomClass_Example
//
//  Created by Yurii Bosov on 12/6/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "Auto.h"

@implementation Auto

- (NSString *)description
{
    // для случая, когда регистрационный данные могут отсутствовать, добавляем проверку, и если их нет, то выводим строку "нет данных"
    // для этого использовали тернарный оператор!
    NSString *tempStr = (_regData != nil) ? [_regData description] : @"нет данных";
    
    return [NSString stringWithFormat:@"\n%@, %@, %@ цвет\nвозраст - %lu лет\nобъем двигателя %.1f л.\nрегистрационные данные: %@", _brandName, _model, _color, _age, _energyV, tempStr];
}

@end
