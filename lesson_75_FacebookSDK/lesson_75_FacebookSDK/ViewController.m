//
//  ViewController.m
//  lesson_75_FacebookSDK
//
//  Created by Yurii Bosov on 10/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <MBProgressHUD.h>

@interface ViewController () {
    FBSDKLoginManager *loginManager;
    MBProgressHUD *hud;
}

@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (nonatomic, weak) IBOutlet UIButton *logoutButton;
@property (nonatomic, weak) IBOutlet UIButton *userProfileButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    loginManager = [[FBSDKLoginManager alloc] init];
    
    if ([FBSDKAccessToken currentAccessToken]) {
        self.loginButton.enabled = NO;
        self.logoutButton.enabled = YES;
        self.userProfileButton.enabled = YES;
    } else {
        self.loginButton.enabled = YES;
        self.logoutButton.enabled = NO;
        self.userProfileButton.enabled = NO;
    }
}

#pragma mark - Actions

-(IBAction)loginButtonClicked:(id)sender {
    
    [hud showAnimated:YES];
    
    __weak typeof(self) weakSelf = self;
    
    [loginManager logInWithReadPermissions:@[@"email",@"user_likes",@"user_birthday",@"user_location",@"user_friends",@"user_about_me"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        [weakSelf loginComplitedWith:result error:error];
        
    }];
}

- (void)loginComplitedWith:(FBSDKLoginManagerLoginResult *)result
                     error:(NSError *)error {
    if (result) {
        [hud hideAnimated:YES];
        
        if (!result.isCancelled &&
            result.token) {
            
            self.loginButton.enabled = NO;
            self.logoutButton.enabled = YES;
            self.userProfileButton.enabled = YES;
        }
        
    } else {
        hud.mode = MBProgressHUDModeText;
        hud.label.text = error.localizedDescription;
        [hud hideAnimated:YES afterDelay:4];
    }
}

-(IBAction)logoutButtonClicked:(id)sender {
    [loginManager logOut];
    
    self.loginButton.enabled = YES;
    self.logoutButton.enabled = NO;
    self.userProfileButton.enabled = NO;
}

-(IBAction)userProfileButtonClicked:(id)sender {
    
    hud.mode = MBProgressHUDAnimationFade;
    [hud showAnimated:YES];
    
    if ([FBSDKAccessToken currentAccessToken]) {
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"birthday,email,first_name,last_name,gender"}];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            
            // не правельно!!! исправить!!!
            [hud hideAnimated:YES];
            
            NSLog(@"result %@", result);
            NSLog(@"error %@", error);
            
        }];
    }
}

@end
