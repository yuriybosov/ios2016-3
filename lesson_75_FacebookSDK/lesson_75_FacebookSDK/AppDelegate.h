//
//  AppDelegate.h
//  lesson_75_FacebookSDK
//
//  Created by Yurii Bosov on 10/3/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

