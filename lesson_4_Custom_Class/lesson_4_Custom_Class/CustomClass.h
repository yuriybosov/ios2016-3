//
//  CustomClass.h
//  lesson_4_Custom_Class
//
//  Created by Yurii Bosov on 11/29/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

// import - это подключение .h-файлов (заголовочных файлов)
#import <Foundation/Foundation.h>

// interface - это объявление интерфейса класса (где интерфейс класса это набор переменных класса, свойст класса и методов класса)
// @interface <имя_класса> : <имя_родительского_класса>
@interface CustomClass : NSObject
// переменные класса (поля класса). все переменные класса описываются в фигурных скобках { }
{
    // поля класса видны только этому классу и классам наследникам
    NSInteger value1;
    CGFloat value2;
    BOOL value3;
}

// свойства класса (property)

// @property синтезирует 3 "сущности":
// 1 - приватную переменную класса, имя который совпадает с именем свойства, но в начале имени стоит нижнее подчеркивание
// 2 - get-метод, метод который возвращает значение этой приватной переменной
// 3 - set-метод, метод который задает значение этой приватной перенной

// assign пишем для простых типов (к примеру NSUInteger,CGFloat,BOOL)
// strong пишем для сложных типов (к примеру NSString)
@property (nonatomic, assign) NSUInteger property1;
@property (nonatomic, assign) CGFloat property2;
@property (nonatomic, strong) NSString *property3;

// методы класса (функции класса)
// методы класса делятся на два типа
// + методы класса (статические), вызываются у класса
// - методы объекта, вызываются у экземпляра класса (у объекта)

+ (void)staticMethod1;

- (void)method1;

// метод объекта с одим аргументом
- (void)method2WithParam1:(NSInteger)param;

// метод объекта с двумя аргументом
- (void)method3WithParam1:(NSUInteger)param1
               withParam2:(CGFloat)param2;

// метод объекта с несколькими аргументами
- (void)method4WithParam1:(NSUInteger)param1
               withParam2:(CGFloat)param2
               withParam3:(BOOL)param3;

@end


