//
//  CustomClass.m
//  lesson_4_Custom_Class
//
//  Created by Yurii Bosov on 11/29/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import "CustomClass.h"

// implementation - реализация класса, в ней будут реализованы все методы, которые есть в классе
@implementation CustomClass

// реализация "своих" методов

+ (void)staticMethod1{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// init - метод вызываем при создании\инициализации объекта
// пример расширения метода инита
- (instancetype)init {
    self = [super init];
    if (self){
        value1 = 1000;
        value2 = 40.43;
        value3 = YES;
        
        _property1 = 10;
        self.property2 = 10.01;
        [self setProperty3:@"!!!!"];
    }
    return self;
}

// dealloc - метод вызывается при удалении объекта
- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)method1 {
    // __PRETTY_FUNCTION__ - возвращает имя текущего метода, а так же имя класса, у кого этот метод был вызван 
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// реализация "родительских" методов
// пример переопределения метода (это значить, что будет полностью убрана реализация метода, которая была в родительском классе)
- (NSString *)description {
    return [NSString stringWithFormat:@"value1 = %li, value2 = %f, value3 = %i, property1 = %lu, property2 = %f, property3 = %@", value1, value2, value3, _property1, self.property2, [self property3]];
}

// реадизация методов несколькими аргументами (параметрами)
- (void)method2WithParam1:(NSInteger)param {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)method3WithParam1:(NSUInteger)param1
               withParam2:(CGFloat)param2 {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)method4WithParam1:(NSUInteger)param1
               withParam2:(CGFloat)param2
               withParam3:(BOOL)param3 {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [self method1];
}

@end
