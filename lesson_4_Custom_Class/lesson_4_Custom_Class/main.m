//
//  main.m
//  lesson_4_Custom_Class
//
//  Created by Yurii Bosov on 11/29/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

// 1. выполняем импорт нашего класса
#import "CustomClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // 2. объявляем переменную класса CustomClass
        CustomClass *obj;
        
        // 3. выделяем память для переменной obj
        obj = [CustomClass alloc];
        
        // 4. инициализируем переменную obj
        obj = [obj init];
        
        // 5. вызываем наши методы. статический
        [CustomClass staticMethod1];
        
        // 6. вызываем наши методы. метод объекта
        [obj method1];
        
        
        // пример объявление переной класса, выделения памяти и инициализация
        CustomClass *obj2 = [[CustomClass alloc] init];
        // пример объявление переной класса, выделения памяти и инициализация
        // испльзуем метод new (он заменяет вызов методов alloc и init)
        CustomClass *obj3 = [CustomClass new];
        
        // пример вызова метода класса, который выводит описание класса
        NSLog(@"%@", [obj2 description]);
        NSLog(@"%@", obj3);
        
        // работа с полями и свойствами класса
        // пример инициализации свойств объекта
        // 1 вариант (через '.')
        obj.property1 = 100;
        // 2 вариант (через set-метод)
        [obj setProperty1:200];
        
        obj.property2 = 0.45;
        obj.property3 = @"какая то строка";
        
        NSLog(@"obj = %@", obj);
        
        // пример получение значение свойств объекта
        // 1 вариант (через '.')
        NSUInteger i = obj.property1;
        // 2 вариант (через get-метод)
        i = [obj property1];
        
        // вызов методов с несколькими агрументами (параметрами)
        [obj method2WithParam1:100];
        [obj method3WithParam1:200 withParam2:4.5];
        [obj method4WithParam1:300 withParam2:50.75 withParam3:YES];
    }
    return 0;
}
