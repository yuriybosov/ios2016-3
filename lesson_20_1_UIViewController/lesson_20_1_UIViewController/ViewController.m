//
//  ViewController.m
//  lesson_20_1_UIViewController
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "ViewController1.h"
#import "ViewController2.h"
#import "ViewController3.h"
#import "ViewController4.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Init

// ВАЖНО!!! В метода инициализации нельзя работать со view!!!

// init - вызывается при создании контроллера программно с помощью этого метода или с помощью метода new
- (instancetype)init {
    self = [super init];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return self;
}

// initWithNibName:bundle: - вызывается при создании контроллера из nib-файла
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return self;
}

// initWithCoder: - вызывается при создании контроллера из storyboard-файла
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return self;
}

#pragma mark - View Life Cycle

// viewDidLoad - вызывается когда была загружена view этого контроллера. Начиная с этого метода можно работать со view. Метод viewDidLoad вызывается только один раз
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// viewWillAppear: - вызывается перед тем, как будет показана veiw нашего контроллера
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// viewDidAppear: - вызыватеся после того, как была показана view нашего контроллера
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

//  viewWillDisappear: - вызывается перед тем, как будет спрятана veiw нашего контроллера
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// viewDidDisappear: - вызыватеся после того, как была спрятана view нашего контроллера
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

#pragma mark - Button Actions

- (IBAction)bnt1DidClicked{
    // создаем ViewController1 программно используя метод new
    ViewController1 *controller = [[ViewController1 alloc] init];
    // отобразим данный контроллер
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)bnt2DidClicked{
    // создаем ViewController2 программно используя nib-файл
    ViewController2 *controller = [[ViewController2 alloc] initWithNibName:@"ViewController2" bundle:nil];
    // отобразим данный контроллер
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)bnt3DidClicked{
    // создаем ViewController3 из того же сторибоарда, где находится и ViewController (self)
    // 1. получаем storyboard, в котором был создан наш ViewController
    UIStoryboard *storyboard = self.storyboard;
    // 2. из сторибоарда получаем контроллер по его идентификатору. Важно что бы этот идентификатор был задан. В рамках одно сторибоарда неможет быть два одинаковых идентификатора.
    ViewController3 *controller = [storyboard instantiateViewControllerWithIdentifier:@"ViewController3"];
    // отобразим controller
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)bnt4DidClicked{
    // создаем ViewController4 из другого сторибоарда
    // 1. создаем storyboard по его имени
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard2" bundle:nil];
    // 2. Из сторибоарда берем контроллер который является "рутовым" (на которые указываем entry point)
    ViewController4 *controller = [storyboard instantiateInitialViewController];
    // отобразим controller
    [self.navigationController pushViewController:controller animated:YES];
}

@end
