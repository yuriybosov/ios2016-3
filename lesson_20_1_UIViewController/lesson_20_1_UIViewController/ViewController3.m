//
//  ViewController3.m
//  lesson_20_1_UIViewController
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController3.h"

@interface ViewController3 ()

@end

@implementation ViewController3
#pragma mark - Init

// ВАЖНО!!! В метода инициализации нельзя работать со view!!!

// init - вызывается при создании контроллера программно с помощью этого метода или с помощью метода new
- (instancetype)init {
    self = [super init];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return self;
}

// initWithNibName:bundle: - вызывается при создании контроллера из nib-файла
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return self;
}

// initWithCoder: - вызывается при создании контроллера из storyboard-файла
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return self;
}

#pragma mark - View Life Cycle

// viewDidLoad - вызывается когда была загружена view этого контроллера. Начиная с этого метода можно работать со view. Метод viewDidLoad вызывается только один раз
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// viewWillAppear: - вызывается перед тем, как будет показана veiw нашего контроллера
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// viewDidAppear: - вызыватеся после того, как была показана view нашего контроллера
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

//  viewWillDisappear: - вызывается перед тем, как будет спрятана veiw нашего контроллера
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

// viewDidDisappear: - вызыватеся после того, как была спрятана view нашего контроллера
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"%s", __PRETTY_FUNCTION__);
}
@end
