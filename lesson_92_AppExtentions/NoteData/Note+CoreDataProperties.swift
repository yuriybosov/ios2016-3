//
//  Note+CoreDataProperties.swift
//  NoteData
//
//  Created by Yurii Bosov on 11/30/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

import Foundation
import CoreData


extension Note {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Note> {
        return NSFetchRequest<Note>(entityName: "Note")
    }

    @NSManaged public var title: String?
    @NSManaged public var text: String?
    @NSManaged public var createDate: NSDate?
    @NSManaged public var modifyDate: NSDate?

}
