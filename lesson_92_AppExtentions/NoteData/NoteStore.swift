//
//  NoteStore.swift
//  NoteData
//
//  Created by Yurii Bosov on 11/30/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Foundation
import CoreData

public class NoteStore: NSObject {
    
    private let container = NSPersistentContainer(name: "Model")
    
    public static let shared = {
        return NoteStore()
    }()
    
    public func save() {
        
        if container.viewContext.hasChanges {
            do {
                try container.viewContext.save()
            } catch {
                print("View Context Saved With Error \(error)")
                abort()
            }
        }
    }
    
    private override init() {
        super.init()
        
        container.loadPersistentStores { (_, error) in
            if let error = error {
                print("Load Persistent Stores with error \(error)")
                abort()
            }
        }
    }
    
    //MARK - Note
    public func createNote() -> Note {
        let note = Note(context: container.viewContext)
        note.createDate = NSDate()
        save()
        return note
    }
    
    public func deleteNote(note: Note) {
        container.viewContext.delete(note)
        save()
    }
    
    public func getAllNotes() -> [Note] {
        
        let request: NSFetchRequest = Note.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "modifyDate", ascending: false)]
        do {
            return try container.viewContext.fetch(request)
        } catch {
            print("fetch with errro \(error)")
            return []
        }
    }
    
}
