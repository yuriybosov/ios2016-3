//
//  ViewController.swift
//  lesson_92_AppExtentions
//
//  Created by Yurii Bosov on 11/30/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import NoteData

class ViewController: UIViewController {

    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var trashButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let array = NoteStore.shared.getAllNotes()
        print("\(array)")
        
        if array.isEmpty {
            addButton.isEnabled = true
            trashButton.isEnabled = false
        } else {
            addButton.isEnabled = false
            trashButton.isEnabled = true
        }
    }
    
    @IBAction func addButtonClicked() {
        let note1 = NoteStore.shared.createNote()
        note1.title = "Note 1"
        note1.text = "QQQQQQQQQQ"
        
        let note2 = NoteStore.shared.createNote()
        note2.title = "Note 2"
        note2.text = "WWWWWWWWWW"
        
        let note3 = NoteStore.shared.createNote()
        note3.title = "Note 3"
        note3.text = "EEEEEEEEE"
        
        NoteStore.shared.save()
        
        addButton.isEnabled = false
        trashButton.isEnabled = true
    }
    
    @IBAction func trashButtonClicked() {
        NoteStore.shared.getAllNotes().forEach { (note) in
            NoteStore.shared.deleteNote(note: note)
        }
        
        addButton.isEnabled = true
        trashButton.isEnabled = false
    }
}

