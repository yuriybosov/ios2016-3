//
//  TodayViewController.swift
//  NoteWidget
//
//  Created by Yurii Bosov on 11/30/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import NotificationCenter
import NoteData

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = nil
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        
        if let note = NoteStore.shared.getAllNotes().first {
            label.text = "\(note.title ?? "" )" + "\n\(note.text ?? "")"
        } else {
            label.text = "Нет записей"
        }
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
