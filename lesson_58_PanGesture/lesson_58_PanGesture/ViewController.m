//
//  ViewController.m
//  lesson_58_PanGesture
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
}

@end

@implementation ViewController

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    CGPoint point = [sender translationInView:rect];
    rect.center = CGPointMake(rect.center.x + point.x, rect.center.y + point.y);
    [sender setTranslation:CGPointZero inView:rect];
}

@end
