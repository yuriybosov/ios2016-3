//
//  main.m
//  lesson_10_2_for_while
//
//  Created by Yurii Bosov on 12/20/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
    
        // while
        NSInteger i = 5;
        while (i < 5) {
            NSLog(@"!!!!!");
            i++;
        }
        
        // do while (отличие от цикла while в том, что у do while тело цикла выполнится хотя бы один раз)
        i = 100;
        do {
            NSLog(@"????");
            i++;
        } while (i < 5);

        // for
        for (NSInteger y = 0; y < 5; y++) {
            NSLog(@"####");
        }
        
        // for in (используется для удобной работы с коллекциями)
        // выполним обход массива с помощью forin
        NSArray *array1 = @[@"1",@"2",@"3",@"4",@"5"];
        
        for (NSString *str in array1) {
            NSLog(@"%@", str);
        }
        
        // выполним то же действие (обход массива), но уже с помощью for
        for (NSInteger i = 0; i < array1.count; i++){
            
            NSString *str = [array1 objectAtIndex:i];
            NSLog(@"%@", str);
        }
        
        // выполним обратный обход массива с помощью for
        for (NSInteger i = array1.count - 1; i >= 0; i--) {
            NSString *str = [array1 objectAtIndex:i];
            NSLog(@"%@", str);
        }
    }
    return 0;
}
