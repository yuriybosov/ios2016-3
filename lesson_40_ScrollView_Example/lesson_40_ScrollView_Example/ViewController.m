//
//  ViewController.m
//  lesson_40_ScrollView_Example
//
//  Created by Yurii Bosov on 4/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {

    IBOutlet UILabel *label1;
    
    IBOutlet UILabel *labelAutor;
    IBOutlet UILabel *labelDate;
    
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    IBOutlet UILabel *label4;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // title
    self.title = @"Scroll Example";
    
    // text for labels
    label1.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set";
    labelAutor.text = @"Какой то автор, может быть в две или более строк";
    labelDate.text = @"14.09.2000";
    label2.text = @"Called after the view has been loaded";
    label3.text = @"For view controllers created in code. For view controllers created in code.";
    label4.text = @"For view controllers unarchived from a nib";
}

@end
