//
//  main.m
//  lesson_7_NSString
//
//  Created by Yurii Bosov on 12/11/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // NSString - класс для работы со строками
        
        // объявили и инициализировали переменную NSString *
        NSString *str1 = @"str 1";
        NSLog(@"str1 = %@", str1);
        
        // объявили и инициализировали переменную NSString * пустой строкой
        NSString *str2 = @"";
        NSLog(@"str2 = %@", str2);
        
        // объявили переменную NSString * (без инициализации)
        NSString *str3;
        NSLog(@"str3 = %@", str3);
        
        // объявили переменную NSString * и инициализировали ее с помощью стригформатт метода
        NSString *str4 = [NSString stringWithFormat:@"это формат строка %@, %i, %0.2f", @"!!!!", 100, 29.39];
        NSLog(@"str4 = %@", str4);
        
        // методы класса NSString
        // 1. получение длины строки
        NSUInteger l = str4.length;
        NSLog(@"длина строки равна %lu", l);
        // пример исользования метода length
        if (str4.length > 0) {
            NSLog(@"строка не пустая");
        } else {
            NSLog(@"строка равна nil или кол-во символов равно нулю");
        }
        
        // 2. сравнение двух строк (строгое сравнение, с учетом регистра) c помощью метода isEqualToString:
        NSString *str5 = @"qwerty";
        NSString *str6 = @"Qwerty";
        BOOL result = [str5 isEqualToString:str6];
        if (result) {
            NSLog(@"строка \"%@\" равна строке \"%@\"", str5, str6);
        } else{
            NSLog(@"строка \"%@\" не равна строке \"%@\"", str5, str6);
        }
        
        // 3. сравнение двух строк c опциями (с учетом регистра или без или другие) с помощью метода compare:otrions:
        NSComparisonResult compareResult = [str5 compare:str6 options:NSCaseInsensitiveSearch];
        if (compareResult == NSOrderedSame) {
            NSLog(@"строка \"%@\" равна строке \"%@\" без учета регистра", str5, str6);
        } else {
            NSLog(@"строка \"%@\" не равна строке \"%@\"", str5, str6);
        }
        
        // 4. Поиск строк
        NSString *bigString = @"qwer asdf zxcv";
        NSString *searchString = @"qwe";
        
        // 4.1. совпадение в начале строки с помощью метода hasPrefix:
        result = [bigString hasPrefix:searchString];
        if (result) {
            NSLog(@"строка \"%@\" имеет \"%@\" начало", bigString, searchString);
        }
        
        // 4.2. совпадение в конце строки с помощью метода hasSuffix:
        searchString = @"cv";
        result = [bigString hasSuffix:searchString];
        if (result) {
            NSLog(@"строка \"%@\" имеет \"%@\" окончание", bigString, searchString);
        }
        
        // 4.3 поиск подстроке в строке с помощью метода containsString:
        searchString = @"as";
        result = [bigString containsString:searchString];
        if (result) {
            NSLog(@"строка \"%@\" содержится в строке \"%@\"", searchString, bigString);
        }
        
        // 4.4 поиск подстрок в строке с помощью метода rangeOfString:
        searchString = @"er";
        NSRange range = [bigString rangeOfString:searchString];
        if (range.location != NSNotFound) {
            NSLog(@"строка \"%@\" содержится в строке \"%@\" начиная с %lu символа, длиной %lu", searchString, bigString, range.location, range.length);
        }
        
        // 5. получение подстроки по диапазону(NSRange)
        range = NSMakeRange(10, 4);
        
        if (range.location + range.length <= bigString.length) {
            NSString *subString = [bigString substringWithRange:range];
            NSLog(@"subString = \"%@\"", subString);
        } else {
            NSLog(@"выход диапазона за пределы строки");
        }
        
        // 6. получение подстроки, с указанием индекса
        // 6.1 получение подстроки, до определенного индекса
        NSUInteger index = 13;
        if (index <= bigString.length) {
            NSString *subString = [bigString substringToIndex:index];
            NSLog(@"подстрока начиная с нулевого и до %lu индекса = %@", index, subString);
        } else{
            NSLog(@"индекс %lu больше чем длина строки", index);
        }
    
        // 6.2 получение подстроки, начиная с определеного индекса и до конца строки
        if (index <= bigString.length) {
            NSString *subString = [bigString substringFromIndex:index];
            NSLog(@"подстрока начиная c %lu индекса = %@", index, subString);
        } else {
            NSLog(@"индекс %lu больше чем длина строки", index);
        }
        
        // 7. Объединение (конкатинация) строк
        // 7.1 с помощью метода stringByAppendingString:
        str1 = @"!!!!!!";
        str2 = @"??????";
        str3 = [str1 stringByAppendingString:str2];
        NSLog(@"Объединенная строка %@", str3);
        
        // 7.2 с помощью метода stringByAppendingFormat:
        str3 = [str1 stringByAppendingFormat:@" %@", str2];
        NSLog(@"Объединенная строка %@", str3);
        
        // 7.3 с помощью метода stringByAppendingPathComponent: (он добавляет слеш "/" при конкатенации строк)
        str3 = [str1 stringByAppendingPathComponent:str2];
        NSLog(@"Объединенная строка %@", str3);
        
        // 8. Перевод в нижний \ верхний регистр
        // 8.1 lowercaseString - все буквы в нижнем регистре
        str3 = [bigString lowercaseString];
        NSLog(@"lowercaseString %@", str3);
        
        // 8.2 uppercaseString - все буквы в вверхнем регистре
        str3 = [bigString uppercaseString];
        NSLog(@"uppercaseString %@", str3);
        
        // 8.3 - каждая первая буква слова с большой буквы
        str3 = [bigString capitalizedString];
        NSLog(@"capitalizedString %@", str3);
        
        // 9 конвертация в примитивные типы
        NSString *string = @"10eee";
        NSLog(@"%li", [string integerValue]);
        NSLog(@"%0.2f", [string floatValue]);
        NSLog(@"%i", [string boolValue]);
    }
    return 0;
}
