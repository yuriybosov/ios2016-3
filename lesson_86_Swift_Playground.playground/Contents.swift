//: Playground - noun: a place where people can play

import UIKit

// 1. объявление переменных
// let - константа, нельзя let переменной задать другое значение
let var1 = 100
//var1 = 200 - для let это ошибка, так как let-переменной нельзя задать новое значение

// var-переменные можно менять (к примеру если это массив, то это аналогично как mutableArray), можно задать новое значение, если это объявлена структура, то в этом можно менять ее поля
var var2 = 200
var2 = 300

// 2. Типизация
let var3: Double = 10
let var4 = 5.5
var3 + var4

var point = CGPoint(x: var3, y: var4)
point.x = 10
point.y = 11

var array1 = [Int]()
array1.append(10)
array1.append(11)
array1.append(12)

var array2 = [10,11,12.5]
array2.append(13)
array2.append(14)

var array3 = [Any]()
array3.append(10)
array3.append("A")
array3.append(10.5)

if let a = array3[0] as? Int {
    a + 100
}

if let str = array3[1] as? String {
    str + " - " + "это строка"
}



// Опциональный типы
var var5: UIView?
var5 = nil
//var5 = UIView()

if var5 != nil {
    let x = var5!.frame.origin.x
    let y = var5!.frame.origin.y
}

if let view = var5 {
    let x = view.frame.origin.x
    let y = view.frame.origin.y
}

