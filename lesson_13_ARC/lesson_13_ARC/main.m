//
//  main.m
//  lesson_13_ARC
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        MyClass *obj = [[MyClass alloc] init];
        
        NSArray *array1 = [NSArray arrayWithObject:obj];
        NSArray *array2 = [[NSArray alloc] initWithObjects:obj, nil];
    }
    return 0;
}
