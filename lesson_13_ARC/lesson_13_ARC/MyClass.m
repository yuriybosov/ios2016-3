//
//  MyClass.m
//  lesson_13_ARC
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyClass.h"

@implementation MyClass

// метод, который вызывается при создании объекта
- (instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
    }
    return self;
}

// метод, который вызываетмя при удалении объекта
- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
