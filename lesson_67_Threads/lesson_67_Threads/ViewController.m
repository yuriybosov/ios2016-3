//
//  ViewController.m
//  lesson_67_Threads
//
//  Created by Yurii Bosov on 8/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSMutableArray *dataSource;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataSource = [NSMutableArray new];
}

#pragma mark - Buttons Actions

- (IBAction)buttonClicked:(id)sender {
    // вызов метода в главном потоке
//    [self testMethod];
    
    // вызов метода в фоновом потоке
    [self performSelectorInBackground:@selector(testMethod)
                           withObject:nil];
}

- (IBAction)buttonClicked2:(id)sender {
    @synchronized (self) {
        [dataSource addObject:@(1)];
    }
}

- (IBAction)buttonClicked3:(id)sender {
    // выполнить метод через определенный интервал
    [self performSelector:@selector(testMethod) withObject:nil afterDelay:10];
}

- (IBAction)buttonClicked4:(id)sender {
    // недать выполниться отложеным методам
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

#pragma mark - Test Methods

- (void)testMethod {
    
    // c помощью @synchronized мы потокообезопасим наш массив
    @synchronized (self) {
        NSLog(@"START %f", [NSDate date].timeIntervalSince1970);
        [dataSource removeAllObjects];
        for(NSUInteger i = 0; i < 100000000; i++) {
            [dataSource addObject:@(i)];
        }
        NSLog(@"testMethod done, count = %lu", dataSource.count);
        NSLog(@"END %f", [NSDate date].timeIntervalSince1970);
     
        [self performSelectorOnMainThread:@selector(testMethodComplited) withObject:nil waitUntilDone:YES];
    }
}

- (void)testMethodComplited {
    self.view.backgroundColor = [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
}

@end
