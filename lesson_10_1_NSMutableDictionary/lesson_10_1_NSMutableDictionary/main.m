//
//  main.m
//  lesson_10_1_NSMutableDictionary
//
//  Created by Yurii Bosov on 12/20/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        //1. создание словаря
        // 1.1 создаем по new
        NSMutableDictionary *dict1 = [[NSMutableDictionary alloc] init];
        
        // 1.2. создаем mutable словарь, на основе пар(ключей\значений) другого словаря
        dict1 = [NSMutableDictionary dictionaryWithDictionary:@{@"key1":@"obj1",@"key2":@"obj2"}];
        NSLog(@"%@", dict1);
        
        // 1.3 друие методы инициализации с наполнением контента так можно использовать (см урок NSDictionary)
        
        // 2. добавить новую пару (ключ\значение)
        [dict1 setObject:@"obj3" forKey:@"key3"];
        // альтернативная версия
        dict1[@"key4"] = @"obj4";
        
        NSLog(@"%@", dict1);
        
        // 3. удалить объект по ключу (удаляет всю пару из массива)
        [dict1 removeObjectForKey:@"key1"];
        // альтернативная версия
        dict1[@"key2"] = nil;
        
        NSLog(@"%@", dict1);
        
        // 3.1 удалить несколько объект, указав массив ключей
        [dict1 removeObjectsForKeys:@[@"key3",@"key4"]];
        NSLog(@"%@", dict1);
        
        // 3.2 удалить все объекты из словаря (очистить словарь)
        [dict1 removeAllObjects];
        NSLog(@"%@", dict1);
    }
    return 0;
}
