//
//  ViewController.m
//  lesson_28_Cell_From_Nib
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *dataSources;
    
    IBOutlet UITableView *myTableView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dataSources = @[@"q",@"a",@"v",@"b",@"t",@"n"];
    
    // для того, что бы использовать в таблице кастомную ячейку нужно выпольнить ее регистрацию в этой таблице
    // если нужно зарегистрировать ячейку, которая описана в отдельно nib-файле - то используем данный метод
    // 1. создаем nib-файл
    UINib *nib = [UINib nibWithNibName:@"CustomCell" bundle:nil];
    // 2. регистрируем nib-файл в таблице. важно указать корректный ReuseIdentifier по которому мы будем создавать ячейки из данного nib-файла
    [myTableView registerNib:nib forCellReuseIdentifier:@"CellID"];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    
    cell.myLabel.text = [dataSources objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

// убрать выделение с нажатой ячейки с анимацией
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 63;
}

@end
