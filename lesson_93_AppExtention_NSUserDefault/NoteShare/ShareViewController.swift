//
//  ShareViewController.swift
//  NoteShare
//
//  Created by Yurii Bosov on 12/3/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import Social
import MobileCoreServices


class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        // validate share content
        return true
    }

    override func didSelectPost() {
        
        // save note
        guard let item = self.extensionContext?.inputItems.first as? NSExtensionItem else {
            return
        }
        
        guard let proveder = item.attachments?.first as? NSItemProvider else {
            return
        }
        
        let typeText = kUTTypeText as String
        let typeURL = kUTTypeURL as String
        
        if proveder.hasItemConformingToTypeIdentifier(typeText) {
            proveder.loadItem(forTypeIdentifier: typeText, options: nil, completionHandler: { (coding, error) in
                if let text = coding as? String {
                    print("share TEXT \(text)")
                }
            })
        } else if proveder.hasItemConformingToTypeIdentifier(typeURL) {
            proveder.loadItem(forTypeIdentifier: typeURL, options: nil, completionHandler: { (coding, error) in
                if let url = coding as? URL {
                    print("share URL \(url)")
                }
            })
        }
        
        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        return []
    }
}
