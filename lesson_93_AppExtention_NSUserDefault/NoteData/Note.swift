//
//  Note.swift
//  NoteData
//
//  Created by Yurii Bosov on 12/3/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

public class Note: NSObject, NSCoding {
    
    public var title: String
    public var text: String
    public let createDate: Date
    public var modifyDate: Date
    
    public override var description: String {
        get {
            return "title: \(title), text: \(text), created: \(createDate), modify: \(modifyDate)"
        }
    }
    
    //MARK: - Init
    public init(with title: String, text: String) {
        self.title = title
        self.text = text
        self.createDate = Date()
        self.modifyDate = self.createDate
        super.init()
    }
    
    //MARK: - Save \ Load
    
    public class func saveNote(_ note: Note) {
        var savedNotes = loadNotes()
        savedNotes.append(note)
        
        let data = NSKeyedArchiver.archivedData(withRootObject: savedNotes)
        UserDefaults(suiteName:"group.com")?.set(data, forKey: "saved_notes")
    }
    
    public class func loadNotes() -> [Note] {
        
        if let data = UserDefaults(suiteName:"group.com")?.object(forKey: "saved_notes") as? Data {
            if let notes = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Note] {
                return notes
            }
        }
        return []
    }
    
    //MARK: - NSCoding
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(text, forKey: "text")
        aCoder.encode(createDate, forKey: "createDate")
        aCoder.encode(modifyDate, forKey: "modifyDate")
    }
    
    public required init?(coder aDecoder: NSCoder) {
        guard let title = aDecoder.decodeObject(forKey: "title") as? String,
            let text = aDecoder.decodeObject(forKey: "text") as? String,
            let createDate = aDecoder.decodeObject(forKey: "createDate") as? Date,
            let modifyDate = aDecoder.decodeObject(forKey: "modifyDate") as? Date else {
                return nil
        }
        self.title = title
        self.text = text
        self.createDate = createDate
        self.modifyDate = modifyDate
        super.init()
    }
}
