//
//  TodayViewController.swift
//  NoteTestWidget
//
//  Created by Yurii Bosov on 12/3/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import NotificationCenter
import NoteData

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = nil
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        
        label.text = Note.loadNotes().first?.description
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
