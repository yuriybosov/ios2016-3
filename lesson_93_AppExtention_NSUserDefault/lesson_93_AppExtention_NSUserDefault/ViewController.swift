//
//  ViewController.swift
//  lesson_93_AppExtention_NSUserDefault
//
//  Created by Yurii Bosov on 12/3/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import NoteData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //MAKR: - Actions
    @IBAction func createNote() {
        let note = Note(with: "title", text: "text \(Date().timeIntervalSince1970)")
        Note.saveNote(note)
    }
    
    @IBAction func loadNotes() {
        print("\(Note.loadNotes())")
    }
}

