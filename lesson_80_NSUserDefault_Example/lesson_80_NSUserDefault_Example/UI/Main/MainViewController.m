//
//  MainViewController.m
//  lesson_80_NSUserDefault_Example
//
//  Created by Yurii Bosov on 10/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MainViewController.h"
#import "Constants.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Main";
}

#pragma mark - Actions

- (IBAction)logoutButtonCliked:(id)sender {
    // 1. очищаем данные в UserDefaults
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLoginKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kPasswordKey];
    
    // 2. вместо этого экрана показать login controller
//    UIStoryboard *storyboard = [UIStoryboard ];
}

@end
