//
//  LoginViewController.m
//  lesson_80_NSUserDefault_Example
//
//  Created by Yurii Bosov on 10/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "LoginViewController.h"
#import "Constants.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Login";
}

@end
