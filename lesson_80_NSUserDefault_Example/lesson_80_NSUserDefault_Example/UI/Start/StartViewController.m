//
//  StartViewController.m
//  lesson_80_NSUserDefault_Example
//
//  Created by Yurii Bosov on 10/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "StartViewController.h"
#import "Constants.h"

@implementation StartViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // проверяем, есть ли в UserDefaults какие то сохраненные данные о пользователе, если есть, то откроем main controller, если нет, то откроем login controller
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    NSString *login = [ud objectForKey:kLoginKey];
    NSString *password = [ud objectForKey:kPasswordKey];
    
    NSString *storyboardName = nil;
    
    if (login && password) {
        // open main controller
        storyboardName = kMainStoryboardName;
    } else {
        // open login controller
        storyboardName = kLoginStoryboardName;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *controller = [storyboard instantiateInitialViewController];
    [UIApplication sharedApplication].keyWindow.rootViewController = controller;
}

@end
