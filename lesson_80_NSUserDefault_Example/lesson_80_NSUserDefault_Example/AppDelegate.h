//
//  AppDelegate.h
//  lesson_80_NSUserDefault_Example
//
//  Created by Yurii Bosov on 10/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

