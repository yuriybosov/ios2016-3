//
//  Constants.h
//  lesson_80_NSUserDefault_Example
//
//  Created by Yurii Bosov on 10/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import <Foundation/Foundation.h>

static NSString * const kLoginKey = @"kLoginKey";
static NSString * const kPasswordKey = @"kPasswordKey";

static NSString * const kLoginStoryboardName = @"Login";
static NSString * const kMainStoryboardName = @"Main";

#define UserDef [NSUserDefaults standardUserDefaults]

#endif /* Constants_h */
