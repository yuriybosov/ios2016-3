//
//  ViewController.m
//  lesson_78_EventKit
//
//  Created by Yurii Bosov on 10/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

@interface ViewController () <EKEventEditViewDelegate, EKEventViewDelegate> {
    EKEventStore *store;
}

@property (nonatomic, weak) IBOutlet UIButton *createEventButton;
@property (nonatomic, weak) IBOutlet UIButton *removeEventButton;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    store = [[EKEventStore alloc] init];
    
    // проверим статус, и если надо сделаем кнопки неактивными
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    switch (status) {
        case EKAuthorizationStatusNotDetermined:
            self.createEventButton.enabled = YES;
            self.removeEventButton.enabled = NO;
            break;
            
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
            self.createEventButton.enabled = NO;
            self.removeEventButton.enabled = NO;
            break;
            
        default:
            break;
    }
}

#pragma mark - Actions

- (IBAction)createEventButtonCliecked:(id)sender{
    // сначало проверим статус авторизации
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (status) {
        case EKAuthorizationStatusNotDetermined:
            // если не определен - выполним запрос авторизации
        {
            [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError * _Nullable error) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (granted) {
                        [self createEvent];
                        self.removeEventButton.enabled = YES;
                    } else {
                        self.createEventButton.enabled = NO;
                        NSLog(@"error %@", error);
                    }
                });
                
            }];
        }
            break;
            
        case EKAuthorizationStatusAuthorized:
            // если авторизован - создадим событие
            [self createEvent];
            break;
        default:
            break;
    }
}

- (IBAction)removeEventButtonCliecked:(id)sender{
    // сначало проверим статус авторизации
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (status) {
        case EKAuthorizationStatusAuthorized:
            // если авторизован - удалим событие
            [self removeEvent];
            break;
            
        default:
            break;
    }
}

- (void)createEvent {
    EKEvent *event = [EKEvent eventWithEventStore:store];
    event.title = @"TEST EVENT";
    
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    
    event.startDate = [NSDate dateWithTimeIntervalSince1970:1507712400 - zone.secondsFromGMT];
    event.endDate = [NSDate dateWithTimeIntervalSince1970:1507716000 - zone.secondsFromGMT];
    event.calendar = [store defaultCalendarForNewEvents];
    
    NSError *error = nil;
    [store saveEvent:event span:EKSpanThisEvent error:&error];
    if (error) {
        NSLog(@"%@", error);
    } else {
        NSLog(@"%@", event.eventIdentifier);
    }
}

- (void)removeEvent {
    EKEvent *event = [store eventWithIdentifier:@"64779B62-CA16-42B1-9562-625B16BE644A:937D3117-B655-4325-842E-5DFE77AEF943"];
    if (event) {
        NSError *error = nil;
        [store removeEvent:event span:EKSpanThisEvent error:&error];
        if (error) {
            NSLog(@"%@", error);
        }
    }
}

- (IBAction)openEventUIController:(id)sender {
    
    EKCalendarChooser *chooser = [[EKCalendarChooser alloc] initWithSelectionStyle:EKCalendarChooserSelectionStyleSingle displayStyle:EKCalendarChooserDisplayAllCalendars entityType:EKEntityTypeEvent eventStore:store];
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:chooser];
    
    [self presentViewController:nc animated:YES completion:nil];
}

- (IBAction)openEventEditController:(id)sender{
    EKEventEditViewController *controller = [EKEventEditViewController new];
    controller.eventStore = store;
    controller.editViewDelegate = self;
    
    [self presentViewController:controller
                       animated:YES completion:nil];
}

- (IBAction)openEventController:(id)sender {
    
    EKEventViewController *controller = [[EKEventViewController alloc] init];
    controller.delegate = self;
    
    EKEvent *event = [EKEvent eventWithEventStore:store];
    controller.event = event;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - EKEventEditViewDelegate

- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action {
    
    [controller dismissViewControllerAnimated:YES
                                   completion:nil];
}

#pragma mark - EKEventViewDelegate

- (void)eventViewController:(EKEventViewController *)controller didCompleteWithAction:(EKEventViewAction)action {
    
    [controller dismissViewControllerAnimated:YES
                                   completion:nil];
}

@end
