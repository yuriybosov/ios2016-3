//
//  Student+CoreDataClass.m
//  lesson_81_CoreData
//
//  Created by Yurii Bosov on 10/29/17.
//  Copyright © 2017 Georgesoft. All rights reserved.
//
//

#import "Student+CoreDataClass.h"

@implementation Student

+ (NSFetchRequest<Student *> *)studentsByGroupID:(NSString *)groupID {
    // нужно получить студентов, у которых groupID совпадает с id группы, которую мы передали в методе
    // для этого нужно:
    // 1. создать NSFetchRequest, указав из какой таблицы мы хотим получить данные (указав entity name)
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Student"];
    // 2. задать условие выборки (совпаде по groupID)
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupID == %@", groupID];
    request.predicate = predicate;
    // 3. опиционально еще можно задать сортировку результата, для этого используем NSSortDescriptor (класс, который описываем условие сортировки)
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    
    return request;
}

@end
