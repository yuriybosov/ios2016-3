//
//  Student+CoreDataProperties.h
//  lesson_81_CoreData
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 Georgesoft. All rights reserved.
//
//

#import "Student+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateOfBirthday;
@property (nullable, nonatomic, copy) NSString *groupID;
@property (nonatomic) int64_t identifier;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) BOOL gender;

@end

NS_ASSUME_NONNULL_END
