//
//  Group+CoreDataClass.h
//  lesson_81_CoreData
//
//  Created by Yurii Bosov on 10/29/17.
//  Copyright © 2017 Georgesoft. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Group : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Group+CoreDataProperties.h"
