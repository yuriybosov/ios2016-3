//
//  Group+CoreDataProperties.h
//  lesson_81_CoreData
//
//  Created by Yurii Bosov on 10/29/17.
//  Copyright © 2017 Georgesoft. All rights reserved.
//
//

#import "Group+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Group (CoreDataProperties)

+ (NSFetchRequest<Group *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *identifier;

@end

NS_ASSUME_NONNULL_END
