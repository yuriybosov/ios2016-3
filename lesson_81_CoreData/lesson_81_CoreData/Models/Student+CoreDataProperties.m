//
//  Student+CoreDataProperties.m
//  lesson_81_CoreData
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 Georgesoft. All rights reserved.
//
//

#import "Student+CoreDataProperties.h"

@implementation Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Student"];
}

@dynamic dateOfBirthday;
@dynamic groupID;
@dynamic identifier;
@dynamic name;
@dynamic gender;

@end
