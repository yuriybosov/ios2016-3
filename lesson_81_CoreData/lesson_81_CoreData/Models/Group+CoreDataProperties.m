//
//  Group+CoreDataProperties.m
//  lesson_81_CoreData
//
//  Created by Yurii Bosov on 10/29/17.
//  Copyright © 2017 Georgesoft. All rights reserved.
//
//

#import "Group+CoreDataProperties.h"

@implementation Group (CoreDataProperties)

+ (NSFetchRequest<Group *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Group"];
}

@dynamic title;
@dynamic identifier;

@end
