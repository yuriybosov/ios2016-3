//
//  ViewController.m
//  lesson_81_CoreData
//
//  Created by Yurii Bosov on 10/29/17.
//  Copyright © 2017 Georgesoft. All rights reserved.
//

#import "ViewController.h"
#import "Student+CoreDataClass.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Actions

- (IBAction)createButtonCliecked:(id)sender {
    // как "добраться" до app delegate
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // создать несколько студентов
    Student *student1 = [[Student alloc] initWithContext:appDelegate.persistentContainer.viewContext];
    student1.name = @"Bosov Yurii";
    student1.identifier = 1;
    student1.dateOfBirthday = [NSDate dateWithTimeIntervalSinceNow:495504000];
    student1.groupID = @"100";
    
    [appDelegate saveContext];
}

- (IBAction)loadButtonCliecked:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    // выполнить запрос в бд, получисть список всех всудентов, получисть список всудентов для определенной группы
    NSFetchRequest *request = [Student fetchRequest];
    NSArray *result = [appDelegate.persistentContainer.viewContext executeFetchRequest:request error:nil];
    NSLog(@"%@", result);
}

- (IBAction)removeButtonCliecked:(id)sender {
    // очистить бд
}

@end
