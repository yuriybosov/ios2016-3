//
//  ViewController.m
//  lesson_62_ResizebleImage
//
//  Created by Yurii Bosov on 7/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label;
    IBOutlet UIImageView *imageView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    label.text = @"-";
    
    //
    UIImage *originalImage = [UIImage imageNamed:@"bubleLeft"];
    CGSize size = originalImage.size;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(size.height/2,
                                               size.width/2,
                                               size.height/2 - 1,
                                               size.width/2 - 1);
    UIImage *resizeImage = [originalImage resizableImageWithCapInsets:edgeInsets];
    imageView.image = resizeImage;
}

@end
