//
//  main.m
//  lesson_8_1_NSArray
//
//  Created by Yurii Bosov on 12/13/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // NSArray - коллекция данных, данные в ней упорядоченые. доступ к данным можно получить по индексу.
        // NSArray не позволяем добавлять данные, удалять данные, заменять данные, т.е. размер и наполение массива остается не меняемым на протяжении всей объекта
        
        // 1. Инициализация и наполнение содержимым массива
        // 1.1. инициализация массива с один объектов
        id obj = @"somthing string";
        NSArray *array1 = [NSArray arrayWithObject:obj];
        // альтернативный вариант
        array1 = @[obj];
        
        NSLog(@"array1 %@", array1);
        
        // 1.2 инициализация массива с несколькими объектами (с тремя строками)
        NSArray *array2 = [NSArray arrayWithObjects:@"q",@"w",@"3",nil];
        // альтернативный вариант
        array2 = @[@"q",@"w",@"3"]; // в этом варианте nil в конце не пишем!!!
        
        NSLog(@"array2 %@", array2);
        
        // 1.3 инициализация массива с помощью другого массива
        NSArray *array3 = [NSArray arrayWithArray:array2];
        NSLog(@"array3 %@", array3);
        
        // 2 Получить длину массива (колличество элементов в массиве) с помощью свойства count
        NSUInteger count = array3.count;
        NSLog(@"колличество элементов в массиве равно %lu", count);
        
        // 3. Получение данных из массива
        // 3.1 - получить первый объект в массиве (не нужно делать проверку на выход за пределы массива)
        id firstObject = array3.firstObject;
        NSLog(@"первый элемент массива %@", firstObject);
        
        // 3.2 - получить последний объект в массиве (не нужно делать проверку на выход за пределы массива)
        id lastObject = array3.lastObject;
        NSLog(@"последний элемент массива %@", lastObject);
        
        // 3.3 - получить объект по индексу (нужно делать проверку на выход за пределы массива!!!)
        NSUInteger index = 1;
        // делаем проверку на то, что индекс не вышел за пределы массива
        if (index < array3.count) {
            obj = [array3 objectAtIndex:index];
            // альтернативный вариант
            obj = array3[index];
            
            NSLog(@"объект по индексу %lu равен %@", index, obj);
        } else {
            NSLog(@"выход за пределы массива");
        }
        
        // 3.4 - получить из массива подмассив (нужно делать проверку на выход за пределы массива!!!)
        NSRange range = NSMakeRange(1, 2);
        // делаем проверку на то, что range.location + range.length не вышли за пределы массива
        if (range.location + range.length <= array3.count){
            NSArray *result = [array3 subarrayWithRange:range];
            NSLog(@"subarray %@", result);
        } else {
            NSLog(@"выход за пределы массива");
        }
        
        // 4. Получить индекс по объекту (как понять что объек уже находится в массиве)
        // 4.1. получить порядковый номер объекта, и если он не равен NSNotFound значит объект содержится в массиве
        index = [array3 indexOfObject:@"r"];
        if (index == NSNotFound) {
            NSLog(@"такой элемент не содержится в массиве");
        } else {
            NSLog(@"элемент содержится в массиве под индексов %lu", index);
        }
        // 4.2. получить BOOL результат о том что объект находися в массиве
        BOOL result = [array3 containsObject:@"s"];
        if (result == NO) {
            NSLog(@"элемент не содержится в массиве");
        } else {
            NSLog(@"элемент содержится в массиве");
        }
        
        // 5. получить из массива одну строку, в которой будут объеденые все элементы массива, разделенные сепараторам
        NSString *string = [array3 componentsJoinedByString:@";"];
        NSLog(@"components string %@", string);
        
        // 6. получить из строки массив подстрок, указав разделитель
        NSString *bigString = @"Yurii Ivan Kolya Dmitrii";
        NSArray *substringsArray = [bigString componentsSeparatedByString:@" "];
        NSLog(@"substringsArray %@", substringsArray);
    }
    return 0;
}
