//
//  View1.m
//  lesson_63_Gradient
//
//  Created by Yurii Bosov on 7/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "View1.h"


@interface View1 () {
    CAGradientLayer *gradient;
}

@end

@implementation View1
    
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
    
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}
    
- (void)setup {
    
    // для того что бы вызывался метод drawRect: при изминении размер вьюшки нужно задать ей контент мод равный UIViewContentModeRedraw
    self.contentMode = UIViewContentModeRedraw;
    
    // настраивает градиент
    UIColor *color1 = [UIColor blueColor];
    UIColor *color2 = [UIColor greenColor];

    // градиент из двух цветов, переход вертикальный
    gradient = [CAGradientLayer layer];
    gradient.colors = @[(id)color1.CGColor, (id)color2.CGColor];
    [self.layer addSublayer:gradient];
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
    gradient.frame = rect;
}

@end
