//
//  ViewController.m
//  lesson_63_Gradient
//
//  Created by Yurii Bosov on 7/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "View1.h"
#import "View2.h"
#import "View3.h"

@interface ViewController () {
    IBOutlet View1 *view1;
    IBOutlet View2 *view2;
    IBOutlet View3 *view3;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // переносим всю логику рисования градиента в классы UIView, так как является правильным и не требуется дополнительного кода, для того что бы исправить размеры градиент леера и относительно размеров вью
    
//    //
//    UIColor *color1 = [UIColor blueColor];
//    UIColor *color2 = [UIColor greenColor];
//    
//    // 1. градиент из двух цветов, переход вертикальный
//    CAGradientLayer *gradient1 = [CAGradientLayer layer];
//    gradient1.colors = @[(id)color1.CGColor, (id)color2.CGColor];
//    gradient1.frame = view1.bounds;
//    [view1.layer addSublayer:gradient1];
//    
//    NSLog(@"startPoint %@, endPoint %@", NSStringFromCGPoint(gradient1.startPoint), NSStringFromCGPoint(gradient1.endPoint));
//    
//    // 2. градиент из двух цветов, переход горизонтальный
//    CAGradientLayer *gradient2 = [CAGradientLayer layer];
//    gradient2.colors = @[(id)color1.CGColor, (id)color2.CGColor];
//    gradient2.startPoint = CGPointMake(0, 0.5);
//    gradient2.endPoint = CGPointMake(1, 0.5);
//    gradient2.frame = view2.bounds;
//    [view2.layer addSublayer:gradient2];
//    
//    // 3. не равномерный градиент из двух цветов
//    CAGradientLayer *gradient3 = [CAGradientLayer layer];
//    gradient3.colors = @[(id)color1.CGColor, (id)color2.CGColor];
//    gradient3.frame = view3.bounds;
//    gradient3.locations = @[@(0.5),@(0.5)];
//    [view3.layer addSublayer:gradient3];
    
}

@end
