//
//  ClassA.m
//  lesson_14_Protocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ClassA.h"

@implementation ClassA

- (void)method1 {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)requiredMethod1 {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)requiredMethod2 {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)optionalMethod1 {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end
