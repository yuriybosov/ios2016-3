//
//  MyProtocol.h
//  lesson_14_Protocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#ifndef MyProtocol_h
#define MyProtocol_h

#import <Foundation/Foundation.h>

// в этом файле мы опишем протокол!!!
// протокол - это набор методов, которые может реализовать класс, который "доключил" конкретный протокол.
// часть методов в протоколе может быть обязательной для реализации, а часть нет (т.е. опциональной)

@protocol MyProtocol <NSObject>

@required // все методы, которые будут описаны ниже @required являются обязательными к выполнению. По умолчанию (если не указано @optional) все методы обязательны!!!

- (void)requiredMethod1;
- (void)requiredMethod2;

@optional  // все методы, которые будут описаны ниже @optional являются НЕ обязательными к выполнению

- (void)optionalMethod1;
- (void)optionalMethod2;

@end

#endif /* MyProtocol_h */
