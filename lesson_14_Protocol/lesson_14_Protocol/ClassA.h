//
//  ClassA.h
//  lesson_14_Protocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyProtocol.h"

@interface ClassA : NSObject <MyProtocol>

- (void)method1;

@end
