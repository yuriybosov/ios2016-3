//
//  main.m
//  lesson_14_Protocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassA.h"
#import "ClassB.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        ClassA *obj = [[ClassA alloc] init];
        ClassB *objB = [[ClassB alloc] init];
        
        // вызово метода, который был объявлен в классе А
        [obj method1];
        
        // вызово обязательного метода протокола (проверку можно не делать)
        [obj requiredMethod1];
        
        // вызово обязательного метода протокола (проверку можно не делать)
        [obj requiredMethod2];
        
        // !!! при вызове опциональных методов важно проверять, что данный метод был реализован данным классом. такую проверку можно выполнить с помощью метода respondsToSelector:
        // (SEL) заменяем на @selector(<название метода>)
        // делаем проверку для первого опционального метода
        if ([obj respondsToSelector:@selector(optionalMethod1)]) {
            [obj optionalMethod1];
        } else {
            NSLog(@"метод %@ не реализован", NSStringFromSelector(@selector(optionalMethod1)));
        }
    
        // делаем проверку для второго опционального метода
        if ([obj respondsToSelector:@selector(optionalMethod2)]) {
            [obj optionalMethod2];
        } else {
            NSLog(@"метод %@ не реализован", NSStringFromSelector(@selector(optionalMethod2)));
        }
        
        // сделаем проверку на реализацию протокола. это можно сделать с помощью метода conformsToProtocol:
        if ([objB conformsToProtocol:@protocol(MyProtocol)]) {
            NSLog(@"класс %@ реализует %@ протокол", NSStringFromClass([objB class]), NSStringFromProtocol(@protocol(MyProtocol)));
        } else {
            NSLog(@"класс %@ не реализует %@ протокол", NSStringFromClass([objB class]), NSStringFromProtocol(@protocol(MyProtocol)));
        }
    }
    return 0;
}
