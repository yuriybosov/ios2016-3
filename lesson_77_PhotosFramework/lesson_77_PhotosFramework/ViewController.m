//
//  ViewController.m
//  lesson_77_PhotosFramework
//
//  Created by Yurii Bosov on 10/8/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <Photos/Photos.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //1. проверить текущий статус авторизации для использования фотохранилища
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    switch (status) {
        case PHAuthorizationStatusNotDetermined:
            // доступ небыл еще запрошел приложением
            // нужно выполнимть запрос на разрешение использовать фотохранилище
            {
                [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                    
                    if (status == PHAuthorizationStatusAuthorized) {
                        // можно выполнять запросы на получение фото или альбомов
                        
                        // 1. вызов тестового метода получения всех фотохрафий
//                        [self allPhotos];
                        
                        // 2. вызов тестового метода получения всех коллекций
                        [self allColleсtions];
                    }
                }];
            }
            break;
        
        case PHAuthorizationStatusDenied:
        case PHAuthorizationStatusRestricted:
            // доступу был отклонет
            break;
            
        case PHAuthorizationStatusAuthorized:
            // доступу был разрешен
            // можно выполнять запросы на получение фото или альбомов
            
            // 1. вызов тестового метода получения всех фотографий
//            [self allPhotos];
            
            // 2. вызов тестового метода получения всех коллекций
            [self allColleсtions];
            
            break;
    }
}

#pragma mark - Fetch Photo Data

- (void)allPhotos{
    
    PHFetchOptions *options = [PHFetchOptions new];
    NSSortDescriptor *sortDescriptorByCreateDate = [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO];
    options.sortDescriptors = @[sortDescriptorByCreateDate];
    
    PHFetchResult<PHAsset *> *result = [PHAsset fetchAssetsWithOptions:options];
    
    for (PHAsset *asset in result) {
        NSLog(@"%@", asset);
    }
}

- (void)allColleсtions{
    
    PHFetchOptions *options = [PHFetchOptions new];
    
    PHFetchResult<PHAssetCollection *> *result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeAny options:options];
    
    for (PHAssetCollection *collection in result) {
        NSLog(@"%@", collection);
    }
}

@end
