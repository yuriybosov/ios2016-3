//
//  MyNavigationController.m
//  lesson_54_ScreenEdgePan
//
//  Created by Yurii Bosov on 6/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyNavigationController.h"

@implementation MyNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // для эксперимента выключили стандартный back-swipe
    self.interactivePopGestureRecognizer.enabled = NO;
    
    UIScreenEdgePanGestureRecognizer *recognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(didScreenEdgePanGesture:)];
    recognizer.edges = UIRectEdgeLeft;
    [self.view addGestureRecognizer:recognizer];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

#pragma mark - Actions

- (void)didScreenEdgePanGesture:(UIScreenEdgePanGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateBegan) {
        if (self.viewControllers.count > 1) {
            [self popViewControllerAnimated:YES];
        }
    }
}

@end
