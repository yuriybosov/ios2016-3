//
//  ViewController.m
//  lesson_54_ScreenEdgePan
//
//  Created by Yurii Bosov on 6/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

@end
