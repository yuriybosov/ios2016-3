//
//  Dispatcher.h
//  lesson_16_Example_Protocol
//
//  Created by Yurii Bosov on 1/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Truck.h"

@interface Dispatcher : NSObject <TruckProtocol>

@end
