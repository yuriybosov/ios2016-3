//
//  main.m
//  lesson_16_Example_Protocol
//
//  Created by Yurii Bosov on 1/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Truck.h"
#import "Dispatcher.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Dispatcher *dispatcher = [[Dispatcher alloc] init];
        
        Truck *truck = [[Truck alloc] init];
        truck.model = @"Volvo";
        truck.delegate = dispatcher;
        
        [truck prepareToMove];
        [truck move];
        [truck move];
        [truck move];
    }
    return 0;
}
