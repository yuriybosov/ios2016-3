//
//  Dispatcher.m
//  lesson_16_Example_Protocol
//
//  Created by Yurii Bosov on 1/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Dispatcher.h"

@implementation Dispatcher

- (void)truckDidMove:(Truck *)truck {
    NSLog(@"грузовик %@ начал путь в %@, везет %@", truck.model, truck.destination, [Truck cargoTypeToString:truck.cargoType]);
}

-(void)truckDidArrive:(Truck *)truck {
    NSLog(@"грузовик %@ прибыл в %@, привез %@", truck.model, truck.destination, [Truck cargoTypeToString:truck.cargoType]);
}

- (CargoType)cargoTypeForTruck:(Truck *)truck {
    
    CargoType type = CargoTypeNone;
    if ([truck.model isEqualToString:@"Volvo"]) {
        type = CargoTypeRef;
    }
    return type;
}

- (NSString *)destinationForTruck:(Truck *)truck {

    NSString *destination = nil;
    if ([truck.model isEqualToString:@"Volvo"]) {
        destination = @"Dnipro";
    }
    return destination;
}

@end
