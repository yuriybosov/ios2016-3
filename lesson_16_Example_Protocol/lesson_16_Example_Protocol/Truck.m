//
//  Truck.m
//  lesson_16_Example_Protocol
//
//  Created by Yurii Bosov on 1/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Truck.h"

@implementation Truck

+ (NSString *)cargoTypeToString:(CargoType)type {

    NSString *string = nil;
    
    switch (type) {
        case CargoTypeOil:
            string = @"нефте продукты";
            break;
            
        case CargoTypeTent:
            string = @"оборудование";
            break;
            
        case CargoTypeRef:
            string = @"мороженое";
            break;
            
        default:
            string = @"груз не задан";
            break;
    }
    
    return string;
}

- (void)prepareToMove {
    // запросили тип груза у делегата (у диспетчера)
    CargoType type = [self.delegate cargoTypeForTruck:self];
    // сохранили этот тип в свойстве
    self.cargoType = type;
    
    // запросили пункт назначение у делегата (у диспетчера)
    NSString *destination = [self.delegate destinationForTruck:self];
    // сохранили этот пункт назначение в свойстве
    self.destination = destination;
}

- (void)move {
    
    // перед начал движения проверяем что есть все данные для начала пути, а именно: указан типа груза и указан пункт назначения
    // если это данные не указаны - грузовик не может начать движение

    if (self.cargoType != CargoTypeNone &&
        self.destination.length > 0) {
        
        // оповещаем делегата о том что начали движение
        [self.delegate truckDidMove:self];
        
        NSLog(@"грузовик %@ едет", self.model);
        NSLog(@"грузовик %@ едет", self.model);
        NSLog(@"грузовик %@ едет", self.model);
        
        // оповещаем делегата о том что приехали
        [self.delegate truckDidArrive:self];
        
        // после того, как приехал нужно занулять тип груза и пункт назначения
        self.cargoType = CargoTypeNone;
        self.destination = nil;
        
    } else {
        NSLog(@"нет вводных данных для начала движения для грузовика %@", self.model);
    }
}

@end
