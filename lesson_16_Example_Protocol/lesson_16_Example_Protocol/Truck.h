//
//  Truck.h
//  lesson_16_Example_Protocol
//
//  Created by Yurii Bosov on 1/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    CargoTypeNone,      // тип груза не задан
    CargoTypeOil,       // тип груза для нефте продукты
    CargoTypeTent,      // тип груза тент
    CargoTypeRef        // тип груза для перевозки холодных товаров
} CargoType; // создали enum, который определяет тип груза

@class Truck;

// протокол, который поможет грузовику получать данные (что и куда перевозить) и который будет оповещать делегата что грузовик начал вдижение и закончил движение
@protocol TruckProtocol <NSObject>

- (void)truckDidMove:(Truck *)truck;
- (void)truckDidArrive:(Truck *)truck;
- (CargoType)cargoTypeForTruck:(Truck *)truck;
- (NSString *)destinationForTruck:(Truck *)truck;

@end


@interface Truck : NSObject

@property (nonatomic, strong) NSString *model;

@property (nonatomic, strong) NSString *destination;
@property (nonatomic, assign) CargoType cargoType;

@property (nonatomic, weak) id<TruckProtocol> delegate;

+ (NSString *)cargoTypeToString:(CargoType)type;

- (void)prepareToMove; // подготовка к поездке. в этом методе мы должны запросить у диспетчера что и куда вести
- (void)move;

@end
