//
//  main.m
//  lesson_15_PrinterNotification
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Printer.h"
#import "OfficeManager.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Printer *p = [Printer new];
        OfficeManager *m = [OfficeManager new];
        
        p.name = @"Canon 2900";
        p.currentPageCount = 10;
        
        [p startPrintWithQuantity:0];
        [p startPrintWithQuantity:7];
        [p startPrintWithQuantity:5];
    }
    return 0;
}
