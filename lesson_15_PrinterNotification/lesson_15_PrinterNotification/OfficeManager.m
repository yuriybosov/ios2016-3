//
//  OfficeManager.m
//  lesson_15_PrinterNotification
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "OfficeManager.h"
#import "Printer.h"

@implementation OfficeManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(printerStartedNotification:) name:kPrinterStartedNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(printerComplitedNotification:) name:kPrinterComplitedNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(printerFeiledNotification:) name:kPrinterFeiledNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(printerPrintedPageFromPagesNotification:) name:kPrinterPrintedPageFromPagesNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(printerResumeNotification:) name:kPrinterResumeNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notification

- (void)printerStartedNotification:(NSNotification *)notification {
    Printer *printer = notification.object;
    NSLog(@"принтер %@ начал печать", printer.name);
}

- (void)printerComplitedNotification:(NSNotification *)notification {
    Printer *printer = notification.object;
    NSLog(@"принтер %@ закончил печать", printer.name);
}

- (void)printerFeiledNotification:(NSNotification *)notification {
    Printer *printer = notification.object;
    NSString *errorMessage = notification.userInfo[kMessageKey];
    
    NSLog(@"принтер %@ не смог завершить печать по причине %@", printer.name, errorMessage);
    
    printer.currentPageCount = 10;
    [printer resume];
}

- (void)printerPrintedPageFromPagesNotification:(NSNotification *)notification {
    Printer *printer = notification.object;
    NSNumber *page = notification.userInfo[kPageKey];
    NSNumber *totalPage = notification.userInfo[kTotalPageKey];
    
    NSLog(@"принтер %@ напечатал %@ из %@", printer.name, page, totalPage);
}

- (void)printerResumeNotification:(NSNotification *)notification {
    Printer *printer = notification.object;
    NSLog(@"принтер %@ возобновил печать", printer.name);
}

@end
