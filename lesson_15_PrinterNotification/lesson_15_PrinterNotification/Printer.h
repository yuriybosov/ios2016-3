//
//  Printer.h
//  lesson_15_PrinterNotification
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Printer : NSObject {
    NSUInteger currentPage;
    NSUInteger currentQuantity;
}

@property (nonatomic, assign) NSUInteger currentPageCount;
@property (nonatomic, strong) NSString *name;

- (void)startPrintWithQuantity:(NSUInteger)quantity;
- (void)resume;

@end
