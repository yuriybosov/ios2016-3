//
//  Constants.h
//  lesson_15_PrinterNotification
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import <Foundation/Foundation.h>

// for notifications
static NSString * const kPrinterStartedNotification = @"PrinterStartedNotification";
static NSString * const kPrinterComplitedNotification = @"PrinterComplitedNotification";
static NSString * const kPrinterFeiledNotification = @"PrinterFeiledNotification";
static NSString * const kPrinterPrintedPageFromPagesNotification = @"PrinterPrintedPageFromPagesNotification";
static NSString * const kPrinterResumeNotification = @"PrinterResumeNotification";

// for dictionary
static NSString *const kMessageKey = @"MessageKey";
static NSString *const kPageKey = @"PageKey";
static NSString *const kTotalPageKey = @"TotalPageKey";

#endif /* Constants_h */
