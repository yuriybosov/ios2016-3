//
//  Printer.m
//  lesson_15_PrinterNotification
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Printer.h"

@implementation Printer

- (void)startPrintWithQuantity:(NSUInteger)quantity {
    
    // делаем проверку что кол-во станиц больше нуля, тольок в том случае начинаем печать
    if (quantity > 0) {
    
        // посылаем уведомление о том, что печать начата
        [[NSNotificationCenter defaultCenter] postNotificationName:kPrinterStartedNotification object:self];
        
        currentQuantity = quantity;
        [self startPrintFromPage:0 withQuantity:currentQuantity];
    }
}

- (void)startPrintFromPage:(NSUInteger)page
              withQuantity:(NSUInteger)quantity {
    
    // в цикле выполняем "печать"
    for (NSUInteger i = page; i < quantity; i++) {
        // делаем проверку, что печать возможна (т.е. бумага в принтере есть)
        if (self.currentPageCount > 0) {
            
            // отняли страничку, так как выполнили печать
            self.currentPageCount--;
            
            // посылаем уведомление о том что начетатили новую страницу
            [[NSNotificationCenter defaultCenter] postNotificationName:kPrinterPrintedPageFromPagesNotification object:self userInfo:@{kPageKey:@(i+1), kTotalPageKey:@(quantity)}];
            
        } else {
            
            // запоминаем не напечатаную страничку
            currentPage = i;
            
            // посылаем уведомление о том что печать не возможна по причине отсутствия бумаги
            [[NSNotificationCenter defaultCenter] postNotificationName:kPrinterFeiledNotification object:self userInfo:@{kMessageKey:@"закончилась бумага"}];
            
            // что бы прервать цикл и полностью выйти из этой функции
            return;
        }
    }
    
    // обнуляем текущие данные
    currentPage = 0;
    currentQuantity = 0;
    
    // если цикл печать отбаротал без фейла (return не вызволся) значит мы успешно выполнили печать и посылаем соотв. уведомление
    [[NSNotificationCenter defaultCenter] postNotificationName:kPrinterComplitedNotification object:self];
}

- (void)resume {
    
    // посылаем сообщение, что принтер возобновил работу
    [[NSNotificationCenter defaultCenter] postNotificationName:kPrinterResumeNotification object:self];
    
    [self startPrintFromPage:currentPage
                withQuantity:currentQuantity];
}

@end
