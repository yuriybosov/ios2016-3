//
//  AppDelegate.h
//  lesson_24_UILabel
//
//  Created by Yurii Bosov on 2/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

