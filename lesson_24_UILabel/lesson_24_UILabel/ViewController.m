//
//  ViewController.m
//  lesson_24_UILabel
//
//  Created by Yurii Bosov on 2/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    IBOutlet UILabel *label4;
    IBOutlet UILabel *label5;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // задаем бг цвет
    label1.backgroundColor = [UIColor yellowColor];
    
    // устанавливаем текст
    label1.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set";
    
    // устанавливаем цвет текста
    label1.textColor = [UIColor blueColor];
    
    // устанавливаем цвет тени
    label1.shadowColor = [UIColor redColor];
    
    // устанавливаем отступ тени
    label1.shadowOffset = CGSizeMake(1, 1);
    
    // установим кол-во линий равное 0, это позволит выполнить нужно кол-во переносов строк, что бы отобразился весь контент.
    label1.numberOfLines = 0;
    
    // установим шрифт
    label1.font = [UIFont boldSystemFontOfSize:14];
    
    ////////
    label2.text = @"Title:";
    label3.text = @"Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text TextText Text Text Text Text TextText Text Text Text Text TextText Text Text Text Text Text";
    
    label4.text = @"Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text TextText Text Text Text Text TextText Text Text Text Text TextText Text Text Text Text Text";
    
    label5.text = @"Text Text Text Text Text Text Text Text Text Text Text Text Text Text Text Tex";
    
}

@end
