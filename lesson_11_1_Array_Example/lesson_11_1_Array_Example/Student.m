//
//  Student.m
//  lesson_11_1_Array_Example
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "Student.h"

@interface Student () {
    NSMutableDictionary *journal;
}

@end


@implementation Student

+ (instancetype)studenWithName:(NSString *)name
                           age:(NSUInteger)age {
    
    Student *student = [[Student alloc] init];
    
    student.name = name;
    student.age = age;
    
    return student;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        journal = [NSMutableDictionary new];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %lu", _name, _age];
}

- (void)addMark:(NSNumber *)mark forSubject:(NSString *)subjectName {
    
    // берем массив оценок из нашего журнала.
    // т.е. из словаря по ключу "имя предмата" берем массив оценок
    NSMutableArray *marks = [journal objectForKey:subjectName];
    
    // массив оценок может не существовать, если мы еще ни разу не добавляли в наш журнал оценки
    // если массива оценок нет - то его нужно создать и не забыть добавить в журнал
    if (marks == nil) {
        marks = [[NSMutableArray alloc] init];
        [journal setObject:marks forKey:subjectName];
    }
    
    // добавим в массив оценку
    [marks addObject:mark];
}

- (void)showJournal {
    // выводим в лог журнал с оценками
    // выводим в таком виде:
    // <название предмета> : <оценки через запятую>
    
    for (NSString *key in [journal.allKeys sortedArrayUsingSelector:@selector(compare:)]) {
        
        NSArray *marks = [journal objectForKey:key];
        NSString *marksString = [marks componentsJoinedByString:@","];
        NSLog(@"%@ : %@", key, marksString);
    }
}

- (void)showAvarageJournal{
    
    for (NSString *key in [journal.allKeys sortedArrayUsingSelector:@selector(compare:)]) {
        
        NSArray *marks = [journal objectForKey:key];
        CGFloat summ = 0;
        for (NSNumber *mark in marks) {
            summ += mark.doubleValue;
        }
        CGFloat avarage = summ / marks.count;
        NSLog(@"%@ : %0.1f", key, avarage);
    }
}

- (CGFloat)avarageMark {
    CGFloat avarage = 0;
    CGFloat summ = 0;
    NSUInteger count = 0;
    
    for (NSString *key in journal) {
        NSArray *marks = [journal objectForKey:key];
        
        for (NSNumber *mark in marks) {
            summ += mark.doubleValue;
            count++;
        }
    }
    
    if (count != 0) {
        avarage = summ / count;
    }
    
    return avarage;
}

@end



