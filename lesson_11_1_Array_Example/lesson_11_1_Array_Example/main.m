//
//  main.m
//  lesson_11_1_Array_Example
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Group.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Student *st1 = [Student studenWithName:@"Yurii" age:20];
        Student *st2 = [Student studenWithName:@"Alex" age:25];
        Student *st3 = [Student studenWithName:@"Kirill" age:27];
        
        Group *group = [Group new];
        group.name = @"iOS cources 2016";
        
        [group addStudent:st1];
        [group addStudent:st2];
        [group addStudent:st3];
        NSLog(@"%@", group);

        //////////////////////////////////////////////////////////
        // удаление студента
//        [group removeStudent:st1];
//        NSLog(@"%@", group);
        
        //////////////////////////////////////////////////////////
//        NSArray *searchByName = [group searchStudentsByName:@"i"];
//        NSLog(@"searchByName = %@", searchByName);
//        
//        NSArray *searchByAge = [group searchStudentsByAge:21];
//        NSLog(@"searchByAge = %@", searchByAge);
//        
//        NSArray *searchArray = [group searchStudentsByMinAge:30 maxAge:35];
//        NSLog(@"searchArray = %@", searchArray);
        
        //////////////////////////////////////////////////////////
        [st1 addMark:@(10) forSubject:@"Matematic"];
        [st1 addMark:@(8) forSubject:@"Matematic"];
        [st1 addMark:@(11) forSubject:@"Matematic"];
        
        [st1 addMark:@(10) forSubject:@"History"];
        [st1 addMark:@(12) forSubject:@"History"];
        
        [st1 addMark:@(2) forSubject:@"Matematic"];
        
        [st1 showJournal];
        [st1 showAvarageJournal];
        CGFloat avarage = [st1 avarageMark];
        NSLog(@"avarage = %0.1f", avarage);
        
        CGFloat groupAvagate = [group avarageMark];
        NSLog(@"groupAvagate = %0.1f", groupAvagate);
    }
    return 0;
}
