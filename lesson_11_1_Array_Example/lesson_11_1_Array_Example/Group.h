//
//  Group.h
//  lesson_11_1_Array_Example
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

@interface Group : NSObject

@property (nonatomic, strong) NSString *name;

- (void)addStudent:(Student *)student;
- (void)removeStudent:(Student *)student;

- (NSArray <Student *> *)searchStudentsByName:(NSString *)name;

- (NSArray <Student *> *)searchStudentsByAge:(NSUInteger)age;

- (NSArray <Student *> *)searchStudentsByMinAge:(NSUInteger)minAge
                                         maxAge:(NSUInteger)maxAge;

- (CGFloat)avarageMark;

@end
