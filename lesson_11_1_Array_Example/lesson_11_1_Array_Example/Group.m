//
//  Group.m
//  lesson_11_1_Array_Example
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "Group.h"

@interface Group () {
    NSMutableArray <Student *> *studentsArray;
}

@end


@implementation Group

#pragma mark - Init

- (instancetype)init
{
    self = [super init];
    if (self) {
        studentsArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"\n%@\n%@", self.name, studentsArray];
}

#pragma mark - Add \ Remove Student

- (void)addStudent:(Student *)student {
    if (![studentsArray containsObject:student])
        [studentsArray addObject:student];
}

- (void)removeStudent:(Student *)student {
    [studentsArray removeObject:student];
}

#pragma mark - Search

- (NSArray<Student *> *)searchStudentsByName:(NSString *)name {
    
    // 1. создаем массив, в который будем сохранять найденых студентов
    NSMutableArray *result = [NSMutableArray array];
    
    // 2. с помощью цикла "просмотрим" всех студентов, и если есть совпадение по имени, то добавим студетна в result массив
    for (Student *obj in studentsArray) {
        
        // проверяем наличие совпадения name с именем студента
        NSRange range = [obj.name rangeOfString:name options:NSCaseInsensitiveSearch];
        
        // есть есть совпадение, то location не будет равен NSNotFound
        if  (range.location != NSNotFound) {
            // то добавляем студента в result массив
            [result addObject:obj];
        }
    }
    
    return result;
}

- (NSArray<Student *> *)searchStudentsByAge:(NSUInteger)age {
    NSMutableArray *result = [NSMutableArray new];
    
    for (Student *obj in studentsArray) {
        if (obj.age == age) {
            [result addObject:obj];
        }
    }
    
    return result;
}

- (NSArray<Student *> *)searchStudentsByMinAge:(NSUInteger)minAge maxAge:(NSUInteger)maxAge {
    
    NSMutableArray *result = [NSMutableArray new];
    
    for (Student *obj in studentsArray) {
        if (obj.age >= minAge && obj.age <= maxAge) {
            [result addObject:obj];
        }
    }
    
    return result;
}

- (CGFloat)avarageMark {
    CGFloat avarage = 0;
    
    if (studentsArray.count) {
        
        CGFloat summ = 0;
        for (Student *student in studentsArray) {
            summ += [student avarageMark];
        }
        avarage = summ / studentsArray.count;
    }
    
    return avarage;
}

@end


