//
//  Student.h
//  lesson_11_1_Array_Example
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Student : NSObject 

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger age;

+ (instancetype)studenWithName:(NSString *)name
                           age:(NSUInteger)age;

- (void)addMark:(NSNumber *)mark forSubject:(NSString *)subjectName;
- (void)showJournal; // показать все оценки по предметам
- (void)showAvarageJournal; // показать средний бал по предметам
- (CGFloat)avarageMark; // метод, который вернет средний балл

@end
