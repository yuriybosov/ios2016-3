//
//  ViewController.swift
//  lesson_89_SnapKit
//
//  Created by Yurii Bosov on 11/19/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        oneViewExample()
        
//        twoViewsExample()
        
        twoLabelsExample()
    }
    
    private func oneViewExample() {
        // 1. create one view
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        self.view.addSubview(view)
        
        view.snp.makeConstraints { maker in
// 1. размер как и у родительской вью
//            maker.edges.equalToSuperview()

// 2. добавим отступы со всех сторон по 20 пунктов
//            maker.edges.equalToSuperview().inset(20)

// 3. добавим отступы со всех сторон, но с разными значениями
//            maker.top.equalToSuperview().inset(40)
//            maker.left.equalToSuperview().inset(20)
//            maker.bottom.equalToSuperview().inset(40)
//            maker.right.equalToSuperview().inset(20)

// 4. ширина как у родительской вью, а высота имеет фиксированое значение
//            maker.width.equalToSuperview()
//            maker.height.equalTo(200)

// 5. по центру родительской вью
//            maker.width.height.equalTo(100)
//            maker.centerX.centerY.equalToSuperview()

// 6. разместить вью в нижнем правом углу
            maker.width.height.equalTo(100)
            maker.bottom.right.equalToSuperview()
        }
    }
    
    private func twoViewsExample() {
        
        let view1 = UIView()
        view1.backgroundColor = .yellow
        self.view.addSubview(view1)
        
        let view2 = UIView()
        view2.backgroundColor = .blue
        self.view.addSubview(view2)
        
//        // 1. первая и вторя вью находятся одна под одной, с одинаковой шириной и высотой, заполняют всю родительскую вью
//
//        view1.snp.makeConstraints { maker in
//            maker.top.left.right.equalToSuperview()
//        }
//
//        view2.snp.makeConstraints { maker in
//            maker.left.bottom.right.equalToSuperview()
//            maker.top.equalTo(view1.snp.bottom)
//            maker.height.equalTo(view1.snp.height)
//        }
        
        // 2. парвая и вторя вью в фиксированными размерами, с позиционированные односительно друг друга (выравнивание по верху)
        
        view1.snp.makeConstraints { maker in
            maker.top.left.equalToSuperview().inset(40)
            maker.width.height.equalTo(100)
        }
        
        view2.snp.makeConstraints { maker in
            maker.left.equalTo(view1.snp.right).offset(40)
//            maker.top.equalTo(view1.snp.top)
            maker.centerY.equalTo(view1.snp.centerY)
            maker.width.height.equalTo(50)
        }
    }
    
    private func twoLabelsExample() {
        let label1 = UILabel()
        label1.backgroundColor = .red
        label1.text = "Called loaded"
        self.view.addSubview(label1)
        
        let label2 = UILabel()
        label2.backgroundColor = .green
        label2.text = "Called after the view has been loaded Called after the view has been loaded Called after the view has been loaded"
        label2.numberOfLines = 0
        self.view.addSubview(label2)
        
        label1.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().inset(100)
            maker.left.equalToSuperview().inset(10)
        }
        
        label2.snp.makeConstraints { (maker) in
            maker.top.equalTo(label1.snp.top)
            maker.left.equalTo(label1.snp.right).offset(10)
            maker.right.lessThanOrEqualToSuperview().inset(10)
        }
    }
    
    //MARK: Actions
    @IBAction func createViewButtonClicked() {
        let view2 = R.nib.view22.firstView(owner: nil)
        if let view2 = view2 {
            self.view.addSubview(view2)
            view2.snp.makeConstraints({ (make) in
                make.width.equalTo(200)
                make.height.equalTo(100)
                make.centerX.centerY.equalToSuperview()
            })
        }
    }
    
    @IBAction func pushControllerButtonClicked() {
        let controller = R.storyboard.storyboard.vc2()
        if let controller = controller {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

