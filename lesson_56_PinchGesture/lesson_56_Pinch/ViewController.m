//
//  ViewController.m
//  lesson_56_Pinch
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
}

@end

@implementation ViewController

#pragma mark - Actions

- (IBAction)pich:(UIPinchGestureRecognizer *)sender {
    
    CGAffineTransform transform = CGAffineTransformScale(rect.transform, sender.scale, sender.scale);
    
    CGFloat newScale = sqrt(pow(transform.a, 2) + pow(transform.c, 2));
    if (newScale >= 1) {
        rect.transform = transform;
        sender.scale = 1;
    }
}

@end
