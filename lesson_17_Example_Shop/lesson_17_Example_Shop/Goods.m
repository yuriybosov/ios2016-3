//
//  Goods.m
//  lesson_17_Example_Shop
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Goods.h"

@implementation Goods

+ (instancetype)googsWithName:(NSString *)name
                        price:(CGFloat)price
                 categoryName:(NSString *)categoryName
                         tags:(NSArray<NSString *> *)tags
               specifications:(NSArray<NSString *> *)specifications {
    Goods *goods = [[Goods alloc] init];
    
    goods.name = name;
    goods.price = price;
    goods.categoryName = categoryName;
    goods.tags = tags;
    goods.specifications = specifications;
    
    return goods;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, $%0.2f, %@, теги: %@, характеристики: %@", _name, _price, _categoryName, [_tags componentsJoinedByString:@","], [_specifications componentsJoinedByString:@","]];
}

@end
