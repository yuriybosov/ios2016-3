//
//  Shop.m
//  lesson_17_Example_Shop
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Shop.h"

@interface Shop () {
    NSMutableDictionary *shopDictionary;
}

@end

@implementation Shop

- (instancetype)init
{
    self = [super init];
    if (self) {
        shopDictionary = [NSMutableDictionary new];
    }
    return self;
}

- (void)addGoods:(Goods *)goods {
    NSMutableArray *goodsArray = [shopDictionary objectForKey:goods.categoryName];
    if (goodsArray == nil) {
        goodsArray = [NSMutableArray new];
        [shopDictionary setObject:goodsArray forKey:goods.categoryName];
    }
    
    if (![goodsArray containsObject:goods]) {
        [goodsArray addObject:goods];
    }
}

- (void)removeGoods:(Goods *)goods {
    NSMutableArray *goodsArray = [shopDictionary objectForKey:goods.categoryName];
    if (goodsArray) {
        [goodsArray removeObject:goods];
        if (goodsArray.count == 0) {
            [shopDictionary removeObjectForKey:goods.categoryName];
        }
    }
}

- (void)showTotalInfo {
    NSLog(@"========================================");
    NSLog(@"Название магазина: %@", _name);

    CGFloat totalAmount = 0;
    NSUInteger totalCount = 0;

    for (NSArray *goodsArray in shopDictionary.allValues){
        totalCount += goodsArray.count;
        for (Goods *goods in goodsArray) {
            totalAmount += goods.price;
        }
    }
    //
    
    NSLog(@"Общая сумма товаров: %0.2f", totalAmount);
    NSLog(@"Общее кол-вл товаов: %lu", totalCount);
}

- (void)showAllCategoriesInfo {
    NSLog(@"========================================");
    if (shopDictionary.count) {
        
        NSArray *sortedCategories = [shopDictionary.allKeys sortedArrayUsingSelector:@selector(compare:)];
        
        for (NSString *categoryName in sortedCategories) {
            NSArray *goodsArray = [shopDictionary objectForKey:categoryName];
            NSLog(@"%@: %lu шт.", categoryName, goodsArray.count);
        }
    } else {
        NSLog(@"товаров нет");
    }
}

#pragma mark - Search

- (NSArray *)searchGoogdsByName:(NSString *)name {
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSArray *goodsArray in shopDictionary.allValues) {
        for (Goods *goods in goodsArray) {
            if ([goods.name rangeOfString:name options:NSCaseInsensitiveSearch].location != NSNotFound) {
                [result addObject:goods];
            }
        }
    }
    
    return result;
}

- (NSArray *)searchGoogdsByCategory:(NSString *)categoryName {
    NSArray *result = [shopDictionary objectForKey:categoryName];
    return  result;
}

- (NSArray *)searchGoogdsByTag:(NSString *)searchTag {
    NSMutableArray *result = [NSMutableArray new];
    
    for (NSArray *goodsArray in shopDictionary.allValues) {
        for (Goods *goods in goodsArray) {
            for (NSString *tag in goods.tags) {
                // проверим совпадение тегов, приведя теги к эдному регистру
                if ([tag.lowercaseString isEqualToString:searchTag.lowercaseString]) {
                    [result addObject:goods];
                    break;
                }
            }
        }
    }
    
    return result;
}

- (NSArray *)searchGoogdsBySpecifications:(NSArray *)specifications {
    NSMutableArray *result = [NSMutableArray new];
    
    for (NSArray *goodsArray in shopDictionary.allValues) {
        for (Goods *goods in goodsArray){
            BOOL isFound = NO;
            for (NSString *spec in specifications) {
                if ([goods.specifications containsObject:spec]){
                    isFound = YES;
                } else {
                    isFound = NO;
                }
                
                if (!isFound) {
                    break;
                }
            }
            
            if (isFound) {
                [result addObject:goods];
            }
        }
    }
        
    return result;
}

@end
