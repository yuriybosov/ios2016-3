//
//  Goods.h
//  lesson_17_Example_Shop
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Goods : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) CGFloat price;
@property (nonatomic, strong) NSString *categoryName;

@property (nonatomic, strong) NSArray<NSString *> *tags;
@property (nonatomic, strong) NSArray<NSString *> *specifications;

+ (instancetype)googsWithName:(NSString *)name
                        price:(CGFloat)price
                 categoryName:(NSString *)categoryName
                         tags:(NSArray<NSString *>*)tags
               specifications:(NSArray<NSString *>*)specifications;

@end
