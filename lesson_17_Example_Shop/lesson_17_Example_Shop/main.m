//
//  main.m
//  lesson_17_Example_Shop
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Goods.h"
#import "Shop.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Goods *goods1 = [Goods googsWithName:@"Холодильник АТЛАНТ"
                                       price:150.5
                                categoryName:@"Бытовая техника"
                                        tags:@[@"холод",@"лед",@"мороженое",@"холодец"]
                              specifications:@[@"AAA",@"240 литров",@"nofrost"]];
        
        Goods *goods2 = [Goods googsWithName:@"Холодильник NORD"
                                       price:170.5
                                categoryName:@"Бытовая техника"
                                        tags:@[@"холод",@"лед",@"мороженое",@"премиум"]
                              specifications:@[@"AAA",@"240 литров",@"black"]];
        
        Goods *goods3 = [Goods googsWithName:@"iPhone 7 plus"
                                       price:1000
                                categoryName:@"Средства связи"
                                        tags:@[@"Apple",@"телефон",@"премиум"]
                              specifications:@[@"256 gb",@"black"]];
        
        Shop *shop = [Shop new];
        shop.name = @"Мой магазин";
        
        [shop addGoods:goods1];
        [shop addGoods:goods2];
        [shop addGoods:goods3];
        
        [shop showTotalInfo];
        [shop showAllCategoriesInfo];
        
        [shop removeGoods:goods1];
        
        [shop showTotalInfo];
        [shop showAllCategoriesInfo];
        
        // search
        NSArray *search1 = [shop searchGoogdsByName:@"7"];
        if (search1.count > 0) {
            NSLog(@"search1 = \n%@", [search1 componentsJoinedByString:@"\n"]);
        }
        
        NSArray *rearch2 = [shop searchGoogdsByCategory:@"Бытовая техника"];
        if (rearch2.count > 0) {
            NSLog(@"rearch2 = \n%@", [rearch2 componentsJoinedByString:@"\n"]);
        }
        
        NSArray *search3 = [shop searchGoogdsByTag:@"Премиум"];
        if (search3.count > 0) {
            NSLog(@"search3 = \n%@", [search3 componentsJoinedByString:@"\n"]);
        }
        
        NSArray *search4 = [shop searchGoogdsBySpecifications:@[@"black",@"aaa"]];
        if (search4.count > 0) {
            NSLog(@"search4 = \n%@", [search4 componentsJoinedByString:@"\n"]);
        }
    }
    return 0;
}
