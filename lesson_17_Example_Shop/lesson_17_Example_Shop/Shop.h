//
//  Shop.h
//  lesson_17_Example_Shop
//
//  Created by Yurii Bosov on 1/29/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Goods.h"

@interface Shop : NSObject

@property (nonatomic, strong) NSString *name;

- (void)addGoods:(Goods *)goods;
- (void)removeGoods:(Goods *)goods;

- (void)showTotalInfo;
- (void)showAllCategoriesInfo;

- (NSArray *)searchGoogdsByName:(NSString *)name;
- (NSArray *)searchGoogdsByCategory:(NSString *)categoryName;
- (NSArray *)searchGoogdsByTag:(NSString *)searchTag;
- (NSArray *)searchGoogdsBySpecifications:(NSArray *)specifications;

@end
