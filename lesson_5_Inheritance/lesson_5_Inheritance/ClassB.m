//
//  ClassB.m
//  lesson_5_Inheritance
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ClassB.h"

@implementation ClassB

- (void)method1 {
    // в классе-наследнике доступны все public и protected переменные
    // public
    value1 = 100;
    value2 = 300;
    
    // protectes
    value3 = 10;
    value4 = 13;
    
    // private переменные не доступты в классах-наследниках
    
    // доспут к проперти, которое объявлено в родительском классе
    // можем получить доступ 2-я способами:
    self.property1 = 30.2; // через "точку"
    [self setProperty1:24.1]; // через setter\getter
}

// переопредение метода - полностью игнорим реализацией родительского метода
- (void)testMethod1 {
    NSLog(@"вызво метода testMethod1 в класса В");
}

// расширение метода - в реализации медота вызывает метод родительского класса, тем самым мы расширяем изначальный функционал этого метода
- (void)testMethod2 {
    [super testMethod2];
    NSLog(@"вызов метода testMethod2 в класса В");
}

@end
