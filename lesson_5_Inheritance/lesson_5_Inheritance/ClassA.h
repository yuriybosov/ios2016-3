//
//  ClassA.h
//  lesson_5_Inheritance
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClassA : NSObject {
    @public // этот спецификатор указывает на то, что все ниже описаные переменные будут публичными (видные всем другим классам)
    NSInteger value1;
    NSInteger value2;
    
    @protected // этот спецификатор указывает на то, что все ниже описаные переменные будут "защищены", т.е. видны в самом классе и в классах наследниках. Поумолчанию все переменные класса - protected
    NSInteger value3;
    NSInteger value4;
    
    @private // этот спецификатор указывает на то, что все ниже описаыне перменные будут приватными, т.е. видны только в реализации это класса и больше нигде 
    NSInteger value5;
    NSInteger value6;
}

@property (nonatomic, assign) CGFloat property1;

- (void)method1;

- (void)testMethod0;
- (void)testMethod1;
- (void)testMethod2;

@end
