//
//  main.m
//  lesson_5_Inheritance
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassA.h"
#import "ClassB.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        ClassA *objA = [ClassA new];
        // public-переменные видны вне класса
        // доступ к переменным класс осуществляется через '->'
        objA->value1 = 10;
        objA->value2 = 20;
        
        objA.property1 = 0.01;
        
        ClassB *objB = [ClassB new];
        objB.property1 = 2.01;
        
        NSLog(@"-------------------");
        [objB testMethod0];
        NSLog(@"-------------------");
        [objB testMethod1];
        NSLog(@"-------------------");
        [objB testMethod2];
    }
    return 0;
}
