//
//  ClassB.h
//  lesson_5_Inheritance
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ClassA.h"

@interface ClassB : ClassA

@end
