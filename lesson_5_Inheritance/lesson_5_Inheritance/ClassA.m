//
//  ClassA.m
//  lesson_5_Inheritance
//
//  Created by Yurii Bosov on 12/4/16.
//  Copyright © 2016 comfy. All rights reserved.
//

#import "ClassA.h"

// создадим приватную (анонимную) категорию
@interface ClassA () {
    // в анонимной категории можно объявлять приватные переменные класса
    NSInteger privateValue1;
    NSInteger privateValue2;
}

// в анонимной категории можно объявлять приватные свойства класса
@property (nonatomic, assign) BOOL privateProperty1;
@property (nonatomic, assign) CGFloat privateProperty2;

// в анонимной категории можно объявлять приватные методы класса
- (void)privateMethod1;
- (void)privateMethod2;

@end


@implementation ClassA

- (void)method1 {
    // доступ к public-переменным
    value1 = 100;
    value2 = 100;
    
    // доступ к protected-переменным
    value3 = 100;
    value4 = 100;
    
    // доступ к private-переменным
    value5 = 300;
    value6 = 300;
    
    // доспут к проперти, которое объявлено в этом же классе
    // можем получить доступ 3-я способами:
    _property1 = 40.4; // через переменну
    self.property1 = 30.2; // через "точку"
    [self setProperty1:24.1]; // через setter\getter
    
    [self privateMethod2];
//    [self privateMethod1];
}

- (void)testMethod0 {
    NSLog(@"вызов метода testMethod0 в класса А");
//    [self privateMethod1];
}

- (void)testMethod1 {
    NSLog(@"вызов метода testMethod1 в класса А");
//    [self privateMethod1];
}

- (void)testMethod2 {
    NSLog(@"вызов метода testMethod2 в класса А");
}

- (void)privateMethod1 {
    NSLog(@"вызов метода privateMethod1 в класса А");
}

- (void)privateMethod2 {
    NSLog(@"вызов метода privateMethod1 в класса А");
}

@end
