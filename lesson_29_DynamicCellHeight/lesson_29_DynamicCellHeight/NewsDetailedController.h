//
//  NewsDetailedController.h
//  lesson_29_DynamicCellHeight
//
//  Created by Yurii Bosov on 3/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"

@interface NewsDetailedController : UIViewController

@property (nonatomic, strong) NewsModel *news;

@end
