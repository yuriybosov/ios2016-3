//
//  NewsDetailedController.m
//  lesson_29_DynamicCellHeight
//
//  Created by Yurii Bosov on 3/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NewsDetailedController.h"

@interface NewsDetailedController () {
    IBOutlet UIImageView *logo;
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *textLabel;
}

@end

@implementation NewsDetailedController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Детальный экран новости";
    
    logo.image = [UIImage imageNamed:self.news.logoName];
    titleLabel.text = self.news.title;
    textLabel.text = self.news.fullText;
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
