//
//  NewsModel.m
//  lesson_29_DynamicCellHeight
//
//  Created by Yurii Bosov on 3/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NewsModel.h"

@implementation NewsModel

+(NSArray *)newsArray {
    NewsModel *news1 = [NewsModel new];
    news1.logoName = @"newsLogo1";
    news1.title = @"Украина предложит разделить списки заложников на категории - СБУ";
    news1.shortText = @"Завтра, 15 марта, на встрече трехсторонней контактной группы в Минске Украина предложит разделить списки заложников на четыре категории. Об этом LIGA.net сообщил советник главы СБУ Юрий Тандит";
    news1.fullText = @"Есть план эффективной реализации процесса освобождения украинских заложников, - сказал Тандит. Согласно данному плану, по его словам, определены четыре категории заложников: первая - это лица, которые установлены и дали согласие на общение с представителями международных организаций (ООН, Красного креста, - ред), вторая - лица, которые установлены, но не дали согласие.";
    
    NewsModel *news2 = [NewsModel new];
    news2.logoName = @"newsLogo2";
    news2.title = @"Рада намерена разрешить офицерам не заполнять декларации";
    news2.shortText = @"Предлагается разрешить не заполнять электронные декларации мобилизованным и контрактникам рядовым, сержантам, старшинам и офицерам младшего состава";
    news2.fullText = @"Верховная Рада намерена разрешить рядовым и младшим офицерам не заполнять электронные декларации. За проект изменений в статью 3 закона о недопущении коррупции 6172 в первом чтении проголосовали 239 депутатов, передает корреспондент LIGA.net.";
    
    NewsModel *news3 = [NewsModel new];
    news3.logoName = @"newsLogo3";
    news3.title = @"Укрпочта ввела две новые онлайн-услуги";
    news3.shortText = @"На сайте Укрпочты теперь можно перечислить средства с одной банковской карты на другую и оплатить коммунальные услуги";
    news3.fullText = @"Конкретнее, стали доступны возможности перечислить средства с карты на карту или осуществить оплату услуг он-лайн. Как сообщат компания, такие новшества стали возможны после начала партнерства с компанией Рortmone, оператора рынка онлайн-платежей и переводов В Украине.";
    
    return @[news1, news2, news3];
}

+ (NSArray *)newsArrayFromPlist{
    // 1. получить путь к файлу (указали имя файла, указали расширения файла)
    NSString *path = [[NSBundle mainBundle] pathForResource:@"newsData" ofType:@"plist"];
    
    // 2. создаем коллекцию, указав пусть к plist-файлу
    NSArray *plistData = [NSArray arrayWithContentsOfFile:path];
    
    // 3. с помощью цикла for in выполним "обход" всего массива, и создадим массив моделей
    NSMutableArray *result = [NSMutableArray array];
    for (NSDictionary *newsData in plistData) {
        NewsModel *model = [[NewsModel alloc] init];
        model.title = newsData[@"title"];
        model.shortText = newsData[@"shortText"];
        model.fullText = newsData[@"fullText"];
        model.logoName = newsData[@"logoName"];
        
        [result addObject:model];
    }
    
    // 4. вернули массив с моделями новостей
    return result;
}

@end
