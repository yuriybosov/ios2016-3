//
//  NewsModel.h
//  lesson_29_DynamicCellHeight
//
//  Created by Yurii Bosov on 3/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsModel : NSObject

@property (nonatomic, strong) NSString *logoName;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *shortText;
@property (nonatomic, strong) NSString *fullText;

+(NSArray *)newsArray;
+(NSArray *)newsArrayFromPlist;

@end
