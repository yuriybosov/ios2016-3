//
//  NewsCell.m
//  lesson_29_DynamicCellHeight
//
//  Created by Yurii Bosov on 3/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NewsCell.h"

@implementation NewsCell

- (void)setNews:(NewsModel *)news {
    _news = news;
    
    self.logo.image = [UIImage imageNamed:_news.logoName];
    self.title.text = _news.title;
    self.text.text = _news.shortText;
}

@end
