//
//  NewsCell.h
//  lesson_29_DynamicCellHeight
//
//  Created by Yurii Bosov on 3/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsModel.h"

@interface NewsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *logo;
@property (nonatomic, weak) IBOutlet UILabel *title;
@property (nonatomic, weak) IBOutlet UILabel *text;

@property (nonatomic, strong) NewsModel *news;

@end
