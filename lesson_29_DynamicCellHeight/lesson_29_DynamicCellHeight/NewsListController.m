//
//  NewsListController.m
//  lesson_29_DynamicCellHeight
//
//  Created by Yurii Bosov on 3/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NewsListController.h"
#import "NewsModel.h"
#import "NewsCell.h"
#import "NewsDetailedController.h"

@interface NewsListController () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *dataSources;
}

@end

@implementation NewsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Список новостей";
//    dataSources = [NewsModel newsArray];
    dataSources = [NewsModel newsArrayFromPlist];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsCell"];
    
    NewsModel *news = [dataSources objectAtIndex:indexPath.row];
    cell.news = news;
    
    return cell;
}

#pragma mark - UITableViewDelegate

// для того, что бы отображать динамическую высоту ячейки нужно:
// 1. корректно настроить констреинты
// 2. в методе tableView:estimatedHeightForRowAtIndexPath: вернуть значение UITableViewAutomaticDimension
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

// данные метод вызывается, когда в сторибоарде есть переход(segue)
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // segue - переход, содержит в себе котроллер с которого будет выполне передох и контроллер на который будет выполне переход (это destinationViewController)
    // sender - это тот объект, который вызвал данный переход. к примеру, если переход был назначен по клику на ячейку, то сендер буде как раз та ячейка по которой кликнули
    
    if ([segue.destinationViewController isKindOfClass:[NewsDetailedController class]] && [sender isKindOfClass:[NewsCell class]]) {
        
        NewsDetailedController *controller = segue.destinationViewController;
        NewsCell *cell = sender;
        
        controller.news = cell.news;
    }
}

@end
