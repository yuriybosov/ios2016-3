//
//  ViewController.m
//  lesson_51_Search_Example
//
//  Created by Yurii Bosov on 6/11/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate> {
    
    NSArray *originalDataSource;
    NSMutableArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Search string";
    
    originalDataSource = @[@"Yurii", @"Alex", @"Oleg", @"Dnmitrii", @"Diana", @"Yuliia", @"Kostya", @"Vitalic", @"Nataliia", @"Heorhii"];
    dataSource = [[NSMutableArray alloc] initWithArray:originalDataSource];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // сделаем смещение contentOffset на высоту searchBara, для того, что бы при первом показе этого экрана спрятать searchBar под навигейшен бар
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        self.tableView.contentOffset = CGPointMake(0, 44);
    });
}

- (void)filterDataSourceByFilter:(NSString *)filter {
    
    [dataSource removeAllObjects];
    
    // обновляем массив данных согласно фильтру
    if (filter.length == 0) {
        [dataSource addObjectsFromArray:originalDataSource];
    } else {
        // для фильтрации массива строк используем предикат
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", filter];
        [dataSource addObjectsFromArray:[originalDataSource filteredArrayUsingPredicate:predicate]];
    }
    
    // перегружаем таблицу, что бы отобразить измененные данные
    [self.tableView reloadData];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.searchBar setShowsCancelButton:YES
                                animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [self.searchBar setShowsCancelButton:NO
                                animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // filter data source and reload table view
    [self filterDataSourceByFilter:searchText];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

@end
