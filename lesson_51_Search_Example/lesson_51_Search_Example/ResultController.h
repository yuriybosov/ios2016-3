//
//  ResultController.h
//  lesson_51_Search_Example
//
//  Created by Yurii Bosov on 6/13/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultController : UITableViewController

- (void)reloadData:(NSArray *)data;

@end
