//
//  ResultController.m
//  lesson_51_Search_Example
//
//  Created by Yurii Bosov on 6/13/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ResultController.h"

@interface ResultController () {
    NSArray *dataSource;
}

@end

@implementation ResultController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)reloadData:(NSArray *)data {
    dataSource = data;
    NSLog(@"Result controller show new data %@", dataSource);
    // TODO show data source
    [self.tableView reloadData];
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID = @"cellID";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    // setup cell data
    cell.textLabel.text = dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%@", dataSource[indexPath.row]);
}

@end
