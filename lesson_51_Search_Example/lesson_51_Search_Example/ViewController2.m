//
//  ViewController2.m
//  lesson_51_Search_Example
//
//  Created by Yurii Bosov on 6/13/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController2.h"
#import "ResultController.h"

@interface ViewController2 () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate> {
    
    NSArray *originalDataSource;
    
    ResultController *resultController; // экран, который будет использован для отображения результатов поиска (удобным для вас способом)
    UISearchController *searchController;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Search string";
    
    originalDataSource = @[@"Yurii", @"Alex", @"Oleg", @"Dnmitrii", @"Diana", @"Yuliia", @"Kostya", @"Vitalic", @"Nataliia", @"Heorhii"];
    
    resultController = [[ResultController alloc] init];
    searchController = [[UISearchController alloc] initWithSearchResultsController:resultController];
    
    [searchController.searchBar sizeToFit];
    self.navigationItem.titleView = searchController.searchBar;
//    self.tableView.tableHeaderView = searchController.searchBar;
    
    searchController.searchBar.delegate = self;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // 1. фильтруем контент
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    NSArray *filterData = [originalDataSource filteredArrayUsingPredicate:predicate];
    
    // 2. передаем отфилтрованый контнт в экран отображения результата поиска
    [resultController reloadData:filterData];
    
    self.definesPresentationContext = YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return originalDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID" forIndexPath:indexPath];
    
    cell.textLabel.text = originalDataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

@end
