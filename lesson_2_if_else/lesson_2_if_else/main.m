//
//  main.m
//  lesson_2_if_else
//
//  Created by Yurii Bosov on 11/22/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // условный оператор if ()
        // if (<условие>)
        // {
        //      <код, который выполнится при истине>
        // }
        // else
        // {
        //      <код, который выполнится при лже>
        // }
    
        // операторы сравнение
        // >    больше
        // <    меньше
        // >=   больше либо равно
        // <=   меньше либо равно
        // ==   оператор раветсвто
        // !=   оператор не раветства
        // &&   логическое И
        // ||   логическое ИЛИ
        
        NSInteger i = 89;
        NSLog(@"i = %li", i);
        // if
        if (i < 100) {
            NSLog(@"переменная i меньше 100");
        }
        
        // if else
        // проверим на четность\нечестность
        if (i % 2 == 0) {
            NSLog(@"переменная i четная");
        } else {
            NSLog(@"переменная i НЕ четная");
        }
        
        // if else if else
        if ( i > 0)
        {
            NSLog(@"переменная i положительная");
        }
        else if ( i < 0)
        {
            NSLog(@"переменная i отрицательная");
        }
        else
        {
            NSLog(@"переменная i равна нулю");
        }
        
        // составные условия
        // пример на стоимость билета по возрасту
        NSUInteger age = 3;
        
        CGFloat price = 100;
        CGFloat sale = 1;
        
        //1
        NSLog(@"START");
        if ( age <= 6) {
            NSLog(@"вход свободный");
            sale = 1;
        } else if ( age > 6 && age <= 14) {
            NSLog(@"вход по 40%% скидке");
            sale = 0.4;
        } else if ( age > 14 && age < 55) {
            NSLog(@"вход по полной цене");
            sale = 0;
        } else {
            NSLog(@"вход по 50%% скинке (по пенсионному)");
            sale = 0.5;
        }
        
        
        price = price - (price * sale);
        NSLog(@"Цена билета %0.2f грн, скидка составляет %0.f %%", price,sale * 100);
        
        // тернарный оператор
        (i % 2 == 0) ? NSLog(@"Четное") : NSLog(@"Нечетное");
        // пример: вычисляем цену за обучение, в зависимости от того есть ли у меня свой макбук
        BOOL hasMacbook = YES;
        
        CGFloat priceiOSCources = hasMacbook ? 90 : 100;
        NSLog(@"priceiOSCources $%0.2f", priceiOSCources);
        
        //пример сокращенного тернарного оператора
        NSInteger x = 11;
        x = x ?: 100;
        NSLog(@"x = %li",x);
    }
    return 0;
}
