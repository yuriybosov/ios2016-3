//
//  ViewController.swift
//  lesson_85_Localization_Swift
//
//  Created by Yurii Bosov on 11/7/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // пример color-литерал
        self.view.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
        
        // default used NSLocalizedString
        self.title = NSLocalizedString("Hello", comment: "")
        //  R.swift
        self.title = R.string.localizable.hello()
        
        // default used NSLocalizedString
        self.label.text = NSLocalizedString("Hello, I am UILabel", comment: "")
        // used R.swift
        self.label.text = R.string.localizable.helloIAmUILabel()

        // default used NSLocalizedString
        self.button.setTitle(NSLocalizedString("Hello, I am UIButton", comment: ""), for: .normal)
        // used R.swift
        self.button.setTitle(R.string.localizable.helloIAmUIButton(), for: .normal)
    }
    
}

