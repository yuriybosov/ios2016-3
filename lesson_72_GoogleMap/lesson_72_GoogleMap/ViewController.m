//
//  ViewController.m
//  lesson_72_GoogleMap
//
//  Created by Yurii Bosov on 9/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface ViewController ()

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // поменять тип карты
    self.mapView.mapType = kGMSTypeTerrain;
    
    // настройка дополнительный кнопки в карте
    // 1. отобразить юзер локацию
    self.mapView.myLocationEnabled = YES;
    // 2. отобразить кнопку перейти на мою локаци
    self.mapView.settings.myLocationButton = YES;
    
    // 48.464717 , 35.046182
    // добавим маркер(пин) на карту с координатами города Днепр
    GMSMarker *marker = [GMSMarker new];
    marker.position = CLLocationCoordinate2DMake(48.464717,
                                                 35.046182);
    marker.title = @"Днепр";
    marker.snippet = @"Это не река, а город";
    marker.map = self.mapView;
    
    //48.443552, 35.013134
    // добавим маркер(пин) на карту с координатой дома
    marker = [GMSMarker new];
    marker.position = CLLocationCoordinate2DMake(48.443552,
                                                 35.013134);
    marker.title = @"Мой дом";
    marker.icon = [UIImage imageNamed:@"mapPin"];
    marker.map = self.mapView;
    
}

#pragma mark - Actions

- (IBAction)toDniproButtonClicked:(id)sender {
    // установить карту в опреденный регион (в г.Днепр)
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:48.464717 longitude:35.046182 zoom:12];
    [self.mapView animateToCameraPosition:cameraPosition];
}

- (IBAction)toHomeButtonClicked:(id)sender {
    GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:48.443552 longitude:35.013134 zoom:self.mapView.camera.zoom];
    [self.mapView animateToCameraPosition:cameraPosition];
}

@end
