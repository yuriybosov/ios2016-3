//
//  AppDelegate.h
//  lesson_72_GoogleMap
//
//  Created by Yurii Bosov on 9/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

