//
//  AppDelegate.m
//  lesson_72_GoogleMap
//
//  Created by Yurii Bosov on 9/24/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GMSServices provideAPIKey:@"AIzaSyCDZyMcAOqdImPE3DwztIoiVzfimYj3QVo"];
    
    return YES;
}

@end
