//
//  SectionHeader.h
//  lesson_28_1_Header_Footer_View
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeader : UIView

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

+ (SectionHeader *)create;

@end
