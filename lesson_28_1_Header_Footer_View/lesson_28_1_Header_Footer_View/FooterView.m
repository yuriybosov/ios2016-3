//
//  FooterView.m
//  lesson_28_1_Header_Footer_View
//
//  Created by Yurii Bosov on 3/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "FooterView.h"

@implementation FooterView

+ (FooterView *)create {
    FooterView *view = [[NSBundle mainBundle] loadNibNamed:@"FooterView" owner:nil options:nil].firstObject;
    return view;
}

@end
