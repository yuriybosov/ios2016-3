//
//  ViewController.m
//  lesson_25_UIImageView
//
//  Created by Yurii Bosov on 2/28/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIImageView *imageView1;
    IBOutlet UIImageView *imageView2;
    IBOutlet UIImageView *imageView3;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *img = [UIImage imageNamed:@"img2"];
    imageView1.image = img;
    imageView1.contentMode = UIViewContentModeScaleAspectFill;
    imageView1.clipsToBounds = YES; // обрезает картинку если она выходит за рамки imageView
}

@end
