//
//  ViewController.m
//  lesson_38_2_UITextField_AccessoryView
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "Toolbar.h"
#import "KeyboardView.h"

@interface ViewController () <KeyboardViewProtocol> {
    IBOutlet UITextField *textField;
    
    Toolbar *toolbar;
    KeyboardView *keyboardView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // toolbar settings
    // 1. создали toolbar (через наш статический метод)
    toolbar = [Toolbar create];
    
    // 2. назначили кнопке done обработчик на нажатие
    toolbar.doneButton.target = self;
    toolbar.doneButton.action = @selector(hideKeyboard);
    
    // 3. назначили полю ввода inputAccessoryView - это та вью, что отображается выше клавиатуры
    textField.inputAccessoryView = toolbar;
    
    // keyboadview settings
    // 1. создаем keyboardView (через наш статический метод)
    keyboardView = [KeyboardView create];
    // 2. назначаем делегата для keyboardView
    keyboardView.delegate = self;
    // 3. назначаем полю ввода inputView - это та вью, что отображается вместо клавиатуры!!!
    textField.inputView = keyboardView;
    
    // textfield settings
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.spellCheckingType = UITextSpellCheckingTypeNo;
    textField.keyboardType = UIKeyboardTypeEmailAddress;
    textField.returnKeyType = UIReturnKeyDone;
}

#pragma mark - Action

- (IBAction)hideKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - KeyboardViewProtocol

- (void)keyboardViewDidDeleteButtonClicked:(KeyboardView *)keyboardView {
    if (textField.text.length > 0) {
        textField.text = [textField.text substringToIndex:textField.text.length - 1];
    }
}

- (void)keyboardViewDidClearButtonClicked:(KeyboardView *)keyboardView {
    textField.text = nil;
}

- (void)keyboardView:(KeyboardView *)keyboardView didSelectSymbol:(NSString *)symbol {
    
    textField.text = [textField.text stringByAppendingString:symbol];
}

@end
