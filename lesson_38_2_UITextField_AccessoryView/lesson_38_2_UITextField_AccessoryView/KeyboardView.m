//
//  KeyboardView.m
//  lesson_38_2_UITextField_AccessoryView
//
//  Created by Yurii Bosov on 4/11/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "KeyboardView.h"

@implementation KeyboardView

+ (instancetype)create {
    return [[NSBundle mainBundle] loadNibNamed:@"KeyboardView" owner:nil options:nil].firstObject;
}

#pragma mark - Actions

- (IBAction)deleteButtonClicked:(id)sender{
    [self.delegate keyboardViewDidDeleteButtonClicked:self];
}

- (IBAction)clearAllButtonClicked:(id)sender{
    [self.delegate keyboardViewDidClearButtonClicked:self];
}

- (IBAction)symbolButtonClicked:(UIButton *)sender{
    
    // берем у кнопки ее тайтл
    NSString *symbol = [sender titleForState:UIControlStateNormal]; // ? ! # &
    
    [self.delegate keyboardView:self didSelectSymbol:symbol];
}

@end
