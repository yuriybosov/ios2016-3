//
//  Toolbar.m
//  lesson_38_2_UITextField_AccessoryView
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Toolbar.h"

@implementation Toolbar

+ (instancetype)create {
    return [[NSBundle mainBundle] loadNibNamed:@"Toolbar" owner:nil options:nil].firstObject;
}

@end
