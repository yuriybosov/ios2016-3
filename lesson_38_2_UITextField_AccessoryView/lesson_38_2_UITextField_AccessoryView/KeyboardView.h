//
//  KeyboardView.h
//  lesson_38_2_UITextField_AccessoryView
//
//  Created by Yurii Bosov on 4/11/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KeyboardView;

@protocol KeyboardViewProtocol <NSObject>

- (void)keyboardViewDidDeleteButtonClicked:(KeyboardView *)keyboardView;
- (void)keyboardViewDidClearButtonClicked:(KeyboardView *)keyboardView;
- (void)keyboardView:(KeyboardView *)keyboardView didSelectSymbol:(NSString *)symbol;

@end


@interface KeyboardView : UIView

@property (nonatomic, weak) id<KeyboardViewProtocol> delegate;

+ (instancetype)create;

@end
