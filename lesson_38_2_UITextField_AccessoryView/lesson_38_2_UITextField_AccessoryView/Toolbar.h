//
//  Toolbar.h
//  lesson_38_2_UITextField_AccessoryView
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Toolbar : UIToolbar

@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButton;

+ (instancetype)create;

@end
