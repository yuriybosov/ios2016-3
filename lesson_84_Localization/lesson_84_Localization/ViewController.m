//
//  ViewController.m
//  lesson_84_Localization
//
//  Created by Yurii Bosov on 11/7/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIButton *button;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.title = NSLocalizedString(@"Hello", nil);
    
    self.label.text = NSLocalizedString(@"Hello, I am Label", nil);
    
    [self.button setTitle:NSLocalizedString(@"Hello, I am Button", nil) forState:UIControlStateNormal];
}

@end
