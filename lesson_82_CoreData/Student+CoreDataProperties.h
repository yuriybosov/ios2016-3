//
//  Student+CoreDataProperties.h
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

#import "Student+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateBirthday;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nonatomic) BOOL gender;
@property (nullable, nonatomic, retain) Group *group;

@end

NS_ASSUME_NONNULL_END
