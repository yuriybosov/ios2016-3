//
//  DataManager.m
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 10/31/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import "DataManager.h"

@interface DataManager ()

@property (nonatomic, readonly) NSManagedObjectModel *objectModel;
@property (nonatomic, readonly) NSPersistentStoreCoordinator *coordinator;

@end


@implementation DataManager

@synthesize objectModel = _objectModel;
@synthesize coordinator = _coordinator;
@synthesize objectContext = _objectContext;

+ (instancetype)sharedInstace {
    static DataManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[DataManager alloc] init];
    });
    
    return manager;
}

- (void)save {
    @synchronized(self){
        if ([self.objectContext hasChanges]) {
            
            NSError *error = nil;
            
            [self.objectContext save:&error];
            
            if (error != nil) {
                NSLog(@"database save with error %@", error);
                abort();
            }
        }
    }
}

// инициализаця модели бд, контекста и координатора

- (NSManagedObjectModel *)objectModel {
    
    if (_objectModel == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"DataBase" ofType:@"momd"];
        NSURL *url = [NSURL fileURLWithPath:path];
        _objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:url];
    }
    
    return _objectModel;
}

- (NSPersistentStoreCoordinator *)coordinator {
    if (_coordinator == nil) {
        // 1. инициализация координатора, тут передаем objectModel
        _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.objectModel];
        
        // 2. получаем путь к папке 'NSDocumentDirectory'
        NSURL *directoryURL = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].firstObject;
        
        NSLog(@"directoryURL %@", directoryURL);
        
        // 3. создаем урл на файл, который будем использовать как хранилище.
        NSURL *url = [NSURL URLWithString:@"mydb.sqlite" relativeToURL:directoryURL];
        
        // для поддержки версионости базы данных нужно указать вот такие вот настройки политики миграции:
        NSDictionary *migratinPolicy = @{ NSMigratePersistentStoresAutomaticallyOption : @(YES),NSInferMappingModelAutomaticallyOption : @(YES)};
        
        // 4. создаем хранилище
        NSError *error = nil;
        NSPersistentStore *store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:url options:migratinPolicy error:&error];
        
        // 5. проверяем что хранилище было корректно создано
        if (store) {
            // если файл хранилища успешно был создан, то пометим этот файл (урл к нему), как файл, который не нужно включать в бекап
            [url setResourceValue:@(NO) forKey: NSURLIsExcludedFromBackupKey error:nil];
        } else {
            NSLog(@"database storage was created with error %@", error.localizedDescription);
            abort();
        }
    }
    
    return _coordinator;
}

- (NSManagedObjectContext *)objectContext {
    if (_objectContext == nil) {
        _objectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        
        [_objectContext setPersistentStoreCoordinator:self.coordinator];
    }
    
    return _objectContext;
}

@end
