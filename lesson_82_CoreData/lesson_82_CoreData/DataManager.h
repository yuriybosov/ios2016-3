//
//  DataManager.h
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 10/31/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *objectContext;

+ (instancetype)sharedInstace;

- (void)save;

@end
