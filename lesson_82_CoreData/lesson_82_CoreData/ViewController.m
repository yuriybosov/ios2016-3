//
//  ViewController.m
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 10/31/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import "ViewController.h"
#import "Group+CoreDataClass.h"
#import "Student+CoreDataClass.h"
#import "DataManager.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // если нет в бд групы - создать группу и доавить в нее несолько студентов
    // если группа есть - вывести инфу по ней
    
    NSFetchRequest *request = [Group fetchRequest];
    NSArray *groups = [[DataManager sharedInstace].objectContext executeFetchRequest:request error:nil];
    if (groups.count > 0) {
        // в бд есть группа, хотя бы одна
        Group *group = groups.firstObject;
        NSLog(@"Группа = %@", group.name);
        
        NSLog(@"Студенты группы = \n%@", [[group.students.allObjects valueForKey:@"firstName"] componentsJoinedByString:@"\n"]);
    } else {
        // в бд группы нет, добавим группу и в нее студентов
        // 1. создаем объект базы данных типа Group
        // 1.1. для этого нужно создать класс, который описыват эту сущность NSEntityDescription
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Group" inManagedObjectContext:[DataManager sharedInstace].objectContext];
        // 1.2 имею NSEntityDescription создаем сущность Group
        Group *group = [[Group alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:[DataManager sharedInstace].objectContext];
        group.name = @"iOS 2016-3";
        
        // 2. создание студентов и добавление их в группу
        NSEntityDescription *studentEntityDescription = [NSEntityDescription entityForName:@"Student" inManagedObjectContext:[DataManager sharedInstace].objectContext];
        
        Student *student1 = [[Student alloc] initWithEntity:studentEntityDescription insertIntoManagedObjectContext:[DataManager sharedInstace].objectContext];
        student1.firstName = @"Александр";
        student1.lastName = @"Баранов";
        student1.dateBirthday = [NSDate dateWithTimeIntervalSince1970:373593600];
        
        Student *student2 = [[Student alloc] initWithEntity:studentEntityDescription insertIntoManagedObjectContext:[DataManager sharedInstace].objectContext];
        student2.firstName = @"Наталья";
        student2.lastName = @"Диброва";
        student2.dateBirthday = [NSDate dateWithTimeIntervalSince1970:564624000];
        
        Student *student3 = [[Student alloc] initWithEntity:studentEntityDescription insertIntoManagedObjectContext:[DataManager sharedInstace].objectContext];
        student3.firstName = @"Александр";
        student3.lastName = @"Мошоровский";
        student3.dateBirthday = [NSDate dateWithTimeIntervalSince1970:575337600];
        
        Student *student4 = [[Student alloc] initWithEntity:studentEntityDescription insertIntoManagedObjectContext:[DataManager sharedInstace].objectContext];
        student4.firstName = @"Константин";
        student4.lastName = @"Пухкало";
        student4.dateBirthday = [NSDate dateWithTimeIntervalSince1970:615427200];
        
        Student *student5 = [[Student alloc] initWithEntity:studentEntityDescription insertIntoManagedObjectContext:[DataManager sharedInstace].objectContext];
        student5.firstName = @"Георгий";
        student5.lastName = @"Фарафонов";
        student5.dateBirthday = [NSDate dateWithTimeIntervalSince1970:872553600];
        
        // 3. нужно посвязывать сущности
        // 1-й вариант как связать (студенту назначаем группу)
        student1.group = group;
        // 2-й вариант как связать (группе назначаем студента)
        [group addStudentsObject:student1];
        // 3-й вариант как связать (группе назначаем множество студентов)
        [group setStudents:[NSSet setWithObjects:student1, student2, student3, student4, student5, nil]];
        
        
        // сохраняем все сущности, которые были добавлены в контекст
        [[DataManager sharedInstace] save];
    }
}

@end
