//
//  Student+CoreDataProperties.m
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

#import "Student+CoreDataProperties.h"

@implementation Student (CoreDataProperties)

+ (NSFetchRequest<Student *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Student"];
}

@dynamic dateBirthday;
@dynamic firstName;
@dynamic lastName;
@dynamic gender;
@dynamic group;

@end
