//
//  Group+CoreDataClass.h
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 10/31/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Student;

NS_ASSUME_NONNULL_BEGIN

@interface Group : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Group+CoreDataProperties.h"
