//
//  Student+CoreDataClass.h
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 10/31/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Group;

NS_ASSUME_NONNULL_BEGIN

@interface Student : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Student+CoreDataProperties.h"
