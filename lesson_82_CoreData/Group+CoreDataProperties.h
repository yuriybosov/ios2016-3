//
//  Group+CoreDataProperties.h
//  lesson_82_CoreData
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

#import "Group+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Group (CoreDataProperties)

+ (NSFetchRequest<Group *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) double testAttribute;
@property (nullable, nonatomic, retain) NSSet<Student *> *students;

@end

@interface Group (CoreDataGeneratedAccessors)

- (void)addStudentsObject:(Student *)value;
- (void)removeStudentsObject:(Student *)value;
- (void)addStudents:(NSSet<Student *> *)values;
- (void)removeStudents:(NSSet<Student *> *)values;

@end

NS_ASSUME_NONNULL_END
