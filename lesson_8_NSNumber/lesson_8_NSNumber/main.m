//
//  main.m
//  lesson_8_NSNumber
//
//  Created by Yurii Bosov on 12/13/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // NSNumber - класс для работы с числами
        
        // 1. методы создание переменной NSNumber
        NSNumber *num1 = [NSNumber numberWithInteger:10];
        NSLog(@"num1 = %@", num1);
        
        num1 = [NSNumber numberWithBool:NO];
        NSLog(@"num1 = %@", num1);
        
        num1 = [NSNumber numberWithFloat:22.33001];
        NSLog(@"num1 = %@", num1);
        
        num1 = [NSNumber numberWithUnsignedInteger:100000];
        NSLog(@"num1 = %@", num1);
        
        // 2. сокращенные методы по созданию NSNumber
        num1 = @(10);
        NSLog(@"num1 = %@", num1);
        
        num1 = @(NO);
        NSLog(@"num1 = %@", num1);
        
        num1 = @(200.12);
        NSLog(@"num1 = %@", num1);
        
        // 3. получение простых типо из NSNumber
        NSLog(@"%li", num1.integerValue);
        NSLog(@"%f", num1.floatValue);
        NSLog(@"%i", num1.boolValue);
        NSLog(@"%lu", num1.unsignedIntegerValue);
        
        // сравнение NSNumber
        // 1. по значение
        NSNumber *num2 = @(200);
        
        if ([num1 isEqualToNumber:num2]) {
            NSLog(@"%@ равно %@", num1, num2);
        } else {
            NSLog(@"%@ не равно %@", num1, num2);
        }
        
        // 2. по конкретному типу
        if (num1.integerValue == num2.integerValue) {
            NSLog(@"%li равно %li", num1.integerValue, num2.integerValue);
        }
        
        // 3. сравнение c помощью compare:
        NSComparisonResult result = [num1 compare:num2];
        if (result == NSOrderedAscending) {
            NSLog(@"%@ меньше чем %@", num1, num2);
        } else if (result == NSOrderedDescending) {
            NSLog(@"%@ больше чем %@", num1, num2);
        } else {
            NSLog(@"%@ равно %@", num1, num2);
        }
    }
    return 0;
}
