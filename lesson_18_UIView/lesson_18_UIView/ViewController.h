//
//  ViewController.h
//  lesson_18_UIView
//
//  Created by Yurii Bosov on 2/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *rect;


@end

