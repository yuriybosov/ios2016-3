//
//  ViewController.m
//  lesson_18_UIView
//
//  Created by Yurii Bosov on 2/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

// viewDidLoad - вызывается один раз, в момент, когда загрузилась view этого контроллера
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // меняем цвет нашего rect-a
    self.rect.backgroundColor = [UIColor blueColor];
    // или таков вот метод, если нужно задать произвольный цвет, использую RGB-схему
//    self.rect.backgroundColor = [UIColor colorWithRed:250/255.f green:132/255.f blue:2/255.f alpha:0.5];
    
    // меняем прозрачность нашего rect-a c помощью свойства alpha
    // alpha будет применена и для дочерних subview
    // поэтоме данный вариант не очень хороший
//    self.rect.alpha = 0.5;
    // лучше менять alpha не у самого объекта а у его бг-цвета
    self.rect.backgroundColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
    
    // размеры вью
    // получение текущего размера вью
    CGRect frame = self.rect.frame;
    // задаем новый origin (координата верхнего левого угла)
    frame.origin = CGPointMake(0, 0);
    // задачем новый размер
    frame.size = CGSizeMake(30, 30);
    
    // переназначаем frame нашему rect-у
    self.rect.frame = frame;
    
    // второй способ - это полностью создать новый фрейм
    self.rect.frame = CGRectMake(0, 0, 30, 30);
    
    // вывод в NSLog переменной CGRect
    CGRect frame2 = self.view.frame;
    NSLog(@"размер view = %f,%f; %f,%f", frame2.origin.x, frame2.origin.y, frame2.size.width, frame2.size.height);
}

@end
