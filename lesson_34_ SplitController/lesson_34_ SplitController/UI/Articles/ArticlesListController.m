//
//  ArticlesListController.m
//  lesson_34_ SplitController
//
//  Created by Yurii Bosov on 4/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ArticlesListController.h"
#import "ArticleCell.h"

@implementation ArticlesListController

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleCell" forIndexPath:indexPath];
    return cell;
}

@end
