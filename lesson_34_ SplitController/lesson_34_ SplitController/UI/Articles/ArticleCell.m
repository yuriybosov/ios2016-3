//
//  ArticleCell.m
//  lesson_34_ SplitController
//
//  Created by Yurii Bosov on 4/2/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ArticleCell.h"

@implementation ArticleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
