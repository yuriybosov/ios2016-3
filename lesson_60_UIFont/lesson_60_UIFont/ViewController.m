//
//  ViewController.m
//  lesson_60_UIFont
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    
    IBOutlet UILabel *label4;
    IBOutlet UILabel *label5;
    
    IBOutlet UILabel *label6;
    IBOutlet UILabel *label7;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // системный обычный шрифт
    label1.font = [UIFont systemFontOfSize:20];
    label1.text = [NSString stringWithFormat:@"%@ - %0.f", label1.font.familyName, label1.font.pointSize];
    // системный жирный шрифт
    label2.font = [UIFont boldSystemFontOfSize:20];
    label2.text = [NSString stringWithFormat:@"%@ - %0.f", label2.font.familyName, label2.font.pointSize];
    // системный курсив шрифт
    label3.font = [UIFont italicSystemFontOfSize:20];
    label3.text = [NSString stringWithFormat:@"%@ - %0.f", label3.font.familyName, label3.font.pointSize];
    
    // примеры кастомных шрифтов, которые есть в iOS
    label4.font = [UIFont fontWithName:@"Zapfino" size:20];
    label4.text = [NSString stringWithFormat:@"%@ - %0.f", label4.font.familyName, label4.font.pointSize];
    
    label5.font = [UIFont fontWithName:@"Superclarendon-BlackItalic" size:20];
    label5.text = [NSString stringWithFormat:@"%@ - %0.f", label5.font.familyName, label5.font.pointSize];
    
    // примеры кастомных шрифтов, которых нет в iOS. они добавлялись отдельно как ресурсы приложения
    // для тех шрифтов, которых нет в iOS нужно выполнить ряд действий:
    // 1. добавить шрифт в проект (ttf или otf файл)
    // 2. в info.plist по ключую UIAppFonts добавить файлы новых шрифтов
    // 3. при создании шрифта указать корректно его familyName (к примур у нас был файл "Zen3Demo.ttf" но familyName был "Zen3"
    
    // 1. Zen3Demo
    label6.font = [UIFont fontWithName:@"Zen3" size:20];
    label6.text = [NSString stringWithFormat:@"%@ - %0.f", label6.font.familyName, label6.font.pointSize];
    // 2. Simpsonfont
    label7.font = [UIFont fontWithName:@"Simpsonfont" size:20];
    label7.text = [NSString stringWithFormat:@"%@   %0.f", label7.font.familyName, label7.font.pointSize];
}

@end
