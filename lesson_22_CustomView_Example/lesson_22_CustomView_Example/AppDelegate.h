//
//  AppDelegate.h
//  lesson_22_CustomView_Example
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

