//
//  ViewController.m
//  lesson_22_CustomView_Example
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "RadioGroupView.h"
#import "RadioGroupMultipleView.h"

@interface ViewController () <RadioGroupViewProtocol, RadioGroupMultipleViewProtocol> {
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // RadioGroupView
    NSArray *answer = @[@"ответ 1", @"ответ 2", @"ответ 3"];
    
    RadioGroupView *groupView = [[RadioGroupView alloc] initWithAnswers:answer];
    groupView.delegate = self;
    
    CGRect frame = groupView.frame;
    frame.origin.y = 20;
    frame.size.width = self.view.frame.size.width;
    groupView.frame = frame;
    
    [self.view addSubview:groupView];
    
    // RadioGroupMultipleView
    answer = @[@"ответ 1", @"ответ 2", @"ответ 3", @"ответ 4", @"ответ 5"];
    RadioGroupMultipleView *groupMultipleView = [[RadioGroupMultipleView alloc] initWithAnswers:answer];
    groupMultipleView.delegate = self;
    groupMultipleView.maxAnswerCount = 3;
    
    frame = groupMultipleView.frame;
    frame.origin.y = groupView.frame.origin.y + groupView.frame.size.height + 20;
    frame.size.width = self.view.frame.size.width;
    groupMultipleView.frame = frame;
    
    [self.view addSubview:groupMultipleView];
}

#pragma mark - RadioGroupViewProtocol

- (void)radioGroupViewDidSelectAnswer:(RadioGroupView *)groupView {
    NSLog(@"был выбран ответ №%lu", groupView.answerIndex + 1);
}

#pragma mark - RadioGroupMultipleViewProtocol

- (void)radioGroupMultipleViewDidChagedAnswer:(RadioGroupMultipleView *)groupMultipleView {
    
    NSArray *indexOfAnswers = [groupMultipleView indexOfAnswers];
    
    if (indexOfAnswers.count == 0) {
        NSLog(@"кол-во ответов равно ноль");
    } else if(indexOfAnswers.count == 1) {
        NSLog(@"был выбран ответ №%@", indexOfAnswers.firstObject);
    } else {
        NSLog(@"были выбраны ответы №№%@", [indexOfAnswers componentsJoinedByString:@","]);
    }
}

@end
