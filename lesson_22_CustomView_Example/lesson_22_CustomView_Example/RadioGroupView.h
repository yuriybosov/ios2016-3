//
//  RadioGroupView.h
//  lesson_22_CustomView_Example
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RadioGroupView;


@protocol RadioGroupViewProtocol <NSObject>

- (void)radioGroupViewDidSelectAnswer:(RadioGroupView *)groupView;

@end


@interface RadioGroupView : UIView {
    UIButton *selectedButton;
}

@property (nonatomic, readonly) NSUInteger answerIndex;
@property (nonatomic, weak) id<RadioGroupViewProtocol> delegate;

- (instancetype)initWithAnswers:(NSArray *)answers;

@end
