//
//  RadioGroupMultipleView.m
//  lesson_22_CustomView_Example
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "RadioGroupMultipleView.h"

static CGFloat buttonHeight = 44;

@implementation RadioGroupMultipleView

- (instancetype)initWithAnswers:(NSArray *)answers {
    
    CGRect frame = CGRectMake(0, 0, 320, answers.count * buttonHeight);
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        self.maxAnswerCount = answers.count;
        
        buttons = [NSMutableArray new];
        
        for (NSInteger i = 0; i < answers.count; i++) {
            
            // 1. определить frame кнопки
            CGRect frame = CGRectMake(0,
                                      i * buttonHeight,
                                      self.frame.size.width,
                                      buttonHeight);
            
            // 2. создать кнопку
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            // 2.1 зададим кнопке tag. по тегу мы будет отличать наши кнопки друг от друга
            btn.tag = i;
            
            // 3. задать кнопке title
            NSString *title = answers[i];
            [btn setTitle:title forState:UIControlStateNormal];
            
            // 4. задать кнопке image для двух состояний normal и selected
            UIImage *imgNormal = [UIImage imageNamed:@"pin"];
            UIImage *imgSelect = [UIImage imageNamed:@"pinActive"];
            [btn setImage:imgNormal forState:UIControlStateNormal];
            [btn setImage:imgSelect forState:UIControlStateSelected];
            
            // 4.1. задаем выравнивание контента кнопки по левой стороне
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            
            // 5. добавить кнопке action
            [btn addTarget:self action:@selector(answerButtonDidClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            // 6. задать кнопке frame
            btn.frame = frame;
            
            // 7. добавить кнопку на нашу view
            [self addSubview:btn];
            
            // 8. добавим кнопку в массив кнопок
            [buttons addObject:btn];
        }
    }
    
    return self;
}

// метод, который будет вызываться при нажатии на кнопку
- (void)answerButtonDidClicked:(UIButton *)sender {
    
    if (sender.selected) {
        sender.selected = NO;
        [self.delegate radioGroupMultipleViewDidChagedAnswer:self];
    } else if ([self indexOfAnswers].count < self.maxAnswerCount) {
        sender.selected = YES;
        [self.delegate radioGroupMultipleViewDidChagedAnswer:self];
    }
}

- (NSArray *)indexOfAnswers {

    NSMutableArray *result = [NSMutableArray array];
    
    for (UIButton *btn in buttons) {
        if (btn.selected) {
            [result addObject:@(btn.tag + 1)];
        }
    }
    
    return result;
}

@end
