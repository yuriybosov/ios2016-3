//
//  RadioGroupView.m
//  lesson_22_CustomView_Example
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "RadioGroupView.h"

// статическая перенная, котороя отвечает за высоту одной кнопки
static CGFloat buttonHeight = 44;

@implementation RadioGroupView

- (instancetype)initWithAnswers:(NSArray *)answers {

    CGRect frame = CGRectMake(0, 0, 320, answers.count * buttonHeight);
    
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor grayColor];
        
        _answerIndex = NSNotFound;
        
        for (NSInteger i = 0; i < answers.count; i++) {
            
            // 1. определить frame кнопки
            CGRect frame = CGRectMake(0,
                                      i * buttonHeight,
                                      self.frame.size.width,
                                      buttonHeight);
            
            // 2. создать кнопку
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            
            // 2.1 зададим кнопке tag. по тегу мы будет отличать наши кнопки друг от друга
            btn.tag = i;
            
            // 3. задать кнопке title
            NSString *title = answers[i];
            [btn setTitle:title forState:UIControlStateNormal];
            
            // 4. задать кнопке image для двух состояний normal и selected
            UIImage *imgNormal = [UIImage imageNamed:@"pin"];
            UIImage *imgSelect = [UIImage imageNamed:@"pinActive"];
            [btn setImage:imgNormal forState:UIControlStateNormal];
            [btn setImage:imgSelect forState:UIControlStateSelected];
            
            // 4.1. задаем выравнивание контента кнопки по левой стороне
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            
            // 5. добавить кнопке action
            [btn addTarget:self action:@selector(answerButtonDidClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            // 6. задать кнопке frame
            btn.frame = frame;
            
            // 7. добавить кнопку на нашу view
            [self addSubview:btn];
        }
    }
    
    return self;
}

// метод, который будет вызываться при нажатии на кнопку
- (void)answerButtonDidClicked:(UIButton *)sender {
    
    if (selectedButton != sender) {
        
        selectedButton.selected = NO;
        sender.selected = YES;
        selectedButton = sender;
        
        _answerIndex = selectedButton.tag;

//         при выборе ответа нужно как то оповещать класс-овнер о том что был сделан выбор. Это можно реализовать с помощью уведомлений (NSNotificationCenter) или протокола\делегата. Реализуем с помощью протокола\делегата
        
        [self.delegate radioGroupViewDidSelectAnswer:self];
    }
}

@end
