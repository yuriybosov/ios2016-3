//
//  RadioGroupMultipleView.h
//  lesson_22_CustomView_Example
//
//  Created by Yurii Bosov on 2/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RadioGroupMultipleView;

@protocol RadioGroupMultipleViewProtocol <NSObject>

- (void)radioGroupMultipleViewDidChagedAnswer:(RadioGroupMultipleView *)groupMultipleView;

@end


@interface RadioGroupMultipleView : UIView {
    NSMutableArray *buttons;
}

@property (nonatomic, assign) NSUInteger maxAnswerCount;
@property (nonatomic, weak) id<RadioGroupMultipleViewProtocol> delegate;

- (instancetype)initWithAnswers:(NSArray *)answers;
- (NSArray *)indexOfAnswers;

@end
