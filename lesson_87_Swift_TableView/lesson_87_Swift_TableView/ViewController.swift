//
//  ViewController.swift
//  lesson_87_Swift_TableView
//
//  Created by Yurii Bosov on 11/12/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    private var dataSource = ["A","B","C","D","E"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "TableView #87"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
        
        tableView.reloadData()
    }
    
    //MARK: Actions
    
    @IBAction func addCell() {
        dataSource.append("new cell")
        let indexPath = IndexPath(row: dataSource.count - 1,
                                  section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
    }
}

//MARK: UITableViewDataSource
extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath)
        
        cell.textLabel?.text = dataSource[indexPath.row]
        
        return cell
    }
}

//MARK: UITableViewDelegate
extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let actionDelete = UITableViewRowAction(style: .destructive, title: "Delete") { (_, indexPath) in
            
            self.dataSource.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        actionDelete.backgroundColor = .red
        
        let actionAdd = UITableViewRowAction(style: .default, title: "Add") { (_,_) in
            
        }
        actionAdd.backgroundColor = .blue
        
        return [actionDelete, actionAdd]
    }
    
}


