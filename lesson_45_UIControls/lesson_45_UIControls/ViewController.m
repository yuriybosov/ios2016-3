//
//  ViewController.m
//  lesson_45_UIControls
//
//  Created by Yurii Bosov on 5/30/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIStepper *myStepper;
    IBOutlet UISlider *mySlider;
    IBOutlet UISwitch *mySwitch;
    IBOutlet UISegmentedControl *mySegmented;
    IBOutlet UIProgressView *myProgress;
    
    IBOutlet UILabel *lbStepperValue;
    IBOutlet UILabel *lbSliderValue;
    IBOutlet UILabel *lbSwitchValue;
    
    IBOutlet UIActivityIndicatorView *activityView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // настройка контролов
    myStepper.wraps = YES;
    mySwitch.on = NO;
    mySlider.value = 50;
    
    // настройка values labels
    lbSliderValue.text = [NSString stringWithFormat:@"%.0f", mySlider.value];
    lbSwitchValue.text = mySwitch.on ? @"On" : @"Off";
    lbStepperValue.text = [NSString stringWithFormat:@"%.0f", myStepper.value];
    
    //
//    [mySegmented setSelectedSegmentIndex:1];
//    [self segmentedValueChanged:mySegmented];
}

#pragma mark - Actions

- (IBAction)stepperValueChanged:(UIStepper *)sender {
    lbStepperValue.text = [NSString stringWithFormat:@"%.0f", myStepper.value];
}

- (IBAction)sliderValueChanged:(UISlider *)sender {
    lbSliderValue.text = [NSString stringWithFormat:@"%.0f", mySlider.value];
}

- (IBAction)switchValueChanged:(UISwitch *)sender {
    lbSwitchValue.text = mySwitch.on ? @"On" : @"Off";
}

- (IBAction)startActivityClicked:(id)sender {
    [activityView startAnimating];
}

- (IBAction)stopActivityClicked:(id)sender {
    [activityView stopAnimating];
}

- (IBAction)segmentedValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0){
        self.view.backgroundColor = [UIColor redColor];
    } else if (sender.selectedSegmentIndex == 1){
        self.view.backgroundColor = [UIColor yellowColor];
    } else if (sender.selectedSegmentIndex == 2){
        self.view.backgroundColor = [UIColor greenColor];
    }
}

- (IBAction)addProgressButtonClicked:(id)sender {
    if (myProgress.progress < 0.5) {
        [myProgress setProgress:0.5 animated:YES];
    } else if (myProgress.progress < 1) {
        [myProgress setProgress:1 animated:YES];
    } else {
        [myProgress setProgress:0 animated:YES];
    }
}

@end
