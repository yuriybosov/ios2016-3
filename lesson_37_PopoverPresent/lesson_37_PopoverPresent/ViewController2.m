//
//  ViewController2.m
//  lesson_37_PopoverPresent
//
//  Created by Yurii Bosov on 4/4/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController2.h"

@implementation ViewController2

- (IBAction)hideButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// если нужно задать произвольный размер контроллера в момент, когда он отображается в поповере, то нужно реализовать этот метод и вернуть нужный вам размер
- (CGSize)preferredContentSize {
    return CGSizeMake(300, 300);
}

@end
