//
//  ViewController.m
//  lesson_37_PopoverPresent
//
//  Created by Yurii Bosov on 4/4/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"

@implementation ViewController

// в данном методе будем отображать контроллер в поповере
- (IBAction)showButtonClicked:(UIButton *)sender {
    
    // 1. создать контроллер который будем отображать
    ViewController2 *vc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController2"];
    
    // 2.задаем контроллеру, который хотим отобразить, modalPresentationStyle
    vc2.modalPresentationStyle = UIModalPresentationPopover;
    
    // 3. как только мы задали стить равный UIModalPresentationPopover, то у контроллера появляетмя не nil свойство popoverPresentationController
    UIPopoverPresentationController *ppc = vc2.popoverPresentationController;
    
    // 4. кастомизируем popoverPresentationController, а именно:
    // 4.1. направлениме стрелочки
    ppc.permittedArrowDirections = UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown;
    // 4.2. задаем вью, от которой нужно отобразится
    ppc.sourceView = sender;
    // 4.3. задаем рект, от которого нужно отобразится
    ppc.sourceRect = sender.bounds;
    
    ppc.backgroundColor = [UIColor yellowColor];
    ppc.delegate = self;
    
    // 5. отображаем контроллер
    [self presentViewController:vc2 animated:YES completion:nil];
}

#pragma mark - UIPopoverPresentationControllerDelegate

- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return NO;
}

@end
