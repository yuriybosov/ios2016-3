//
//  ViewController.h
//  lesson_37_PopoverPresent
//
//  Created by Yurii Bosov on 4/4/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPopoverPresentationControllerDelegate>


@end

