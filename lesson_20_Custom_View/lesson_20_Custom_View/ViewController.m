//
//  ViewController.m
//  lesson_20_Custom_View
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "MyView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    MyView *view = [[NSBundle mainBundle] loadNibNamed:@"MyView" owner:nil options:nil].firstObject;
    
    [self.view addSubview:view];
}

@end
