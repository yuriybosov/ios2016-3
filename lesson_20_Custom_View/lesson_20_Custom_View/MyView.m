//
//  MyView.m
//  lesson_20_Custom_View
//
//  Created by Yurii Bosov on 2/12/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyView.h"

@implementation MyView

#pragma mark - Init

// init даный метод вызовыется, в случае, когда view создается программно с помощью этого метода. Важно помнить, что перед вызовом этого метода система сама вызовет метод initWithFrame:
- (instancetype)init {
    
    self = [super init];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        self.backgroundColor = [UIColor yellowColor];
    }
    return self;
}

// initWithFrame: даный метод вызовыется, в случае, когда view создается программно с помощью этого метода, так же он вызывается перед вызом метода init
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        self.backgroundColor = [UIColor redColor];
    }
    return self;
}

// initWithCoder: - данный метод вызывается при создании view, если она была добавлена в storyboard-е или когда view загружается из nib-файла
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"%s", __PRETTY_FUNCTION__);
        self.backgroundColor = [UIColor greenColor];
    }
    return self;
}

#pragma mark - Nib Loading

// awakeFromNib данный метод вызывается после успешного создания view (либо в storyboard либа в nib-файле). Данные метод требует вызов родительского метода [super awakeFromNib]. Важно: в данном методе будут проинициализированы все IBOutlet-ы данные view (если они были)
- (void)awakeFromNib {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [super awakeFromNib];
}

@end
