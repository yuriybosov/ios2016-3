//
//  ViewController.m
//  lesson_46_StackView
//
//  Created by Yurii Bosov on 6/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIStackView *stackView;
    
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    label1.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set";
    label2.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView";
    
    UILabel *label = [UILabel new];
    label.numberOfLines = 0;
    label.text = @"!!! Called after the view controller's view is released and set to nil";
    [stackView addArrangedSubview:label];
    
    UIStackView *stackView2 = [[UIStackView alloc] init];
    stackView2.axis = UILayoutConstraintAxisHorizontal;
    stackView2.spacing = stackView.spacing;
    stackView2.alignment = UIStackViewAlignmentTop;
    [stackView addArrangedSubview:stackView2];
    
    label = [UILabel new];
    label.numberOfLines = 0;
    label.text = @"A vertical UIStackView will return its first view for -view For First Baseline Layout and its";
    [stackView2 addArrangedSubview:label];
    
    label = [UILabel new];
    label.numberOfLines = 0;
    label.text = @"These next 3 property allow the label to be autosized to fit a certain width by scaling the font size(s) by a scaling factor These next 3 property allow the label to be autosized to fit a certain width by scaling the font size(s) by a scaling factor";
    [stackView2 addArrangedSubview:label];
}

@end
