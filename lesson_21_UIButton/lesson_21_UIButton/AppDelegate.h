//
//  AppDelegate.h
//  lesson_21_UIButton
//
//  Created by Yurii Bosov on 2/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

