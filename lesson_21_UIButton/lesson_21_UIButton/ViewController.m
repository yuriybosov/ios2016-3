//
//  ViewController.m
//  lesson_21_UIButton
//
//  Created by Yurii Bosov on 2/14/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIButton *btn1;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //  создаем кастомную кнопку
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    // задаем кнопке заголовок
    [btn setTitle:@"Viber" forState:UIControlStateNormal]; // он же default
    
    // задаем цвет заголовка. для этого создадти цвет а потом его применим
    UIColor *color = [UIColor colorWithRed:100/255.f
                                     green:100/255.f
                                      blue:100/255.f
                                     alpha:1];
    [btn setTitleColor:color forState:UIControlStateNormal];
    
    // задаем шрифт.
    btn.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:20];
    
    // задаем bg-image. для этого создадим картинку UIImage и зададим ее для определеного состояния
    UIImage *img = [UIImage imageNamed:@"btnBkg"];
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    
    // задаем frame
    btn.frame = CGRectMake(30, 100, 316, 42);
    
    // добавляем на нашу кнопку action
    [btn addTarget:self
            action:@selector(btn1DidCliked:)
  forControlEvents:UIControlEventTouchUpInside];
    
    // добавим кнопку на вью
    [self.view addSubview:btn];
    
    //// настраем вторую кнопку
    btn = [UIButton buttonWithType:UIButtonTypeCustom];
    // название
    [btn setTitle:@"Title" forState:UIControlStateNormal];
    // цвет текста
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // задаем img
    [btn setImage:[UIImage imageNamed:@"pin"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"pinActive"] forState:UIControlStateSelected];
    
    // задаем выравние по левой сторое
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    // инсеты
    [btn setContentEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 7, 0, 0)];
    
    
    // frame
    btn.frame = CGRectMake(30, 210, 316, 42);
    // action
    [btn addTarget:self
            action:@selector(btn2DidClicked:)
  forControlEvents:UIControlEventTouchUpInside];
    
    img = [UIImage imageNamed:@"btnBkg"];
    [btn setBackgroundImage:img forState:UIControlStateNormal];
    
    // add to view
    [self.view addSubview:btn];
    
}

#pragma mark - Button

- (IBAction)btn1DidCliked:(id)sender{
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (IBAction)btn2DidClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
}

@end
