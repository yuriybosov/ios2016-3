//
//  MyClass.h
//  lesson_68_Blocks
//
//  Created by Yurii Bosov on 8/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^MyBlock)(BOOL b, NSInteger i, NSString *s);

@interface MyClass : NSObject

@property (nonatomic, copy) void (^block1)();
@property (nonatomic, copy) MyBlock block2;

- (void)test;
- (void)testWithBlock:(MyBlock)block;

@end
