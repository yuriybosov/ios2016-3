//
//  MyClass.m
//  lesson_68_Blocks
//
//  Created by Yurii Bosov on 8/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MyClass.h"

@implementation MyClass

- (void)dealloc {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

- (void)test {
    // перед вызовом блока лучше проверить что блок существует!!!
    if (self.block1) {
        self.block1();
    }
    
    if (self.block2) {
        self.block2(YES, 0, @"str");
    }
}

- (void)testWithBlock:(MyBlock)block {
    if (block) {
        block(NO, 10, @"haha");
    }
}

@end
