//
//  main.m
//  lesson_68_Blocks
//
//  Created by Yurii Bosov on 8/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // 1. пример блока, который ничего не возвращает и ничего не принимает
        // 1.1. объявление блока
        void (^block) (void);
        // 1.2. инициализация блока
        block = ^(void) {
            // код блока
            NSLog(@"!!!");
        };
        // 1.3 вызов блока
        block();
        
        // 2. пример блока, который ничего не возвращает, но принимает аргумент
        void (^blockWithOneParam)(NSInteger) = ^(NSInteger i) {
            if (i % 2 == 0) {
                NSLog(@"Число %li четное", i);
            } else {
                NSLog(@"Число %li нечетное", i);
            }
        };
        
        blockWithOneParam(10);
        blockWithOneParam(15);
        blockWithOneParam(123);
        
        // 3. пример блока, который возвращает и принимает аргументы
        NSInteger (^summ)(NSInteger, NSInteger, NSInteger) = ^(NSInteger a, NSInteger b, NSInteger c) {
            return a + b + c;
        };
        
        NSLog(@"summ %li", summ(10,20,30));
        NSLog(@"summ %li", summ(0,-20,20));
        
        // Блоки и переменные
        
        __block NSInteger i = 10;
        
        void (^testBlock)() = ^(){
            i += 1;
        };
        
        NSLog(@"i = %li", i);
        testBlock();
        NSLog(@"i = %li", i);
        
        // Блоки и кастомные классы
        MyClass *obj = [MyClass new];
        
        __weak typeof(obj) weakObj = obj;
        obj.block1 = ^(){
            NSLog(@"@@@");
            NSLog(@"%@", weakObj);
        };
        
        obj.block2 = ^(BOOL b, NSInteger i, NSString *s) {
            NSLog(@"!!!");
        };
        
        [obj testWithBlock:^(BOOL b, NSInteger i, NSString *s) {
            
        }];
        
        [obj test];
    }
    return 0;
}
