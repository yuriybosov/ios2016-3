//
//  main.m
//  lesson_3_OOP
//
//  Created by Yurii Bosov on 11/25/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

//======================================================================
//////////// объявление функций
// <тип возвращаемого значение> <имя функции> (аргументы);
// аргументы:
// - если функция ничего не принимает, то агрументов у нее нет
// - если функция принимает агрументы, то мы в объявлении функции в () пишем ТИП_АГРУМЕНТА ИМЯ_АРГУМЕНТА
// - если агрументов больше одного, то мы их разделяем запятыми

// пример обьявление функции, которая ничего не возвращает и ничего не принимает
void functionName1(void);

// пример обьявление функции, которая ничего не возвращает, но принимает один аргумент
void functionName2(NSUInteger argument);

// пример обьявление функции, которая ничего не возвращает, но принимает несколько аргументов
void functionName3(NSInteger arg1, CGFloat arg2, BOOL arg3);

// пример обьявление функции, которая возвращает целочисленный результат и принимает два агрумента
NSInteger functionName4(NSInteger arg1, NSInteger arg2);


// пример
BOOL isEvenNumber(NSInteger number);

//======================================================================
//////////// реализация функций
// пример реализации функции, которая ничего не возвращает и ничего не принимает
void functionName1(void) {
    // тело функции
    NSLog(@"вызов функции functionName1");
}

// пример реализации функции, которая ничего не возвращает, но принимает один аргумент
void functionName2(NSUInteger argument) {
    NSLog(@"вызов функции functionName2 c агрументом %lu", argument);
}

// пример реализации функции, которая ничего не возвращает, но принимает несколько аргументов
void functionName3(NSInteger arg1, CGFloat arg2, BOOL arg3) {
    NSLog(@"вызов функции functionName3 c агрументами %li, %f, %i", arg1, arg2, arg3);
}

// пример реализации функции, которая возвращает целочисленный результат и принимает два агрумента
NSInteger functionName4(NSInteger arg1, NSInteger arg2) {
    NSLog(@"вызов функции functionName4");
    
    NSInteger result = (arg1 + arg2) * 100;
    
    return result;
}

BOOL isEvenNumber(NSInteger number) {
    
//    // детальная реализация
//    BOOL result = NO;
//    NSInteger ostatok = number % 2;
//    if (ostatok == 0) {
//        // четное
//        result = YES;
//    } else {
//        // нечетное
//        result = NO;
//    }
//    return result;
    
//     сокращенная реализация
    return (number % 2) == 0;
}

// пример - функция, которая решает квадратное уравнение
// функция возвращает BOOL рещультат, YES - если решение было найдено, NO - если решение нет
// объявили функцию
BOOL definitionSquareEquation(CGFloat a, CGFloat b, CGFloat c);
// реализовали функцию
BOOL definitionSquareEquation(CGFloat a, CGFloat b, CGFloat c){
    
    BOOL result = NO;
    
    CGFloat x1 = 0;  // x1,x2 - это решение нашего уравнения
    CGFloat x2 = 0;
    
    // что бы определить, имеет ли квадратное уравнение решение, нужно найти его дискриминант
    CGFloat discriminant = pow(b,2) - 4*a*c;
    
    if (discriminant == 0) {
        // если дискриминант == 0, то есть одно решение квадратного ур-я
        result = YES;
        
        if (a != 0) {
            x1 = -b/2*a;
            NSLog(@"решение квадратного ур-ния равно %f", x1);
        } else if (b != 0) {
            x1 = -c/b;
            NSLog(@"решение квадратного ур-ния равно %f", x1);
        } else {
            NSLog(@"Ошибка, коэфициенты a,b,c заданы не корректно");
        }
        
    } else if (discriminant > 0){
        // если дискриминант > 0, то есть два решение квадратного ур-я
        result = YES;
        
        if (a != 0) {
            
            x1 = (-b + sqrt(discriminant))/(2*a);
            x2 = (-b - sqrt(discriminant))/(2*a);
            
            NSLog(@"Решение квадратного ур-ния x1 = %f, x2 = %f", x1, x2);
            
        } else if (b != 0) {
            x1 = -c/b;
            NSLog(@"решение квадратного ур-ния равно %f", x1);
        } else {
            NSLog(@"Ошибка, коэфициенты a,b,c заданы не корректно");
        }
        
    } else {
        // если дискриминант меньше нуля, решений квадратного ур-я нет
        result = NO;
        NSLog(@"Ошибка, дискриминант меньше нуля, решения нет!");
    }
    
    return result;
}

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // использование функций
        functionName1();
        functionName2(10);
        functionName3(20, 0.40, YES);
        
        NSInteger a = functionName4(20, 30);
        NSLog(@"a = %li", a);
        
        // пример использование функции, которое определяем четное ли число или нет
        NSInteger x = 12;
        BOOL b = isEvenNumber(x);
        if (b == YES){
            NSLog(@"число %li четное", x);
        } else {
            NSLog(@"число %li нечетное", x);
        }
        
        definitionSquareEquation(1, 6, 9);
        definitionSquareEquation(2, 4, 7);
        definitionSquareEquation(2, 3, 1);
    }
    return 0;
}
