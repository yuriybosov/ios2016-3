//
//  ModalViewController.m
//  lesson_36_ModalPresent
//
//  Created by Yurii Bosov on 4/4/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ModalViewController.h"

@implementation ModalViewController

- (IBAction)showButtonClicked:(id)sender {
    // создаем новый контроллер, который будем отображать модально
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"modalNavigation"];
    // для ipad сделать отображение модального окна в виде небольшого окна, расположеного по центру, тип перехода - плавное появление\исчезновение
    // проверим, что приложение запущено на ipad девайсе
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        // задаем тип отображение
        vc.modalPresentationStyle = UIModalPresentationFormSheet;
        // задаем тип перехода
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    
    // вызов метода, который модально отобразит контроллер
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)hideButtonClicked:(id)sender {
    // вызов метода, который скрывает модальное окно
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
