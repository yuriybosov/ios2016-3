//
//  ZooPark.h
//  lesson_12
//
//  Created by Yurii Bosov on 1/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Animal.h"

@interface ZooPark : NSObject {
    NSMutableArray *animalsArray;
}

@property (nonatomic, strong) NSString *name;

- (void)addAnimal:(Animal*)animal;
- (BOOL)removeAnimalByName:(NSString *)name;

@end
