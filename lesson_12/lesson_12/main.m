//
//  main.m
//  lesson_12
//
//  Created by Yurii Bosov on 1/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Animal.h"
#import "ZooPark.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Animal *tiger = [Animal createAnimalWithName:@"Tigra"
                                              symbol:@"🐅"
                                                 age:2];
        Animal *elephant = [Animal createAnimalWithName:@"Slon"
                                                 symbol:@"🐘"
                                                    age:3];
        
        ZooPark *zoo = [ZooPark new];
        zoo.name = @"Closed zoo";
        [zoo addAnimal:tiger];
        [zoo addAnimal:elephant];
        
        NSLog(@"%@", zoo);
    }
    return 0;
}
