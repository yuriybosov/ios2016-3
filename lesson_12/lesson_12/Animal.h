//
//  Animal.h
//  lesson_12
//
//  Created by Yurii Bosov on 1/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animal : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *symbol;
@property (nonatomic, assign) NSUInteger age;

+ (instancetype)createAnimalWithName:(NSString *)name
                              symbol:(NSString*)symbol
                                 age:(NSUInteger)age;

@end
