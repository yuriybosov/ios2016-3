//
//  ZooPark.m
//  lesson_12
//
//  Created by Yurii Bosov on 1/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ZooPark.h"

@implementation ZooPark

- (instancetype)init
{
    self = [super init];
    if (self) {
        animalsArray = [NSMutableArray new];
    }
    return self;
}

- (void)addAnimal:(Animal *)animal {
    if (![animalsArray containsObject:animal]) {
        [animalsArray addObject:animal];
    }
}

- (BOOL)removeAnimalByName:(NSString *)name {
    
#warning todo - add implementation
    
    return NO;
}

- (NSString *)description
{
    NSString *test = [animalsArray componentsJoinedByString:@"\n"];
    
    return [NSString stringWithFormat:@"\n%@\n%@", _name, test];
}

@end
