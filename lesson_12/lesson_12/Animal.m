//
//  Animal.m
//  lesson_12
//
//  Created by Yurii Bosov on 1/10/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Animal.h"

@implementation Animal

+ (instancetype)createAnimalWithName:(NSString *)name
                              symbol:(NSString*)symbol
                                 age:(NSUInteger)age {
    Animal *obj = [Animal new];
    
    obj.name = name;
    obj.symbol = symbol;
    obj.age = age;
    
    return obj;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ %@, age %lu", _symbol, _name, _age];
}

@end





