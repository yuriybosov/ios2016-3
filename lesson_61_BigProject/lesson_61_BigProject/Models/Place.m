//
//  Place.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 28.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "Place.h"

@implementation Place

+ (NSArray *)testData {
    
    Place *place1 = [Place new];
    place1.placeBigIcon = @"cell_bg_1";
    place1.title = @"Сильпо";
    place1.address = @"Dmytra Yavornytskoho, 101, Dnipropetrovs'k";
    place1.distance = @"1.2 км";
    place1.gradient = [GradientData randomGradient];
    
    Place *place2 = [Place new];
    place2.placeBigIcon = @"cell_bg_2";
    place2.title = @"Сильпо Сильпо Сильпо Сильпо Сильпо ";
    place2.address = @"Dmytra Yavornytskoho";
    place2.distance = @"888 м";
    place2.gradient = [GradientData randomGradient];
    
    Place *place3 = [Place new];
    place3.placeBigIcon = @"cell_bg_3";
    place3.title = @"Сильпо Сильпо Сильпо Сильпо Сильпо Сильпо Сильпо";
    place3.address = @"asdf asdf d dss dfsd sdfds sd sdf sd sdf ds fds fds dsfsd sdf sfsd";
    place3.distance = @"1";
    place3.gradient = [GradientData randomGradient];
    
    Place *place4 = [Place new];
    place4.placeBigIcon = @"cell_bg_1";
    place4.gradient = [GradientData randomGradient];
    place4.title = @"Сильпо Сильпо";
    
    return @[place1, place2, place3, place4];
}

@end
