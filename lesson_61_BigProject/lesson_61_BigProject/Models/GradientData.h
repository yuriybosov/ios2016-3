//
//  GradientData.h
//  Lesson_61_BigProject
//
//  Created by Yurii Bosov on 7/4/17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GradientData : NSObject

@property (nonatomic, strong) UIColor *start;
@property (nonatomic, strong) UIColor *end;

+ (instancetype)gradientForColor:(UIColor *)color;

+ (GradientData *)randomGradient;
+ (NSArray *)allGradients;

@end
