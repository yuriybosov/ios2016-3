//
//  MenuItem.h
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;

@interface MenuItem : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, assign) BOOL selected;

@property (nonatomic, strong) NSString *storyBoardName;
@property (nonatomic, strong) NSString *ControllerID;

@property (nonatomic, strong) UIViewController *contoller;

+ (instancetype)itemWithTitle:(NSString *)title
                         icon:(NSString *)icon
                   storyboard:(NSString *)storyboardName
                 controllerID:(NSString *)controllerID;

+ (NSArray *)listOfItems;
    
@end
