//
//  MenuItem.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

+ (instancetype)itemWithTitle:(NSString *)title
                         icon:(NSString *)icon
                   storyboard:(NSString *)storyboardName
                 controllerID:(NSString *)controllerID {
    
    MenuItem *item = [[MenuItem alloc]init];
    item.title = title;
    item.icon = icon;
    item.storyBoardName = storyboardName;
    item.ControllerID = controllerID;
    return item;
}

+ (NSArray *)listOfItems {
    
    MenuItem *item1 = [MenuItem itemWithTitle:nil icon:@"user" storyboard:@"ProfileStoryBoard" controllerID:nil];
    
    MenuItem *item2 = [MenuItem itemWithTitle:@"Лучшее" icon:nil storyboard:@"Top" controllerID:nil];
    
    MenuItem *item3 = [MenuItem itemWithTitle:@"Места" icon:nil storyboard:@"Places" controllerID:nil];
    
    MenuItem *item4 = [MenuItem itemWithTitle:@"События" icon:nil storyboard:@"Event" controllerID:nil];
    
    MenuItem *item5 = [MenuItem itemWithTitle:@"Кино" icon:nil storyboard:@"Cinema" controllerID:nil];
    
    MenuItem *item6 = [MenuItem itemWithTitle:@"Новости" icon:nil storyboard:@"News" controllerID:nil];
    
    MenuItem *item7 = [MenuItem itemWithTitle:@"Люди" icon:nil storyboard:@"People" controllerID:nil];
    
    MenuItem *item8 = [MenuItem itemWithTitle:@"Настройки" icon:nil storyboard:@"Settings" controllerID:nil];
    
    return @[item1, item2, item3, item4, item5, item6, item7, item8];
}

@end
