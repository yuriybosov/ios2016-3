//
//  GradientData.m
//  Lesson_61_BigProject
//
//  Created by Yurii Bosov on 7/4/17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "GradientData.h"

@implementation GradientData

+ (instancetype)gradientForColor:(UIColor *)color {
    
    GradientData *obj = [GradientData new];
    obj.start = color;
    obj.end = [color colorWithAlphaComponent:0.75];
    return obj;
}

+ (GradientData *)randomGradient {
    NSArray *array = [GradientData allGradients];
    uint32_t count = (uint32_t)array.count;
    NSUInteger index = arc4random_uniform(count);
    return array[index];
}

+ (NSArray *)allGradients {
    static NSArray *array;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        GradientData *gradient1 = [GradientData gradientForColor:[UIColor colorWithRed:50/255.f green:162/255.f blue:138/255.f alpha:1]];
        
        GradientData *gradient2 = [GradientData gradientForColor:[UIColor colorWithRed:158/255.f green:162/255.f blue:50/255.f alpha:1]];
        
        GradientData *gradient3 = [GradientData gradientForColor:[UIColor colorWithRed:99/255.f green:111/255.f blue:198/255.f alpha:1]];
        
        GradientData *gradient4 = [GradientData gradientForColor:[UIColor colorWithRed:179/255.f green:79/255.f blue:79/255.f alpha:1]];
        
        GradientData *gradient5 = [GradientData gradientForColor:[UIColor colorWithRed:57/255.f green:92/255.f blue:152/255.f alpha:1]];
        
        array = @[gradient1, gradient2, gradient3, gradient4, gradient5];
    });
    
    return array;
}

@end
