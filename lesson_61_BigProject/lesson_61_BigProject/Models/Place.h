//
//  Place.h
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 28.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GradientData.h"

@interface Place : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) NSString *placeIcon;
@property (nonatomic, strong) NSString *placeBigIcon;

@property (nonatomic, assign) NSInteger rate;

@property (nonatomic, strong) GradientData *gradient;

+ (NSArray *)testData;

@end
