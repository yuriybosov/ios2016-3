//
//  PeopleController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 03.07.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "PeopleController.h"

@interface PeopleController ()

@end

@implementation PeopleController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Люди";
    
    self.view.backgroundColor = [UIColor cyanColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
