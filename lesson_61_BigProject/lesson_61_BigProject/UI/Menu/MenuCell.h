//
//  MenuCell.h
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuItem.h"

@interface MenuCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *icon;
@property (nonatomic,weak) IBOutlet UILabel *title;
@property (nonatomic,weak) IBOutlet UIView  *selectedIndicator;

@property (nonatomic,strong) MenuItem *item;

@end
