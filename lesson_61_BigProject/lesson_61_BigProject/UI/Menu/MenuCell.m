//
//  MenuCell.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectedIndicator.hidden = YES;
}

-(void)setItem:(MenuItem *)item {
    _item = item;
    
    self.title.text = _item.title;
    self.icon.image = [UIImage imageNamed:_item.icon];
    
    if (_item.selected) {
        self.selectedIndicator.hidden = NO;
        self.title.textColor = [UIColor whiteColor];
    }else{
        self.selectedIndicator.hidden = YES;
        self.title.textColor = [UIColor colorWithRed:126.f/255.f green:126.f/255.f blue:126.f/255.f alpha:1];
    }
    
}

@end
