//
//  MenuController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "MenuController.h"
#import "MenuCell.h"

@interface MenuController () {
    
    MenuItem *selectedItem;
}

@end

@implementation MenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    [dataSource addObjectsFromArray:[MenuItem listOfItems]];
    
    selectedItem = dataSource[1];
    selectedItem.selected = YES;
    
    //Так как ячейки были созданы в отдельных nib-файлах то нужно их зарегестрировать в colectionView
    
    // 1
    UINib *nib = [UINib nibWithNibName:@"MenuCell1" bundle:nil];
    [self.colectionsView registerNib:nib forCellWithReuseIdentifier:@"cellID1"];
    
    // 2
    nib = [UINib nibWithNibName:@"MenuCell2" bundle:nil];
    [self.colectionsView registerNib:nib forCellWithReuseIdentifier:@"cellID2"];
    
    [self didSelectItem:selectedItem];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return [self.childViewControllers.lastObject preferredStatusBarStyle];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didSelectItem:(MenuItem *)item {
    
    UIViewController *controller = item.contoller;
    if (controller == nil) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:item.storyBoardName bundle:nil];
        if (item.ControllerID) {
            controller = [storyboard instantiateViewControllerWithIdentifier:item.ControllerID];
        }else{
            controller = [storyboard instantiateInitialViewController];
        }
        
        item.contoller = controller;
    }
    
    //перед добавлением нового дочернего контроллера нужно удалить старый
    
    UIViewController *prevController = self.childViewControllers.firstObject;
    [prevController removeFromParentViewController];
    [prevController.view removeFromSuperview];
   
    if ([controller isKindOfClass:[UINavigationController class]]) {
        [(UINavigationController *)controller popToRootViewControllerAnimated:NO];
    }
    
    [self addChildViewController:controller];
    [self.view addSubview:controller.view];
    
    controller.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - self.colectionsView.frame.size.height);
}

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MenuItem *item = dataSource[indexPath.row];
    MenuCell *cell = nil;
    
    if (item.icon == nil) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID1" forIndexPath:indexPath];
    }else{
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID2" forIndexPath:indexPath];
    }
    
    cell.item = item;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MenuItem *item = dataSource[indexPath.row];
    
    if (item.storyBoardName == nil) {
        return;
    }
    
    if (item != selectedItem) {
        selectedItem.selected = NO;
        item.selected = YES;
        selectedItem = item;
        [collectionView reloadData];
        
        [self didSelectItem:selectedItem];
    }
    
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}


#pragma mark - UICollectionViewDelegateFlowLayou

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //создать статическую ячейку, наполнить контент, высчитать ее размер исходя из ее контента и размера где ячейка отображается
    
    MenuItem *item = dataSource[indexPath.row];
    MenuCell *cell = nil;
    
    if (item.icon == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"MenuCell1" owner:nil options:nil].firstObject;
        
    }else{
        cell = [[NSBundle mainBundle] loadNibNamed:@"MenuCell2" owner:nil options:nil].firstObject;
    }
    cell.item = item;
    
    CGSize size = [cell systemLayoutSizeFittingSize:CGSizeMake(60, collectionView.frame.size.height)withHorizontalFittingPriority:UILayoutPriorityFittingSizeLevel verticalFittingPriority:UILayoutPriorityRequired];
    
    return CGSizeMake(size.width, size.height);
}


@end
