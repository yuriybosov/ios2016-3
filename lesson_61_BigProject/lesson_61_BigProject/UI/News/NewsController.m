//
//  NewsController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 03.07.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "NewsController.h"

@interface NewsController ()

@end

@implementation NewsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Новости";
    
    self.view.backgroundColor = [UIColor grayColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
