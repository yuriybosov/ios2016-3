//
//  BaseCollectionController.h
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "BaseController.h"

@interface BaseCollectionController : BaseController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    NSMutableArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UICollectionView *colectionsView;

@end
