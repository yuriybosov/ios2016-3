//
//  BaseTable.h
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "BaseController.h"

@interface BaseTableController : BaseController <UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end
