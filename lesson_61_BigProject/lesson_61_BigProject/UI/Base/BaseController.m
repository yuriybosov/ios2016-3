//
//  BaseControllerViewController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "BaseController.h"

@implementation BaseController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSLog(@"%@ init", NSStringFromClass(self.class));
    }
    return self;
}

- (void)dealloc {
    NSLog(@"%@ dealloc", NSStringFromClass(self.class));
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor blackColor];
    
    }

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Actions

- (IBAction)popController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)dismissController {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

@end
