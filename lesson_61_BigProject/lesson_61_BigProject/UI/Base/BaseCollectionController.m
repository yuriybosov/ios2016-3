//
//  BaseCollectionController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "BaseCollectionController.h"

@implementation BaseCollectionController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.colectionsView.dataSource = self;
    self.colectionsView.delegate = self;
    
    dataSource = [NSMutableArray new];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(NO, @"Override is subclass");
    return nil;
}

@end
