//
//  BaseControllerViewController.h
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 25.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseController : UIViewController

- (IBAction)popController;
- (IBAction)dismissController;

- (IBAction)hideKeyboard:(id)sender;

@end
