//
//  SettingsController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 03.07.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "SettingsController.h"

@interface SettingsController ()

@end

@implementation SettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Настройки";
    
    self.view.backgroundColor = [UIColor magentaColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
