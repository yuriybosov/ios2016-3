//
//  EventController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 03.07.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "EventController.h"

@interface EventController ()

@end

@implementation EventController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"События";
    
    self.view.backgroundColor = [UIColor blueColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
