//
//  TopControllerViewController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 28.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "TopControllerViewController.h"

@interface TopControllerViewController ()

@end

@implementation TopControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Лучшее";
    self.view.backgroundColor = [UIColor redColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
