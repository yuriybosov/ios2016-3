//
//  CinemaController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 03.07.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "CinemaController.h"

@interface CinemaController ()

@end

@implementation CinemaController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Кино";
    
    self.view.backgroundColor = [UIColor orangeColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
