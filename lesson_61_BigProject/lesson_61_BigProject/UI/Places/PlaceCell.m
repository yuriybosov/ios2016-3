//
//  PlaceCell.m
//  Lesson_61_BigProject
//
//  Created by ; Alexandr on 28.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "PlaceCell.h"

@implementation PlaceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.cornerRadius = 5;
    self.bgView.clipsToBounds = YES;
    
    self.distanceLabelBG.layer.cornerRadius = 5;
    self.distanceLabelBG.clipsToBounds = YES;
    
    gradient = [CAGradientLayer layer];
    [self.gradientView.layer addSublayer:gradient];
}

- (void)setPlace:(Place *)place {

    _place = place;
    self.bgImageView.image = [UIImage imageNamed:_place.placeBigIcon];
    
    gradient.colors = @[(id)_place.gradient.end.CGColor,
                        (id)_place.gradient.start.CGColor];
    gradient.frame = self.gradientView.bounds;
    
    self.titleLabel.text = _place.title;
    self.addressLabel.text = _place.address;
    self.distanceLabel.text = _place.distance;
    self.descriptionLabel.text = _place.descriptionText;
    
    self.distanceLabelBG.hidden = (_place.distance.length == 0);
}

@end
