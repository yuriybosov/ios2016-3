//
//  PlacesController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 28.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "PlacesController.h"
#import "PlaceCell.h"

@interface PlacesController () <UITextFieldDelegate> {
    IBOutlet UIView *textFieldBG;
    IBOutlet UITextField *searchTextField;
}

@end

@implementation PlacesController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Места";
    
    // data sources
    [dataSource addObjectsFromArray:[Place testData]];
    
    // настройка закруглений
    textFieldBG.layer.cornerRadius = textFieldBG.frame.size.height/2;
    textFieldBG.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textFieldBG.layer.borderWidth = 2;
    
    searchTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ПОИСК" attributes:@{NSForegroundColorAttributeName: [UIColor lightTextColor]}];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - Action

- (IBAction)searchTextFieldDidChangeText:(id)sender {
    NSLog(@"%@", searchTextField.text);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PlaceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceCell"];
    Place *place = dataSource[indexPath.row];
    
    cell.place = place;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"didSelectRowAtIndexPath");
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

@end
