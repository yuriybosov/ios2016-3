//
//  PlacesMapController.m
//  Lesson_61_BigProject
//
//  Created by Yurii Bosov on 7/4/17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "PlacesMapController.h"

@implementation PlacesMapController

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
