//
//  ProfileController.m
//  Lesson_61_BigProject
//
//  Created by Baranov Alexandr on 28.06.17.
//  Copyright © 2017 Baranov Alexandr. All rights reserved.
//

#import "ProfileController.h"

@interface ProfileController ()

@end

@implementation ProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Профиль";
    
    self.view.backgroundColor = [UIColor greenColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
