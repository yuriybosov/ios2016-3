//
//  ViewController.m
//  lesson_57_Rotation
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
}

@end

@implementation ViewController

- (IBAction)rotate:(UIRotationGestureRecognizer *)sender {
    CGAffineTransform transform = CGAffineTransformRotate(rect.transform, sender.rotation);
    sender.rotation = 0;
    rect.transform = transform;
}

@end
