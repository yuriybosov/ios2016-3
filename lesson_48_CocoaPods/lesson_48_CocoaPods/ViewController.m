//
//  ViewController.m
//  lesson_48_CocoaPods
//
//  Created by Yurii Bosov on 6/6/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import <CHRCardNumberMask.h>
#import <CHRPhoneNumberMask.h>
#import <CHRTextFieldFormatter.h>

@interface ViewController () <UITextFieldDelegate> {
    IBOutlet UITextField *textFieldCard;
    IBOutlet UITextField *textFieldPhone;
    
    CHRTextFieldFormatter *cardNumberFormatter;
    CHRTextFieldFormatter *phoneNumberFormatter;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // инитим форматеры (см. пример https://cocoapods.org/pods/CHRTextFieldFormatter)
    cardNumberFormatter = [[CHRTextFieldFormatter alloc] initWithTextField:textFieldCard mask:[CHRCardNumberMask new]];
    
    phoneNumberFormatter = [[CHRTextFieldFormatter alloc] initWithTextField:textFieldPhone mask:[CHRPhoneNumberMask new]];
}
    
#pragma mark - UITextFieldDelegate
    
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == textFieldPhone) {
        return [phoneNumberFormatter textField:textField shouldChangeCharactersInRange:range replacementString:string];
        
    } else if (textField == textFieldCard) {
        
        return [cardNumberFormatter textField:textField shouldChangeCharactersInRange:range replacementString:string];
        
    } else {
        return YES;
    }
}

@end
