//
//  AppDelegate.h
//  lesson_48_CocoaPods
//
//  Created by Yurii Bosov on 6/6/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

