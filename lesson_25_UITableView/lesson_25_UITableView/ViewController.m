//
//  ViewController.m
//  lesson_25_UITableView
//
//  Created by Yurii Bosov on 2/28/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *myTableView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // reloadData - этот метод обновит весь контент у таблицы
    [myTableView reloadData];
}

#pragma mark - UITableViewDataSource

// метод возвращает кол-во секций для таблицы
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// метод возвращает кол-во ячеек для секции, обязательно реализовывать
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 100;
}

// метод возвращает ячейку для определенного indexPath. indexPath хранит номер секции и номер ячейки
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID4"];
    
    cell.textLabel.text = [NSString stringWithFormat:@"row = %li", indexPath.row];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"section = %li", indexPath.section];
    
    cell.imageView.image = [UIImage imageNamed:@"img3"];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // убираем выделение с выбраной ячейки
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// установим новую высоту ячейки. по умолчанию высота ячеек равна 44 пункта
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 0;
    
    if (indexPath.row % 2 == 0) {
        height = 30;
    } else {
        height = 60;
    }
    
    return height;
}

@end
