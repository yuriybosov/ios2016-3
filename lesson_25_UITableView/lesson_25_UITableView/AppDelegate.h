//
//  AppDelegate.h
//  lesson_25_UITableView
//
//  Created by Yurii Bosov on 2/28/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

