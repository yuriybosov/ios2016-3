//
//  DrawView.m
//  lesson_65_UIBezierPath
//
//  Created by Yurii Bosov on 7/23/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "DrawView.h"

@implementation DrawView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor greenColor];
}

- (void)drawRect:(CGRect)rect {
    // 1. настройка цвета (контрура и заливки)
    [[UIColor redColor] setFill];
    [[UIColor yellowColor] setStroke];
    
    // 2. Рисование квадрата
    // 2.1 - квадрат с контуром
    UIBezierPath *path1 = [UIBezierPath bezierPathWithRect:CGRectMake(20, 20, 50, 50)];
    path1.lineWidth = 4;
    [path1 stroke];
    
    // 2.2 - квадрат с "заливкой"
    UIBezierPath *path2 = [UIBezierPath bezierPathWithRect:CGRectMake(90, 20, 50, 50)];
    [path2 fill];
    
    // 2.3 - квадрат с контуром и "заливкой"
    UIBezierPath *path3 = [UIBezierPath bezierPathWithRect:CGRectMake(160, 20, 50, 50)];
    path3.lineWidth = 4;
    [path3 fill];
    [path3 stroke];
    
    // 2.4 - квадрат c закругленными углами (все закругленные)
    UIBezierPath *path4 = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(230, 20, 50, 50) cornerRadius:10];
    [path4 fill];
    
    // 2.5 - квадрат c закругленными углами (можно указать какие именно закруглить)
    UIBezierPath *path5 = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(300, 20, 50, 50) byRoundingCorners:UIRectCornerTopLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(20, 10)];
    [path5 fill];
    
    // 3. круг
    UIBezierPath *path6 = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(20, 90, 50, 50)];
    [path6 fill];
    
    UIBezierPath *path7 = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(90, 90, 120, 50)];
    [path7 fill];
    
    // 4. сектор
    CGPoint point = CGPointMake(20, 160);
    
    UIBezierPath *path8 = [UIBezierPath bezierPathWithArcCenter:point radius:50 startAngle:0 endAngle: - M_PI * 1.5 clockwise:YES];
    [path8 addLineToPoint:point];
    [path8 closePath];
    
    [path8 fill];
    
    // 5. линия
    UIBezierPath *path9 = [UIBezierPath bezierPath];
    
    path9.lineWidth = 4;
    path9.lineJoinStyle = kCGLineJoinMiter;
    
    [path9 moveToPoint:CGPointMake(90, 160)];
    [path9 addLineToPoint:CGPointMake(210, 160)];
    [path9 addLineToPoint:CGPointMake(210, 210)];
    
    [path9 closePath];
    [path9 stroke];
    
    // 6. Кривые безье
    UIBezierPath *path10 = [UIBezierPath bezierPath];
    path10.lineWidth = 4;
    [path10 moveToPoint:CGPointMake(20, 400)];
    [path10 addCurveToPoint:CGPointMake(210, 400)
              controlPoint1:CGPointMake(10, 300)
              controlPoint2:CGPointMake(130, 300)];
    [path10 stroke];
    
    UIBezierPath *path11 = [UIBezierPath bezierPath];
    path11.lineWidth = 4;
    [path11 moveToPoint:CGPointMake(20, 600)];
    [path11 addQuadCurveToPoint:CGPointMake(210, 600)
                   controlPoint:CGPointMake(115, 500)];
    [path11 stroke];
}

@end
