//
//  ViewController.swift
//  lesson_81_Widget
//
//  Created by Yurii Bosov on 11/26/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import APIModul
import PKHUD

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD.show(.progress, onView: self.view)
        
        NetworkProvider.shared.getRandomJokes(count: 20) { (jokes, error) in
            
            if let array = jokes {
                print(array)
                HUD.show(HUDContentType.success)
                HUD.hide(afterDelay: 1.5)
            } else {
                let errorMessage = error?.localizedDescription ?? "что то пошло не так"
                HUD.show(.labeledError(title: errorMessage, subtitle: nil))
                HUD.hide(afterDelay: 3)
            }
            
        }
    }
    
    @IBAction func test(){
        print("\(arc4random_uniform(5))")
    }
    
}

