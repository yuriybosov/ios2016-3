//
//  TodayViewController.swift
//  Widget
//
//  Created by Yurii Bosov on 11/26/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import NotificationCenter
import APIModul

class TodayViewController: UIViewController, NCWidgetProviding {
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        fetchData()
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .compact {
            self.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 110)
        } else {
            
            let size = self.label.systemLayoutSizeFitting(CGSize(width: self.label.frame.size.width, height: 110), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
            self.preferredContentSize = size
        }
    }
    
    @IBAction func refreshButtonClicked() {
        fetchData()
    }
    
    private func fetchData() {
        NetworkProvider.shared.getRandomJoke { (model, error) in
            // show text
            if let model = model {
                self.label.attributedText = model.getHtmlText()
                self.label.font = UIFont.systemFont(ofSize: 14)
                
                if self.extensionContext?.widgetActiveDisplayMode == .expanded {
                    let size = self.label.systemLayoutSizeFitting(CGSize(width: self.label.frame.size.width, height: 110), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
                    self.preferredContentSize = size
                }
                
            } else {
                self.label.text = error?.localizedDescription
            }
            // resize widgeta???
        }
    }

}
