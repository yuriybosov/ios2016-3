//
//  JokeModel.swift
//  APIModul
//
//  Created by Yurii Bosov on 11/26/17.
//  Copyright © 2017 ios. All rights reserved.
//
import Mapper

public struct JokeModel {
    public let site: String
    public let name: String
    public let desc: String
    public let link: String?
    public let html: String
    
    public func getHtmlText() -> NSAttributedString? {
        var result: NSAttributedString?
        
        let data = html.data(using: String.Encoding.utf8)
        if let data = data {
            
            guard let attributedString = try? NSAttributedString(data: data,
                                                                 options: [.documentType: NSAttributedString.DocumentType.html,
                                                                           .characterEncoding: String.Encoding.utf8.rawValue],
                                                                 documentAttributes: nil) else {
                return nil
            }
            
            result = attributedString
        }
        return result
    }
}

extension JokeModel: Mappable {
    public init(map: Mapper) throws {
        site = try map.from("site")
        name = try map.from("name")
        desc = try map.from("desc")
        link = map.optionalFrom("link")
        html = try map.from("elementPureHtml")
    }
}
