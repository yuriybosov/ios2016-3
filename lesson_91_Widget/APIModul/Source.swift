//
//  Source.swift
//  APIModul
//
//  Created by Yurii Bosov on 11/26/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Mapper

public struct Source {
    public let site: String
    public let name: String
    public let url: String
    public let parsel: String
    public let encoding: String
    public let linkpar: String
    public let desc: String
}

extension Source: Mappable {
    public init(map: Mapper) throws {
        site = try map.from("site")
        name = try map.from("name")
        url = try map.from("url")
        parsel = try map.from("parsel")
        encoding = try map.from("encoding")
        linkpar = try map.from("linkpar")
        desc = try map.from("desc")
    }
}
