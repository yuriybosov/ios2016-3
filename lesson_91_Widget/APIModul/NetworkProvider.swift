//
//  NetworkProvider.swift
//  APIModul
//
//  Created by Yurii Bosov on 11/26/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Alamofire
import Mapper

public class NetworkProvider: Any {
    
    public static let shared = {
        return NetworkProvider()
    }()
    
    //MARK: - Source
    public func getAllSources(complite: @escaping(([Source]?, Error?)->())){
        //TODO: сделать запрос на получения списка источников
        // использовать запрос http://umorili.herokuapp.com/api/sources
    }
    
    //MARK: - Jokes
    public func getRandomJoke(complite: @escaping((JokeModel?, Error?)->())) {
        //запрос на получение одного случайного анекдота
        self.getRandomJokes(count: 20) { (data, error) in
            if let data = data {
                let randomIndex: Int = Int(arc4random_uniform(UInt32(data.count)))
                complite(data[randomIndex], nil)
                
            } else {
                complite(nil, error)
            }
        }
    }
    
    public func getRandomJokes(count: Int, complite: @escaping(([JokeModel]?, Error?)->())){
        // запрос на получение анекдотов (кол-во передается в параметре count)
        Alamofire.request("http://umorili.herokuapp.com/api/random", method: .get, parameters: ["num":count], encoding: URLEncoding.default, headers: nil).responseJSON { (data) in
            
            if let responceData = data.value as? NSArray {
                
                let models = JokeModel.from(responceData)
                
                if models == nil {
                    complite(nil, MapperError.customError(field: nil, message: "invalid data"))
                } else {
                    complite(models, nil)
                }
                
            } else {
                complite(nil, data.error)
            }
        }
    }
    
    public func getJokes(from source: Source, count: Int, complite: @escaping(([JokeModel]?, Error?)->())) {
        //TODO: сделать запрос на получение анекдотов из определеного источника (источник передается в параметре source, кол-во передается в параметре count)
    }
}
