//
//  ViewController.m
//  lesson_30_DynamicViewHeight
//
//  Created by Yurii Bosov on 3/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource> {
    IBOutlet UILabel *headerLabel;
    IBOutlet UILabel *footerLabel1;
    IBOutlet UILabel *footerLabel2;
    
    IBOutlet UIView *header;
    IBOutlet UIView *footer;
    
    IBOutlet UITableView *myTableView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    headerLabel.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set";
    footerLabel1.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set";
    footerLabel2.text = @"Called after the view has been loaded. For view controllers cre";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self resizeHeaderTableForWidth:self.view.frame.size.width];
    [self resizeFooterTableForWidth:self.view.frame.size.width];
}

- (void)resizeHeaderTableForWidth:(CGFloat)width {
    
    // получаем размер для фиксированой ширины
    CGSize size = [header systemLayoutSizeFittingSize:CGSizeMake(width, 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityFittingSizeLevel];
    
    // задаем новый frame
    header.frame = CGRectMake(0, 0, size.width, size.height);
    
}

-(void)resizeFooterTableForWidth:(CGFloat)width {
    
    // получаем размер для фиксированой ширины
    CGSize size = [footer systemLayoutSizeFittingSize:CGSizeMake(width, 0) withHorizontalFittingPriority:UILayoutPriorityRequired verticalFittingPriority:UILayoutPriorityFittingSizeLevel];
    
    // задаем новый frame
    footer.frame = CGRectMake(0, 0, size.width, size.height);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    cell.textLabel.text = [NSString stringWithFormat:@"%li", indexPath.row + 1];
    return cell;
}

#pragma mark - Rotation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [self resizeHeaderTableForWidth:size.width];
    [self resizeFooterTableForWidth:size.width];
}

@end
