//
//  NavigationController.m
//  lesson_30_DynamicViewHeight
//
//  Created by Yurii Bosov on 3/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end
