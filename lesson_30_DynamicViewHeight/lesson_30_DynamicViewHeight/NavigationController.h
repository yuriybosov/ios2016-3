//
//  NavigationController.h
//  lesson_30_DynamicViewHeight
//
//  Created by Yurii Bosov on 3/19/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController

@end
