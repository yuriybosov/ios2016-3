//
//  Weather.swift
//  lesson_90_Alamofire
//
//  Created by Yurii Bosov on 11/23/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Mapper

struct Weather {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

extension Weather: Mappable {
    init(map: Mapper) throws {
        id = try map.from("id")
        main = try map.from("main")
        description = try map.from("description")
        icon = try map.from("icon")
    }
}

