//
//  Forecast.swift
//  lesson_90_Alamofire
//
//  Created by Yurii Bosov on 11/23/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Mapper
import Alamofire

struct Forecast {
    let date: Date
    let clouds: Int?
    let windSpeed: Float?
    let windDeg: Float?
    let snow: Float?
    let rain: Float?
    
    let temperature: Temperature
    let weathers: [Weather]
}

extension Forecast: Mappable {
    init(map: Mapper) throws {
        date = try map.from("dt", transformation: dateFromTimestamp)
        
        clouds = map.optionalFrom("clouds.all")
        windSpeed = map.optionalFrom("wind.speed")
        windDeg = map.optionalFrom("wind.deg")
        snow = map.optionalFrom("snow.3h")
        rain = map.optionalFrom("rain.3h")
        
        temperature = try map.from("main")
        weathers = try map.from("weather")
    }
    
    static func getForecastFor(lat: Double,
                               log: Double,
                               completion: @escaping(((City, [Forecast])?, Error?)->())) {
        //
        
        let params = ["lat":lat,
                      "lon":log,
                      "APPID":"8fa5a291d2528b20deabd21e6d50842b",
                      "units":"metric",
                      "lang":"ua"] as [String : Any]
        
        Alamofire.request("http://api.openweathermap.org/data/2.5/forecast",
                          method: .get,
                          parameters: params,
                          encoding: URLEncoding.default,
                          headers: nil).responseJSON { (data) in
            if let error = data.error {
                completion(nil, error)
            } else {
                if let dict = data.value as? [String:Any] {
                    
                    guard let cityData = dict["city"] as? NSDictionary, let forecastData = dict["list"] as? NSArray else {
                        let maperError = MapperError.customError(field: nil, message: "invalid data")
                        completion(nil, maperError)
                        return
                    }
                    
                    guard let city = City.from(cityData), let forecasts = Forecast.from(forecastData) else {
                        let maperError = MapperError.customError(field: nil, message: "invalid data")
                        completion(nil, maperError)
                        return
                    }
                    
                    completion((city, forecasts), nil)
                    
                } else {
                    let maperError = MapperError.customError(field: nil, message: "invalid data")
                    completion(nil, maperError)
                }
            }
        }
    }
}

// пример кастомного парсинга
// в данном примере нам приходит таймштамп для времени (кол-во секунд с 1970). нам нужно таймштамп конвертануть в Date. для этого мы написали кастомный трансофрматион (см. пример документации https://cocoapods.org/pods/ModelMapper)
func dateFromTimestamp(object: Any?) throws -> Date {
    
    guard let timestamp = object as? Int else {
        throw MapperError.convertibleError(value: object, type: Date.self)
    }
    
    return Date(timeIntervalSince1970: TimeInterval(timestamp))
}
