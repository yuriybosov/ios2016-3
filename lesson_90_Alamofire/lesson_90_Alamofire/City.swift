//
//  City.swift
//  lesson_90_Alamofire
//
//  Created by Yurii Bosov on 11/23/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Mapper

struct City{
    let id: Int
    let name: String
}

extension City :Mappable {
    init(map: Mapper) throws {
        id = try map.from("id")
        name = try map.from("name")
    }
}
