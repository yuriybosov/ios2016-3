//
//  Temperature.swift
//  lesson_90_Alamofire
//
//  Created by Yurii Bosov on 11/23/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Mapper

struct Temperature {
    let temp: Float
    let tempMin: Float
    let tempMax: Float
    let pressure: Float
    let seaLevel: Float
    let grndLevel: Float
    let humidity: Float
    let tempKf: Float
}

extension Temperature: Mappable {
    init(map: Mapper) throws {
        temp = try map.from("temp")
        tempMin = try map.from("temp_min")
        tempMax = try map.from("temp_max")
        pressure = try map.from("pressure")
        seaLevel = try map.from("sea_level")
        grndLevel = try map.from("grnd_level")
        humidity = try map.from("humidity")
        tempKf = try map.from("temp_kf")
    }
}
