//
//  ViewController.swift
//  lesson_90_Alamofire
//
//  Created by Yurii Bosov on 11/21/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit
import PKHUD

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.dimsBackground = false
        HUD.allowsInteraction = false
    }
    
    private func fetchData() {
        
        // show HUD
        HUD.show(.progress)
        
        LocationTracker.shared.getMyLocation { (location, errorMessage) in
            
            if let location = location {
                // send request
                Forecast.getForecastFor(lat: location.coordinate.latitude, log: location.coordinate.longitude, completion: { (data, error) in
                    if let data = data {
                        print("data \(data)")
                        HUD.show(.success)
                        HUD.hide(afterDelay: 1)
                        
                    }
                    if let error = error {
                        print("error \(error)")
                        HUD.show(.labeledError(title: error.localizedDescription, subtitle: nil))
                        HUD.hide(afterDelay: 3)
                    }
                })
            }
            
            if let errorMessage = errorMessage {
                // hide HUD with error
                HUD.show(.labeledError(title: errorMessage, subtitle: nil))
                HUD.hide(afterDelay: 3)
            }
        }
    }
    
    @IBAction func retry() {
        fetchData()
    }
}

