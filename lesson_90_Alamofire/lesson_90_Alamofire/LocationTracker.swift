//
//  LocationTracker.swift
//  lesson_90_Alamofire
//
//  Created by Yurii Bosov on 11/21/17.
//  Copyright © 2017 ios. All rights reserved.
//

import CoreLocation

class LocationTracker: NSObject {
    
    private var manager = CLLocationManager()
    private var completion: ((CLLocation?, String?) -> ())?
    
    static let shared: LocationTracker = {
        let instance = LocationTracker()
        return instance
    }()
    
    func getMyLocation(completion: @escaping (CLLocation?, String?) -> ()) {
        
        self.completion = completion
        self.manager.delegate = self
        
        switch CLLocationManager.authorizationStatus() {
        
        case .notDetermined:
            self.manager.requestWhenInUseAuthorization()
            break
            
        case .authorizedWhenInUse, .authorizedAlways:
            self.manager.startUpdatingLocation()
            break
            
        case .denied, .restricted:
            self.completion?(nil, "Нет доступа к геолокации")
            self.completion = nil
            break
        }
    }
}

extension LocationTracker: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            self.manager.startUpdatingLocation()
        } else if status == .denied || status == .restricted  {
            self.manager.stopUpdatingLocation()
            self.completion?(nil, "Нет доступа к геолокации")
            self.completion = nil
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.manager.stopUpdatingLocation()
        self.completion?(locations.first , nil)
        self.completion = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.manager.stopUpdatingLocation()
        self.completion?(nil, error.localizedDescription)
        self.completion = nil
    }
}
