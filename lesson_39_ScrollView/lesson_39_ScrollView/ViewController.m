//
//  ViewController.m
//  lesson_39_ScrollView
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    IBOutlet UILabel *label4;
    
    IBOutlet NSLayoutConstraint *lb3_lb4;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    label1.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set. Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set. Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set. Called after the view has been loaded. For view controllers created in code, this is after -loadView.";
    
    label2.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set. Called after the view has been loaded.";
    
    label3.text = @"Called after the view has been loaded. For view controllers created in code, this is after -loadView. For view controllers unarchived from a nib, this is after the view is set. Called after the view has been loaded. For view controllers unarchived from a nib, this is after the view is set. Called after the view has been loaded.";
    
    label4.text = @"C a l l e d a f t er t h e vi ew ha s be en lo a d ed. Fo r vi e w co n t r o l l er";
    
    if (label3.text.length == 0 ||
        label4.text.length == 0) {
        
        lb3_lb4.constant = 0;
        
    } else {
        
        lb3_lb4.constant = 20;
    }
}

@end
