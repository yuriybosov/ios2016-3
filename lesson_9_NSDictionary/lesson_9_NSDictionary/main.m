//
//  main.m
//  lesson_9_NSDictionary
//
//  Created by Yurii Bosov on 12/18/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // NSDictionary - коллеция которая хранит пары ключ\значение. Ключи (NSSting или NSNumber) должны быть уникальными!!! Значение - любой NSObject.
        //  NSDictionary - не меняемый объект, добавить\удалить пары(ключ\значение) после инициализации объекта нельзя!!!
        
        // 1. Инициализация и наполнение содержимым
        // 1.1. инициализация словаря с одной парой ключ\значение
        NSDictionary *dict1 = [NSDictionary dictionaryWithObject:@"object1" forKey:@"key1"];
        NSLog(@"dict1 %@",dict1);
        // альтернативный вариант
        dict1 = @{@"key1":@"object1"};
        NSLog(@"dict1 %@",dict1);
        
        // 1.2. инициализация словаря с нескольками парами ключей\значений
        NSDictionary *dict2 = [NSDictionary dictionaryWithObjectsAndKeys:@"ob1",@"k1",@"ob2",@"k2",@"ob3",@"k3", nil];
        NSLog(@"dict2 %@",dict2);
        // альтернативный вариант
        dict2 = @{@"k1":@"ob1",
                  @"k2":@"ob2",
                  @"k3":@"ob3"};
        NSLog(@"dict2 %@",dict2);
        
        // 1.3. инициализация словаря с массивом ключей и массивом объектов
        NSDictionary *dict3 = [NSDictionary dictionaryWithObjects:@[@"ob1",@"ob2",@"ob3",@"ob4"] forKeys:@[@"k1",@"k2",@"k3",@"k4"]];
        NSLog(@"dict3 %@",dict3);
        
        // 1.4 инициализация словаря c помощью другого словаря
        NSDictionary *dict4 = [NSDictionary dictionaryWithDictionary:dict2];
        NSLog(@"dict4 %@", dict4);
        
        // 2. Получить кол-ва пар в словаре
        NSUInteger count = dict4.count;
        NSLog(@"кол-во пар равно %lu", count);
        
        // 3. Получить массив всех ключей
        NSArray *allKeys = dict4.allKeys;
        NSLog(@"allKeys %@", allKeys);
        
        // 4. Получить массив всех объектов
        NSArray *allValues = dict4.allValues;
        NSLog(@"Values %@", allValues);
        
        // 5. Получить объект по ключу
        NSString *key = @"k3";
        id obj = [dict4 objectForKey:key];
        if (obj) {
            NSLog(@"по ключу %@ находится объект %@", key, obj);
        } else {
            NSLog(@"по ключу %@ объект не найден", key);
        }
        
        // альтернативный вариант
        key = @"k5";
        obj = dict4[key];
        if (obj) {
            NSLog(@"по ключу %@ находится объект %@", key, obj);
        } else {
            NSLog(@"по ключу %@ объект не найден", key);
        }
    }
    return 0;
}
