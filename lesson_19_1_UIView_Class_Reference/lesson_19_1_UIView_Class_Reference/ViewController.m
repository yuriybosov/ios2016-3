//
//  ViewController.m
//  lesson_19_1_UIView_Class_Reference
//
//  Created by Yurii Bosov on 2/7/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    // 1 способ - это объявление IBOutlet переменной и связь этой переменной с вью в storyboard-e 
    IBOutlet UIView *view1;
    UIView *view2;
    UIView *view3;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 2 способ - это создание view программно
    // 2.1 - создали view
    view2 = [[UIView alloc] init];
    // 2.2. - задаем ей размер и позицию (frame)
    view2.frame = CGRectMake(20, 140, 100, 100);
    // 2.3. - нужно добавить созданую вью на родительскую вью c помощью метода addSubview: , где self.view - родительское вью, view2 - дочернее вью
    [self.view addSubview:view2];
    
    // 3 способ - это загрузка view из хib-файла
    // 3.1. загружаем вью с помощью метода loadNibNamed:owner:options:
    view3 = [[NSBundle mainBundle] loadNibNamed:@"View" owner:nil options:nil].firstObject;
    // 3.2. если нужно - то задаем этой вью новые размеры и позиционируем ее. В нашем примере меняем только origin, размер вью должен остаться таким, какой был задан в nib-файле
    view3.frame = CGRectMake(20,
                             260,
                             view3.frame.size.width,
                             view3.frame.size.height);
    // 3.3. нужно добавить созданую вью на родительскую вью
    [self.view addSubview:view3];
    
    // test (будем менять цвета вьюшкам)
    view1.backgroundColor = [UIColor blueColor];
    view2.backgroundColor = [UIColor yellowColor];
}

@end
