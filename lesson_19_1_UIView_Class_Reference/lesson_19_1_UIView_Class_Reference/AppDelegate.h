//
//  AppDelegate.h
//  lesson_19_1_UIView_Class_Reference
//
//  Created by Yurii Bosov on 2/7/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

