//
//  AppDelegate.h
//  lesson_70_HTTP_Request
//
//  Created by Yurii Bosov on 8/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

