//
//  Department.m
//  lesson_70_HTTP_Request
//
//  Created by Yurii Bosov on 8/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Department.h"

//name: "Южное отд., Отделение №30",
//state: "Днепропетровская",
//id: "2943",
//country: "Украина",
//city: "Днепропетровск",
//index: "49000",
//phone: "8(056)373-33-54, 373-33-56",
//email: "julija.tverdokhlebova@pbank.com.ua",
//address: "ул Титова 29-М"

@implementation Department

- (instancetype)initWithDictionary:(NSDictionary *)data{
    self = [super init];
    if (self) {
        self.name = data[@"name"];
        self.state = data[@"state"];
        self.ID = data[@"id"];
        self.country = data[@"country"];
        self.city = data[@"city"];
        self.index = data[@"index"];
        self.phone = data[@"phone"];
        self.email = data[@"email"];
        self.address = data[@"address"];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"ID = %@, Address = %@", _ID, _address];
}

@end
