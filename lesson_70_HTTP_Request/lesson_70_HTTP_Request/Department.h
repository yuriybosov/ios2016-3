//
//  Department.h
//  lesson_70_HTTP_Request
//
//  Created by Yurii Bosov on 8/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

//name: "Южное отд., Отделение №30",
//state: "Днепропетровская",
//id: "2943",
//country: "Украина",
//city: "Днепропетровск",
//index: "49000",
//phone: "8(056)373-33-54, 373-33-56",
//email: "julija.tverdokhlebova@pbank.com.ua",
//address: "ул Титова 29-М"

#import <Foundation/Foundation.h>

@interface Department : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *index;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *address;

- (instancetype)initWithDictionary:(NSDictionary *)data;

@end
