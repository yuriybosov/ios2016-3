//
//  ViewController.m
//  lesson_70_HTTP_Request
//
//  Created by Yurii Bosov on 8/22/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "Department.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Send request
    // 1. URL
    NSString *baseURLString = @"https://api.privatbank.ua";
    NSString *path = @"p24api/pboffice";
    NSString *params = [@"city=Днепро&json" stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLHostAllowedCharacterSet]];
    
    NSString *URLString = [NSString stringWithFormat:@"%@/%@?%@", baseURLString, path, params];
    
    NSURL *URL = [NSURL URLWithString:URLString];
    
    // 2. Создаем Data Task
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:URL completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        // 3. проверим что пришли данные или пришла ошибка
        if (data) { // если есть data - значить пришли какие то данные с сервера. Будем их парсить
            
            NSError *parsingError = nil;
            
            id value = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parsingError];
            
            if (parsingError != nil) {
                NSLog(@"Ошибка JSON-парсера %@", parsingError);
            } else {
                if ([value isKindOfClass:[NSArray class]]) {
                    
                    NSMutableArray *result = [NSMutableArray new];
                    
                    for (NSDictionary *modelData in value) {
                        //
                        Department *model = [[Department alloc] initWithDictionary:modelData];
                        
                        [result addObject:model];
                    }
                    NSLog(@"result %@", [result componentsJoinedByString:@"\n"]);
                    
                    // reloadData
                }
            }
        }
        
    }];
    
    [task resume];
}

@end
