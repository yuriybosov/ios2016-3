//
//  MainModel.swift
//  lesson_88_Swift_DropdownCell
//
//  Created by Yurii Bosov on 11/14/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

class MainModel: NSObject {
    
    let title: String
    let subModels: [SubModel]
    var isSelected = false
    
    
    init(title: String,
         subModels: [SubModel],
         isSelected: Bool = false){
        
        self.title = title
        self.subModels = subModels
        self.isSelected = isSelected
        super.init()
    }
    
    class func testData() -> [MainModel] {
        // model 1
        let array1 = [SubModel("Called after the view has"),
                      SubModel("Called after the view has been loaded. Called after the view has been loaded. "),
                      SubModel("Called after")]
        
        let model1 = MainModel(title: "Model 1",
                               subModels: array1)
        
        // model 2
        let array2 = [SubModel("Called after the view has Called after the view has Called after the view has Called after the view has"),
                      SubModel("Called after the view has been loaded. Called after the view has been loaded. Called after the view has Called after the view has Called after the view has")]
        let model2 = MainModel(title: "Model 2",
                               subModels: array2,
                               isSelected: true)
        
        // model 3
        let array3 = [SubModel("Called after the view has Called after the view has Called after the view has Called after the view has")]
        let model3 = MainModel(title: "Model 3",
                               subModels: array3)
        
        // result
        return [model1, model2, model3]
    }
}
