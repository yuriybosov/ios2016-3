//
//  SubModelCell.swift
//  lesson_88_Swift_DropdownCell
//
//  Created by Yurii Bosov on 11/14/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

class SubModelCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    private var _model: SubModel?
    
    var model: SubModel? {
        set {
            self._model = newValue
            self.titleLabel.text = self._model?.title
        }
        get {
            return self._model
        }
    }

}
