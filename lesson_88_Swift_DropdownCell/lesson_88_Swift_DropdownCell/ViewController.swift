//
//  ViewController.swift
//  lesson_88_Swift_DropdownCell
//
//  Created by Yurii Bosov on 11/14/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private let dataSource = MainModel.testData()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let model = dataSource[section]
        
        var count = 1
        if model.isSelected {
            count += model.subModels.count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "MainModelCell", for: indexPath)
            
            if let cell = cell as? MainModelCell {
                cell.setupModel(model: dataSource[indexPath.section])
            }
            
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "SubModelCell", for: indexPath)
            if let cell = cell as? SubModelCell {
                
                let mainModel = dataSource[indexPath.section]
                let subModel = mainModel.subModels[indexPath.row - 1]
                cell.model = subModel
            }
            
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            let model = dataSource[indexPath.section]
            model.isSelected = !model.isSelected
            
            let cell = tableView.cellForRow(at: indexPath)
            if let cell = cell as? MainModelCell {
                cell.setupModel(model: model)
            }
            
            var arrayOfIndexPath = [IndexPath]()
            for (index, _) in model.subModels.enumerated() {
                
                let path = IndexPath(row: index + 1,
                                     section: indexPath.section)
                arrayOfIndexPath.append(path)
            }
            
            if model.isSelected {
                tableView.insertRows(at: arrayOfIndexPath,
                                     with: .automatic)
            } else {
                tableView.deleteRows(at: arrayOfIndexPath,
                                     with: .automatic)
            }
        }
    }
    
}

