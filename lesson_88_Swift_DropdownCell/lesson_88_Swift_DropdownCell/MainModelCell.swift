//
//  MainModelCell.swift
//  lesson_88_Swift_DropdownCell
//
//  Created by Yurii Bosov on 11/14/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

class MainModelCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    func setupModel(model: MainModel) {
        titleLabel.text = model.title
        arrowImage.image = model.isSelected ? #imageLiteral(resourceName: "icUpArrow") : #imageLiteral(resourceName: "icDownArrow")
    }
}
