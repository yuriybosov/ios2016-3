//
//  SubModel.swift
//  lesson_88_Swift_DropdownCell
//
//  Created by Yurii Bosov on 11/14/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

class SubModel: NSObject {
    let title: String
    
    init(_ title: String) {
        self.title = title
        super.init()
    }
}
