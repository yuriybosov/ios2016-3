//
//  Model.h
//  lesson_42_2_PickerView_Example
//
//  Created by Yurii Bosov on 5/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject

@property (nonatomic, strong) NSString *name;

@end
