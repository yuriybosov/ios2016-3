//
//  Brand.h
//  lesson_42_2_PickerView_Example
//
//  Created by Yurii Bosov on 5/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"

@interface Brand : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray <Model *> *models;

+ (NSArray *)testData;

@end
