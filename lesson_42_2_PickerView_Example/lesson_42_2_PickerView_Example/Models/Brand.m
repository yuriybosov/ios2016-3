//
//  Brand.m
//  lesson_42_2_PickerView_Example
//
//  Created by Yurii Bosov on 5/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Brand.h"

@implementation Brand

+ (NSArray *)testData {
    
    // бренды
    Brand *brand1 = [Brand new];
    brand1.name = @"BMW";
    
    Brand *brand2 = [Brand new];
    brand2.name = @"VOLVO";
    
    Brand *brand3 = [Brand new];
    brand3.name = @"DEO";
    
    // модели
    Model *model11 = [Model new];
    model11.name = @"X1";
    Model *model12 = [Model new];
    model12.name = @"X2";
    Model *model13 = [Model new];
    model13.name = @"X3";
    Model *model14 = [Model new];
    model14.name = @"X4";
    Model *model15 = [Model new];
    model15.name = @"X5";
    
    brand1.models = @[model11, model12, model13, model14, model15];
    
    Model *model21 = [Model new];
    model21.name = @"v40";
    Model *model22 = [Model new];
    model22.name = @"v60";
    Model *model23 = [Model new];
    model23.name = @"vs90";
    
    brand2.models = @[model21, model22, model23];
    
    Model *model31 = [Model new];
    model31.name = @"Lanos";
    Model *model32 = [Model new];
    model32.name = @"Sens";
    
    brand3.models = @[model31, model32];
    
    return @[brand1, brand2, brand3];
}

@end
