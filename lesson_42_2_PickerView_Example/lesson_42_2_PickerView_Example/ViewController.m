//
//  ViewController.m
//  lesson_42_2_PickerView_Example
//
//  Created by Yurii Bosov on 5/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "Brand.h"

@interface ViewController () <UIPickerViewDataSource, UIPickerViewDelegate> {
    
    Brand *selectedBrand;
    Model *selectedModel;
    NSArray <Brand *> *allBrands;
}

@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    allBrands = [Brand testData];
    
    selectedBrand = allBrands.firstObject;
    selectedModel = selectedBrand.models.firstObject;
    
    self.textLabel.text = [NSString stringWithFormat:@"Brand: %@\nModel: %@", selectedBrand.name, selectedModel.name];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    NSInteger number = 0;
    
    if (component == 0) {
        number = allBrands.count;
    } else if (component == 1) {
        number = selectedBrand.models.count;
    }
    
    return number;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *str = nil;
    
    if (component == 0) {
        str = allBrands[row].name;
    } else if (component == 1) {
        str = selectedBrand.models[row].name;
    }
    
    return str;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (component == 0) {
        selectedBrand = allBrands[row];
        selectedModel = selectedBrand.models.firstObject;
        
        [self.pickerView reloadComponent:1];
        [self.pickerView selectRow:0 inComponent:1 animated:NO];
        
    } else if (component == 1) {
        
        selectedModel = selectedBrand.models[row];
    }
    
    // update label text
    self.textLabel.text = [NSString stringWithFormat:@"Brand: %@\nModel: %@", selectedBrand.name, selectedModel.name];
}

@end
