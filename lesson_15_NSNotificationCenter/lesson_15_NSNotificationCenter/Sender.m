//
//  Sender.m
//  lesson_15_NSNotificationCenter
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Sender.h"

@implementation Sender

- (void)sendNotification {
    // отправка сообщение (без объекта и данных). такой вид сообщений применяется в случаях когда нужно просто уведомить о случившемся
    [[NSNotificationCenter defaultCenter] postNotificationName:kMessage1 object:nil];
}

- (void)sendNotificationWithData {
    // отправка сообщений с данными. даные должны быть представлены в виде словаря (NSDictionary)
    NSDictionary *data = @{@"key1":@"obj1",
                           @"key2":@"obj2"};
    [[NSNotificationCenter defaultCenter] postNotificationName:kMessage2 object:nil userInfo:data];
}

- (void)sendNotificationWithDataAndObject {
    // отправка сообщений  с данными и с тем объектов, кто это сообщение посылате
    NSDictionary *data = @{@"key1":@"obj1",
                           @"key2":@"obj2"};
    [[NSNotificationCenter defaultCenter] postNotificationName:kMessage3 object:self userInfo:data];
}

@end
