//
//  Resiever.m
//  lesson_15_NSNotificationCenter
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Resiever.h"

@implementation Resiever

// для реализовать получение уведомлений от NSNotificationCenter нужно выполнить три условия:
// 1. объявить и реализовать метод, который будет вызван в момент получения уведомления. такие методы имеют опрелеленную сигнатуру, а именно:
//  <имя_метода>:(NSNotification *)notification

// 2. подписаться на уведомления. когда делаем подписку указываем имя уведомления и имя метода, который вызовется в момент получения этого уведомления. подписываться лучше в методе инициализации объекта

// 3. отписаться от уведовления. это очень важно сделать!!! отписку можно сделать в методе -dealloc

- (void)receivedNotification1:(NSNotification *)notification {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%@", notification);
}

- (void)receivedNotification2:(NSNotification *)notification {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%@", notification);
}

- (void)receivedNotification3:(NSNotification *)notification {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    NSLog(@"%@", notification);
}

// в методе init выполним подписку на уведомления
- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification1:) name:kMessage1 object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification2:) name:kMessage2 object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification3:) name:kMessage3 object:nil];
    }
    return self;
}

// в методе dealloc выполним отподписку от уведомлений
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
