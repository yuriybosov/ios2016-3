//
//  Constants.h
//  lesson_15_NSNotificationCenter
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import <Foundation/Foundation.h>

// объявление строковых констант
static NSString * const kMessage1 = @"message1";
static NSString * const kMessage2 = @"message2";
static NSString * const kMessage3 = @"message3";

#endif /* Constants_h */
