//
//  main.m
//  lesson_15_NSNotificationCenter
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sender.h"
#import "Resiever.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Sender *sender = [Sender new];
        Resiever *resiever = [Resiever new];
        
        [sender sendNotification];
        [sender sendNotificationWithData];
        [sender sendNotificationWithDataAndObject];
    }
    return 0;
}
