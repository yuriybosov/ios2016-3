//
//  Sender.h
//  lesson_15_NSNotificationCenter
//
//  Created by Yurii Bosov on 1/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

// класс-отправитель сообщений
@interface Sender : NSObject

- (void)sendNotification;
- (void)sendNotificationWithData;
- (void)sendNotificationWithDataAndObject;

@end
