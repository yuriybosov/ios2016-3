//
//  AppDelegate.h
//  lesson_31_Edti_Cell
//
//  Created by Yurii Bosov on 3/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

