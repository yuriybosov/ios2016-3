//
//  ViewController.m
//  lesson_31_Edti_Cell
//
//  Created by Yurii Bosov on 3/21/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *dataSources;
    IBOutlet UITableView *myTableView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // init data sources
    dataSources = [NSMutableArray new];
    [dataSources addObject:@(arc4random_uniform(100))];
    [dataSources addObject:@(arc4random_uniform(100))];
    [dataSources addObject:@(arc4random_uniform(100))];
}

#pragma mark - Button Action

- (IBAction)addButtonClicked:(id)sender {
    // 1. create new item (NSNumber)
    NSNumber *num = @(arc4random_uniform(100));
    // 2. insert to array
    [dataSources addObject:num];
//    [dataSources insertObject:num atIndex:0];
    // 3. reload table view
    NSIndexPath *path = [NSIndexPath indexPathForRow:dataSources.count - 1 inSection:0];
    [myTableView insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationAutomatic];
    // scroll to visible
    [myTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionNone animated:YES];
}

- (IBAction)removeAllCellsButtonClicked:(id)sender {
    
    [dataSources removeAllObjects];
    
    NSIndexSet *set = [NSIndexSet indexSetWithIndex:0];
    [myTableView reloadSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)editButtonClicked:(id)sender{
    [myTableView setEditing:YES animated:YES];
}

- (IBAction)doneButtonClicked:(id)sender{
    [myTableView setEditing:NO animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", dataSources[indexPath.row]];
    return cell;
}

// реализовав этот метод в делегате мы получаем возможность редактировать tableviewcell
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // можно удалить ячейку по этому indexPath
        // есть два вариант удаление
//        // 1-й простой и не очень "красивый"
//        // 1.1 удалить элемент из массива данных (dataSources)
//        [dataSources removeObjectAtIndex:indexPath.row];
//        // 1.2 вызывать у таблицы методо reload
//        [myTableView reloadData];
        
        // 2-й способ (правельный)
        // 2.1 - удалить элемент из массива данных (dataSources)
        [dataSources removeObjectAtIndex:indexPath.row];
        // 2.2 - вызываем у таблицы метод удаления ячеек. метод принимает массив indexPath и тип анимации
        [myTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    id obj = [dataSources objectAtIndex:sourceIndexPath.row];
    [dataSources removeObjectAtIndex:sourceIndexPath.row];
    [dataSources insertObject:obj atIndex:destinationIndexPath.row];
}

#pragma mark - UITableViewDelegate

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Удалить";
}

@end
