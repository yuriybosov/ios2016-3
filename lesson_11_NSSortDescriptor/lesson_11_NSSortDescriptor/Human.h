//
//  Human.h
//  lesson_11_NSSortDescriptor
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Human : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) CGFloat height;

+ (instancetype)humanWithName:(NSString *)name
                          age:(NSUInteger)age
                       height:(CGFloat)height;

@end
