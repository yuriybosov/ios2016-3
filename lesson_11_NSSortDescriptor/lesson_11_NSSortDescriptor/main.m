//
//  main.m
//  lesson_11_NSSortDescriptor
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Human.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
    
        // Cортировать массив будем с помощью класса NSSortDescriptor или с помощью указание метода сортировки
        // B классе NSSortDescriptor будем задавать условие сортировки: по какому ключи и в порядку убывания или возрастания сортировать
        
        
        // 1. сотрировка массива строк
        // сортировать массив строк будем с использованием метода сортировки compare: , сортировка будет произведена в порядке возрастания (поменять это нельзя)
        NSArray *stringArray = @[@"f",@"d",@"t",@"a",@"v",@"b"];
        NSArray *sortedArray = [stringArray sortedArrayUsingSelector:@selector(compare:)];
        NSLog(@"sortedArray = %@", sortedArray);
        
        // 2. сортировка массива чисел
        // сортировать массив строк будем с использованием метода сортировки compare: , сортировка будет произведена в порядке возрастания (поменять это нельзя)
        NSArray *numberArray = @[@(10),@(-20),@(100.34),@(34),@(96)];
        sortedArray = [numberArray sortedArrayUsingSelector:@selector(compare:)];
        NSLog(@"sortedArray %@", sortedArray);
        
        // 3. сортировка пользовательских классов
        // Сортировать будем с ипользованием NSSortDescriptor, примеч можно использовать как один так и комбинацию из несольких NSSortDescriptor-ов
        NSArray *humansArray = @[[Human humanWithName:@"Yurii" age:31 height:1.65],
                                 [Human humanWithName:@"Yurii" age:25 height:1.80],
                                 [Human humanWithName:@"Alex" age:30 height:1.90],
                                 [Human humanWithName:@"Dimitrii" age:20 height:1.55]];

        // 3.1 - сортируем массив людей по имени. для этого нам понадобится один NSSortDescriptor
        NSSortDescriptor *sdByName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        sortedArray = [humansArray sortedArrayUsingDescriptors:@[sdByName]];
        NSLog(@"sortedArray by name\n%@", sortedArray);
        
        // 3.2 - сортируем массив людей по возрасту. для этого нам понадобится один NSSortDescriptor
        NSSortDescriptor *sdByAge = [NSSortDescriptor sortDescriptorWithKey:@"age" ascending:YES];
        sortedArray = [humansArray sortedArrayUsingDescriptors:@[sdByAge]];
        NSLog(@"sortedArray by age\n%@", sortedArray);
        
        // 3.3 - сортируем массив людей по росту. для этого нам понадобится один NSSortDescriptor
        NSSortDescriptor *sdByHeight = [NSSortDescriptor sortDescriptorWithKey:@"height" ascending:YES];
        sortedArray = [humansArray sortedArrayUsingDescriptors:@[sdByHeight]];
        NSLog(@"sortedArray by height\n%@", sortedArray);
        
        // 3.4. - сортируем массив людей по имени и по возрасту. для этого нам понадобится два NSSortDescriptor-а
        sortedArray = [humansArray sortedArrayUsingDescriptors:@[sdByName, sdByAge]];
        NSLog(@"sortedArray by name and by age\n%@", sortedArray);
        
        // 4 - сортировка массива чисел в обратном порядке
        NSSortDescriptor *sd1 = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO selector:@selector(compare:)];
        sortedArray = [numberArray sortedArrayUsingDescriptors:@[sd1]];
        NSLog(@"sortedArray %@", sortedArray);
        
        // 5 - сортировка массива строк в обратном порядке
        
    }
    return 0;
}



