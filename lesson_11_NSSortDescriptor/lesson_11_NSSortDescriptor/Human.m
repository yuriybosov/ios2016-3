//
//  Human.m
//  lesson_11_NSSortDescriptor
//
//  Created by Yurii Bosov on 12/25/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import "Human.h"

@implementation Human

+ (instancetype)humanWithName:(NSString *)name
                          age:(NSUInteger)age
                       height:(CGFloat)height {
    
    Human *human = [[Human alloc] init];
    human.name = name;
    human.age = age;
    human.height = height;
    
    return human;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %lu, %0.2f", _name, _age, _height];
}

@end
