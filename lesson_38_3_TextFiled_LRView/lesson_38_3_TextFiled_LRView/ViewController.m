//
//  ViewController.m
//  lesson_38_3_TextFiled_LRView
//
//  Created by Yurii Bosov on 4/11/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate> {
    
    IBOutlet UITextField *myTextField;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // зададим бг для поля ввода
    UIImage *bgImage = myTextField.background;
    myTextField.background = [bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(bgImage.size.height/2, bgImage.size.width/2, bgImage.size.height/2 - 1, bgImage.size.width/2 - 1)];
    
    // настраиваем левую вью у поля ввода (испльзуем ее для добавление отступа)
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, myTextField.frame.size.height)];
    myTextField.leftView = view;
    myTextField.leftViewMode = UITextFieldViewModeAlways;
    
    // настраиваем правую вью у поля ввода (испльзуем ее для отобраджения ошибки при валидации)
    UIImage *img = [UIImage imageNamed:@"errorIcon"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
    myTextField.rightView = imageView;
    myTextField.rightViewMode = UITextFieldViewModeNever;
}

#pragma mark - Actions

- (IBAction)sendButtonClicked:(id)sender{
    [self hideKeyboard:nil];
    if (myTextField.text.length == 0) {
        myTextField.rightViewMode = UITextFieldViewModeAlways;
    }
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    myTextField.rightViewMode = UITextFieldViewModeNever;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self sendButtonClicked:nil];
    return NO;
}

@end
