//
//  TabBarController.m
//  lesson_33_2_Tabbar_Custom_Item
//
//  Created by Yurii Bosov on 3/28/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "TabBarController.h"
#import "ViewController.h"

@interface TabBarController ()

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ViewController *vc1 = [ViewController new];
    ViewController *vc2 = [ViewController new];
    ViewController *vc3 = [ViewController new];
    ViewController *vc4 = [ViewController new];
    
    vc1.title = @"VC1";
    vc2.title = @"VC2";
    vc3.title = @"VC3";
    vc4.title = @"VC4";
    
    // создаем программно tabBarItem
    // 1. используем системный айтем
    vc1.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemFeatured tag:0];
    vc2.tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemContacts tag:0];
    // 1. используем кастом айтем
    vc3.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Trainings" image:[UIImage imageNamed:@"tab2"] tag:0];
    vc4.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Map" image:[UIImage imageNamed:@"tab3"] selectedImage:[UIImage imageNamed:@"tab3Activ"]];
    
    UINavigationController *nvc1 = [[UINavigationController alloc] initWithRootViewController:vc1];
    UINavigationController *nvc2 = [[UINavigationController alloc] initWithRootViewController:vc2];
    UINavigationController *nvc3 = [[UINavigationController alloc] initWithRootViewController:vc3];
    UINavigationController *nvc4 = [[UINavigationController alloc] initWithRootViewController:vc4];
    
    self.viewControllers = @[nvc1, nvc2, nvc3, nvc4];
    
    // меняем цвета на таббаре
    self.tabBar.tintColor = [UIColor redColor]; // цвет для выделеного элемента
    self.tabBar.barTintColor = [UIColor yellowColor]; // цвет для бекграунда
    self.tabBar.unselectedItemTintColor = [UIColor brownColor];
    
    UIImage *img = [TabBarController imageFromColor:[UIColor blackColor]];
    UIImage *resizeImg = [TabBarController resizeImage:img];
    
    self.tabBar.selectionIndicatorImage = resizeImg;
    self.tabBar.itemSpacing = 0;
}

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (UIImage *)resizeImage:(UIImage *)img {
    return [img resizableImageWithCapInsets:UIEdgeInsetsMake(img.size.height/2,
                                                             img.size.width/2,
                                                             img.size.height/2 - 1,
                                                             img.size.width/2 - 1)];
}

@end
