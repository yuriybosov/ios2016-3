//
//  AppDelegate.h
//  lesson_33_2_Tabbar_Custom_Item
//
//  Created by Yurii Bosov on 3/28/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

