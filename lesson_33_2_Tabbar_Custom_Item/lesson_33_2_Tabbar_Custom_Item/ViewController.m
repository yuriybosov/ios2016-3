//
//  ViewController.m
//  lesson_33_2_Tabbar_Custom_Item
//
//  Created by Yurii Bosov on 3/28/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor magentaColor];
}

@end
