//
//  AddViewController.m
//  lesson_27_UINavigationController
//
//  Created by Yurii Bosov on 3/7/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController ()

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Add New";
    
    // Back Button
    // 1. создадим программно navigation back button
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navMenuBack"] style:UIBarButtonItemStyleDone target:self action:@selector(backButtonClicked:)];
    // 2. добавим эту кнопку как левую кнопку у навигейшен бара
    self.navigationItem.leftBarButtonItem = backButton;
    
    // To Root Button
    // эту кнопку имеет смысл отображать, если кол-во контроллеров в стеке навигейшен контролера больше чем 2
    if (self.navigationController.viewControllers.count > 2) {
        UIBarButtonItem *toRootButton = [[UIBarButtonItem alloc] initWithTitle:@"Root" style:UIBarButtonItemStyleDone target:self action:@selector(toRootButtonClicked:)];
        self.navigationItem.rightBarButtonItem = toRootButton;
    }
}

#pragma mark - Button Actions

- (void)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)toRootButtonClicked:(id)sender {
    // нужно выполнить переход в самый корневой экран (в самый первый экран навигейшен контроллера)
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
