//
//  ListViewController.m
//  lesson_27_UINavigationController
//
//  Created by Yurii Bosov on 3/7/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ListViewController.h"
#import "AddViewController.h"

@interface ListViewController () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *dataSources; // массив данных, в нашем случае - массив строк (имен студентов)
}

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"User List";
    dataSources = @[@"Виталий",@"Георгий",@"Александр",@"Наталия",@"Константин",@"Александр"];
}

#pragma mark - Button Actions

- (IBAction)addButtonClicked:(id)sender {
    
    // 1. создаем ViewController
    AddViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"AddViewController"];
    // 2. выполним отображение нового контроллера, для этого воспользуемся методом pushViewController:animated:
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)searchButtonClicked:(id)sender {
    // выполним segue который есть в сторибоарде. для этого нам нужно указать какой именно segue, т.е. указать его id
    // что бы вызвать переход (segue) воспользуемся вот таким вот методом performSegueWithIdentifier:sender:
    [self performSegueWithIdentifier:@"toSearchController" sender:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    cell.textLabel.text = dataSources[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

@end
