//
//  SerachViewController.m
//  lesson_27_UINavigationController
//
//  Created by Yurii Bosov on 3/7/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "SerachViewController.h"

@interface SerachViewController ()

@end

@implementation SerachViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Search";
}

#pragma mark - Button Actions

- (IBAction)backButtonClicked:(id)sender {
    // если нужно выполнить back с текущего экрана, то нужно выполнить данный метод popViewControllerAnimated:
    // данный метод есть в navigationController-е
    // если текущий экран находится в стеке UINavigationController-а, то у него всегда будет не nil свойство, которое называется .navigationController
    [self.navigationController popViewControllerAnimated:YES];
}

@end
