//
//  main.m
//  lesson_13_NO_ARC
//
//  Created by Yurii Bosov on 1/15/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyClass.h"
#import "MyClass2.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        // при создании объекта у него счетчик ссылок равен 1
        MyClass *obj = [[MyClass alloc] init];
        NSLog(@"retainCount %lu", obj.retainCount);
        
        // увеличить счетчик ссылок можно с помощью метода retain
        [obj retain];
        NSLog(@"retainCount %lu", obj.retainCount);
        
        // удаление объекта будет лишь в том случае, если счетчик ссылок будет равен 0
        // уменьшить счетчик ссылок на еденицу можно с помощью вызова метода realese
        [obj release];
        [obj release];
        
        ////////////////////////////
        MyClass2 *obj2 = [[MyClass2 alloc] init];
        
        // между init и release можно работать с этим объектом
        
        [obj2 release];
        
        // создадим авторелизный объект (это такой объект, у которого метод realese будет вызван отложено (т.е. попозже)
        MyClass2 *obj3 = [[[MyClass2 alloc] init] autorelease];
        NSLog(@"obj3 retainCount = %lu", [obj3 retainCount]);

        // классы коллекции (массив, словрь, set) при добавлении в них объекта увеличивают счетчик ссылок на 1 этому объекту ("ретейнят объект") 
        
        NSMutableArray *arra1 = [[[NSMutableArray alloc] init] autorelease];
        [arra1 addObject:obj3];
        
        NSMutableArray *arra2 = [NSMutableArray array];
        [arra2 addObject:obj3];
        NSLog(@"obj3 retainCount = %lu", [obj3 retainCount]);
    }
    return 0;
}
