//
//  NoteEditController.m
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/5/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import "NoteEditController.h"

@interface NoteEditController () <UITextFieldDelegate, UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UITextField *titleField;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;

@end

@implementation NoteEditController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.note != nil) {
        self.title = @"Edit note";
        self.titleField.text = self.note.title;
        self.textView.text = self.note.text;
    } else {
        self.title = @"Create note";
        self.deleteButton.hidden = YES;
    }
}

#pragma mark - Actions

- (IBAction)saveButtonClicekd:(id)sender{
    NSString *title = self.titleField.text;
    NSString *text = self.textView.text;
    
    if (title.length > 0) {
        
        if (self.note) {
            // редактируем и сохраняем существующую запись
            self.note.title = title;
            self.note.text = text;
            self.note.modifyDate = [NSDate date];
        } else {
            // создаем новую запись
            self.note = [[Note alloc] initWithContext:[DataManager sharedInstance].container.viewContext];
            self.note.title = title;
            self.note.text = text;
            self.note.createDate = [NSDate date];
            self.note.modifyDate = self.note.createDate;
        }
        
        [[DataManager sharedInstance] save];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)deleteButtonClicked:(id)sender {
    [[DataManager sharedInstance].container.viewContext deleteObject:self.note];
    [[DataManager sharedInstance] save];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate

#pragma mark - UITextViewDelegate

@end
