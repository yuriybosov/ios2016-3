//
//  NoteEditController.h
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/5/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"

@interface NoteEditController : UIViewController

@property (nonatomic, strong) Note *note;

@end
