//
//  NoteDetailedController.m
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import "NoteDetailedController.h"
#import "NoteEditController.h"

@interface NoteDetailedController ()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;

@end

@implementation NoteDetailedController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"View Note";
    self.navigationController.navigationBar.prefersLargeTitles = YES;
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAutomatic;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.titleLabel.text = self.note.title;
    self.textLabel.text = self.note.text;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[NoteEditController class]]){
        
        NoteEditController *controller = segue.destinationViewController;
        
        controller.note = self.note;
    }
}

@end
