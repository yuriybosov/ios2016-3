//
//  NoteListController.m
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import "NoteListController.h"
#import "DataManager.h"
#import "NoteDetailedController.h"

@interface NoteListController () <UITableViewDataSource, UITableViewDelegate> {
    UISearchController *searchController;
    
    NSArray *dataSource;
}

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation NoteListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Notes";
    
    // init dataSource, то есть получить из базы данных все записи(Note)
    [self fetchData];
    
    //
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.obscuresBackgroundDuringPresentation = NO;
    self.navigationItem.searchController = searchController;
    
    //
    searchController.searchBar.scopeButtonTitles = @[@"Date create", @"Date modify"];
    
    //

    self.navigationController.navigationBar.prefersLargeTitles = YES;
    self.navigationItem.largeTitleDisplayMode = UINavigationItemLargeTitleDisplayModeAutomatic;
//    self.navigationItem.hidesSearchBarWhenScrolling = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fetchData];
}

- (void)fetchData {
    // 1. выполнить запрос в бд
    // 2. результат записать в массив dataSource
    dataSource = [[DataManager sharedInstance] fetchAllNotes];
    
    // 3. вызвать обновление таблицы reloadData
    [self.tableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // если переход был инициирован нажатием по ячейке, то мы будем открывать экран просмотра записи, следовательно нужно передать запись в экран просмотра записи
    if ([sender isKindOfClass:[UITableViewCell class]] &&
        [segue.destinationViewController isKindOfClass:[NoteDetailedController class]]) {
        
        NSIndexPath *path = [self.tableView indexPathForCell:sender];
        
        Note *note = dataSource[path.row];
        
        NoteDetailedController *controller = segue.destinationViewController;
        
        controller.note = note;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellId" forIndexPath:indexPath];
    
    Note *note = dataSource[indexPath.row];
    
    cell.textLabel.text = note.title;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

@end
