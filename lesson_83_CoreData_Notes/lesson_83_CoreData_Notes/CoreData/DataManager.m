//
//  DataManager.m
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

@synthesize container = _container;

+ (instancetype)sharedInstance {
    static DataManager *manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [DataManager new];
    });
    
    return manager;
}

- (void)save {
    NSError *error = nil;
    if ([self.container.viewContext hasChanges] &&
        ![self.container.viewContext save:&error]) {
        NSLog(@"Context Save With Error %@", error);
    }
}

- (NSPersistentContainer *)container {
    
    @synchronized (self) {
        if (_container == nil) {
            _container = [NSPersistentContainer persistentContainerWithName:@"DataBase"];
            [_container loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription * storeDescription, NSError * error) {
                
                if (error) {
                    NSLog(@"Load Persistent Stores With Error %@", error);
                    abort();
                }
            }];
        }
    }
    
    return _container;
}

#pragma mark - Fetch Data

- (NSArray *)fetchAllNotes {
    NSFetchRequest *request = [Note fetchRequest];
    
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"modifyDate" ascending:NO];
    request.sortDescriptors = @[sd];
    
    NSArray *result = [self.container.viewContext executeFetchRequest:request error:nil];
    
    return result;
}

@end
