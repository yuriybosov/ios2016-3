//
//  Note+CoreDataProperties.m
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

#import "Note+CoreDataProperties.h"

@implementation Note (CoreDataProperties)

+ (NSFetchRequest<Note *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Note"];
}

@dynamic title;
@dynamic text;
@dynamic createDate;
@dynamic modifyDate;

@end
