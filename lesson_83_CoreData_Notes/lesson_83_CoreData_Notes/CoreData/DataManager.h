//
//  DataManager.h
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Note+CoreDataClass.h"

@interface DataManager : NSObject

@property (nonatomic, readonly) NSPersistentContainer *container;

+ (instancetype)sharedInstance;

- (void)save;

// fetch all notes
- (NSArray *)fetchAllNotes;

@end
