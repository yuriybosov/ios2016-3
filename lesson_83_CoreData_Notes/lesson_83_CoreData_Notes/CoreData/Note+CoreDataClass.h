//
//  Note+CoreDataClass.h
//  lesson_83_CoreData_Notes
//
//  Created by Yurii Bosov on 11/2/17.
//  Copyright © 2017 ios. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Note : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Note+CoreDataProperties.h"
