//
//  ViewController.m
//  lesson_19_UIView_IBAction
//
//  Created by Yurii Bosov on 2/7/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIButton *btn;
    
    IBOutlet UIView *rect0;
    IBOutlet UIView *rect;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)btnDidClicked {
    // по нажатию на кпопку смещать rect на 20 по х и на 20 по y
    
    // 1 способ (используя frame)
//    CGRect frame = rect.frame;
//    frame.origin.x += 20;
//    frame.origin.y += 20;
//    
//    rect.frame = frame;
    
    // 2 способ (используя center)
//    CGPoint center = rect.center;
//    center.x += 20;
//    center.y += 20;
//    
//    rect.center = center;
    
    // 3 способ (используя center, но записываем все в одну строку)
    rect.center = CGPointMake(rect.center.x + 20,
                              rect.center.y + 20);
    
}

- (IBAction)gotoCenterButtonClicked:(id)sender {
    rect.center = rect0.center;
//    rect.center = CGPointMake(rect0.frame.size.width/2,
//                              rect0.frame.size.height/2);
}

@end
