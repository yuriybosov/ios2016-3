//
//  ViewController.m
//  lesson_64_Shadow
//
//  Created by Yurii Bosov on 7/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UILabel *label;
    IBOutlet UIView *view1;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // тень для текста
    label.shadowColor = [[UIColor redColor] colorWithAlphaComponent:0.5];
    label.shadowOffset = CGSizeMake(-2, -2);
    
    // тень для view1
    view1.layer.shadowColor = [UIColor blackColor].CGColor;
    view1.layer.shadowOpacity = 0.35;
    view1.layer.shadowOffset = CGSizeMake(3, 3);
    view1.layer.shadowRadius = 1;
    
    view1.layer.cornerRadius = 10;
}

@end
