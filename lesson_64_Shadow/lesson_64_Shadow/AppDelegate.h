//
//  AppDelegate.h
//  lesson_64_Shadow
//
//  Created by Yurii Bosov on 7/18/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

