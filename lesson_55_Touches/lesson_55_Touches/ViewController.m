//
//  ViewController.m
//  lesson_55_Touches
//
//  Created by Yurii Bosov on 6/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    IBOutlet UIView *rect;
    
    BOOL isMoving;
    CGPoint delta;
}

@end

@implementation ViewController

#pragma mark - Touches

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = touches.anyObject;
    CGPoint point = [touch locationInView:self.view];
    
    // 1. проверим что тач попал в область кадратика, и если это так, то разрешаем передвигать кадрат
    if (CGRectContainsPoint(rect.frame, point)) {
        isMoving = YES;
        delta = CGPointMake(rect.center.x - point.x,
                            rect.center.y - point.y);
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = touches.anyObject;
    CGPoint point = [touch locationInView:self.view];
    // если первый тач был в границе квадратика, то мы считаем что мы можем его передвигать, соответственно обновим позиция квадрата если isMoving = YES
    if (isMoving) {
        
        CGPoint newCenter = CGPointMake(point.x + delta.x,
                                        point.y + delta.y);
        
        // left
        if (newCenter.x - rect.frame.size.width/2 < 0) {
            newCenter.x = rect.frame.size.width/2;
        }
        // top
        if (newCenter.y - rect.frame.size.height/2 < 0) {
            newCenter.y = rect.frame.size.height/2;
        }
        // right
        if (newCenter.x + rect.frame.size.width/2 > self.view.frame.size.width) {
            newCenter.x = self.view.frame.size.width - rect.frame.size.width/2;
        }
        
        // bottom
        if (newCenter.y + rect.frame.size.height/2 > self.view.frame.size.height) {
            newCenter.y = self.view.frame.size.height - rect.frame.size.height/2;
        }
        
        rect.center = newCenter;
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    isMoving = NO;
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    isMoving = NO;
}

@end
