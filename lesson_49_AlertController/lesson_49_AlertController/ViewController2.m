//
//  ViewController2.m
//  lesson_49_AlertController
//
//  Created by Yurii Bosov on 6/11/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController2.h"

@implementation ViewController2

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.title = @"Show Action Sheet";
    }
    return self;
}

#pragma mark - Button Actions

- (IBAction)buttn1Clicked:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"Message" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // добавляем кнопки (actons)
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK");
    }];
    [alert addAction:action1];
    
    // для корректного отображения action sheet для ipad
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        alert.modalPresentationStyle = UIModalPresentationPopover;
        alert.popoverPresentationController.sourceView = sender;
        alert.popoverPresentationController.sourceRect = [sender bounds];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)buttn2Clicked:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"Message" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // добавляем кнопки (actons)
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK");
    }];
    [alert addAction:action1];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked Cancel");
    }];
    [alert addAction:action2];
    
    // для корректного отображения action sheet для ipad
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        alert.modalPresentationStyle = UIModalPresentationPopover;
        alert.popoverPresentationController.sourceView = sender;
        alert.popoverPresentationController.sourceRect = [sender bounds];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)buttn3Clicked:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"Message" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // добавляем кнопки (actons)
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK");
    }];
    [alert addAction:action1];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked Cancel");
    }];
    [alert addAction:action2];
    
    // для корректного отображения action sheet для ipad
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        alert.modalPresentationStyle = UIModalPresentationPopover;
        alert.popoverPresentationController.sourceView = sender;
        alert.popoverPresentationController.sourceRect = [sender bounds];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)buttn4Clicked:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"Message" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // добавляем кнопки (actons)
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"OK1" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK1");
    }];
    [alert addAction:action1];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"OK2" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK2");
    }];
    [alert addAction:action2];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"OK3" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK3");
    }];
    [alert addAction:action3];
    
    // для корректного отображения action sheet для ipad
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        alert.modalPresentationStyle = UIModalPresentationPopover;
        alert.popoverPresentationController.sourceView = sender;
        alert.popoverPresentationController.sourceRect = [sender bounds];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)buttn5Clicked:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Title" message:@"Message" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // добавляем кнопки (actons)
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"OK1" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK1");
    }];
    [alert addAction:action1];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"OK2" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK2");
    }];
    [alert addAction:action2];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"OK3" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"clicked OK3");
    }];
    [alert addAction:action3];
    
    // для корректного отображения action sheet для ipad
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        alert.modalPresentationStyle = UIModalPresentationPopover;
        alert.popoverPresentationController.sourceView = sender;
        alert.popoverPresentationController.sourceRect = [sender bounds];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
