//
//  main.m
//  lesson_2_1_Switch
//
//  Created by Yurii Bosov on 11/22/16.
//  Copyright © 2016 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum : NSInteger {
    GenderNone,         // 0
    GenderMale,         // 1
    GenderFemale        // 2
}Gender;


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        NSInteger i = 2;
        
        switch (i) {
            case 1:     // if (i == 1)
                NSLog(@"код который выполнится при i == 1");
                break;
                
            case 2:     // if (i == 2)
                NSLog(@"код который выполнится при i == 2");
                break;
                
            case 3:
            case 4:
                NSLog(@"код который выполнится при i == 3 или при i == 4");
                break;
                
            case 10:
                NSLog(@"НАКОНЕЦТО ВЫ ПОНЯЛИ!!!1!!");
                break;
                
            default:
                NSLog(@"код который выполнится при невыполении ни одного из кейсов");
                break;
        }
        
        // пример c enum
        Gender x = GenderMale;
        
        switch (x) {
            case GenderMale:
                NSLog(@"пол мужской");
                break;
                
            case GenderFemale:
                NSLog(@"пол женский");
                break;
                
            case GenderNone:
                NSLog(@"пол не был указан");
                break;
        }
    }
    return 0;
}
