//
//  main.m
//  lesson_10_NSMutableArray
//
//  Created by Yurii Bosov on 12/20/16.
//  Copyright © 2016 ios. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        //
        // 1. создание объекта NSMutableArray
        // 1.1 через метод new
        NSMutableArray *array1 = [[NSMutableArray alloc] init];
        
        // 1.2 через статичесикй метод array
        array1 = [NSMutableArray array];
        
        // 1.3 создание и добавление элементов в массив
        array1 = [NSMutableArray arrayWithObjects:@"1",@"2",@"3", nil];
        
        // 1.4. создание и добавление одного элемента в массив
        array1 = [NSMutableArray arrayWithObject:@"6"];
        
        // 1.5. создание массива и добавление в него элементов из другого массива
        array1 = [NSMutableArray arrayWithArray:@[@"qw",@"sd",@"xc"]];
        
        NSLog(@"array1 = %@", array1);
        
        // 2. узнать кол-во элементов в массиве (длину массива)
        NSUInteger count = array1.count;
        NSLog(@"кол-во элементов равно = %lu", count);
        
        // 3. получить элемент массива по индексу - см. класс NSArray
        
        // 4. добавить элемент в массив c помощью метода addObject:
        // элемент добавляется в конец массива
        [array1 addObject:@"!!!"];
        [array1 addObject:@"???"];
        
        NSLog(@"%@", array1);
        
        // 5. вставить элемент в массив по определенному индексу
        // вставим в начало массива
        [array1 insertObject:@"###" atIndex:0];
        NSLog(@"%@", array1);
        
        // вставить в середину массива
        NSUInteger index = 3;
        if  (index <= array1.count) {
            [array1 insertObject:@"$$$" atIndex:index];
            NSLog(@"%@", array1);
        }
        
        // 6. вставка в массив элементов другого массива, указав индекс (!!! индекс требует проверки выхода за пределы массива)
        NSArray *array = @[@(111),@(222),@(333)];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, array.count)];
        
        [array1 insertObjects:array
                    atIndexes:indexSet];
        
        NSLog(@"%@", array1);
        
        // 7. удаление элементов в массиве
        // 7.1. удалить последний элемент
        [array1 removeLastObject];
        NSLog(@"%@", array1);
        
//         7.2. удалить элемент по индексу (!!! индекс требует проверки на выход за пределы массива)
        index = 3;
        [array1 removeObjectAtIndex:index];
        NSLog(@"%@", array1);
        
        // 7.3. удалить все элементы массива (очистить массив)
        [array1 removeAllObjects];
        NSLog(@"%@", array1);
    }
    return 0;
}
