//
//  ViewController.m
//  lesson_38_UITextField
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate> {
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfPassword;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // при показе экрана сделаем поле ввода email активным, т.е. покажем для него клавиатуру
    [tfEmail becomeFirstResponder];
}

#pragma mark - Actions

- (IBAction)hideKeyboard {
    // прячем клавиатуру c помощью метода endEditing:
    [self.view endEditing:YES];
}

- (IBAction)loginButtonClicked {
    
    // 1. спрятоть клаву
    [self hideKeyboard];
    
    BOOL validate = YES;
    
    // 2. провалидировать поля ввода
    // 2.1 валидируем поле ввода email
    if (tfEmail.text.length == 0) {
        validate = NO;
        tfEmail.backgroundColor = [UIColor redColor];
    }
    // 2.2 валидируем поле ввода password
    if (tfPassword.text.length == 0) {
        validate = NO;
        tfPassword.backgroundColor = [UIColor redColor];
    }
    
    // если валидация успешна - перейдем на основной экран приложения
    if (validate) {
        UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainNavigation"];
        // делаем подмену rootviewcontroller для окна приложение.
        // для этого берем приложение использую метод [UIApplication sharedApplication]. далее у приложение есть его текущее онко - это свойство keyWindow. у окна есть текущий отображаемый экран - это свойство rootViewController. Заменяем этот rootViewController на новый созданый NavigationController
        [UIApplication sharedApplication].keyWindow.rootViewController = nc;
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.backgroundColor = [UIColor whiteColor];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == tfEmail) {
        // becomeFirstResponder - данный метод делает поле ввода активным
        [tfPassword becomeFirstResponder];
    } else {
        // resignFirstResponder - данный метод делает поле ввода не активным, т.е. прячет клавиатуру
        [tfPassword resignFirstResponder];
        
        // в этом случае выполним теже действия что и при нажатии на кнопку авторизации
        [self loginButtonClicked];
    }
    
    return NO;
}

@end
