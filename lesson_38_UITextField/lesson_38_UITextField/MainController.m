//
//  MainController.m
//  lesson_38_UITextField
//
//  Created by Yurii Bosov on 4/9/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "MainController.h"

@interface MainController ()

@end

@implementation MainController

- (IBAction)logoutButtonClicked:(id)sender {
    UIViewController *vc = [self.storyboard instantiateInitialViewController];
    [UIApplication sharedApplication].keyWindow.rootViewController = vc;
}

@end
