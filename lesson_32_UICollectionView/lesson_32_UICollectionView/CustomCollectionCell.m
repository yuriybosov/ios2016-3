//
//  CustomCollectionCell.m
//  lesson_32_UICollectionView
//
//  Created by Yurii Bosov on 3/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "CustomCollectionCell.h"

@implementation CustomCollectionCell

- (void)setSelected:(BOOL)selected {
    self.selectedBackgroundView.frame = self.bounds;
    self.selectedBackgroundView.alpha = selected ? 1 : 0;
}

@end
