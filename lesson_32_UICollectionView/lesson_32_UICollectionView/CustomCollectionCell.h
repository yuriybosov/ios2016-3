//
//  CustomCollectionCell.h
//  lesson_32_UICollectionView
//
//  Created by Yurii Bosov on 3/26/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *textLabel;

@end
