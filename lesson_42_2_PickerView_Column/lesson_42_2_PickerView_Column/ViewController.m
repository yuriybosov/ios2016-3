//
//  ViewController.m
//  lesson_42_2_PickerView_Column
//
//  Created by Yurii Bosov on 5/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIPickerViewDataSource, UIPickerViewDelegate> {
    
    NSArray *dataSource1;
    NSArray *dataSource2;
}

@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataSource1 = @[@"q",@"w",@"e",@"r",@"t"];
    dataSource2 = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7"];
    
    [self.pickerView reloadAllComponents];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    NSInteger number;
    
    if (component == 0) {
        number = dataSource1.count;
    } else {
        number = dataSource2.count;
    }
    
    return number;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *str = nil;
    
    if (component == 0) {
        str = dataSource1[row];
    } else {
        str = dataSource2[row];
    }
    
    return str;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    NSString *str = nil;
    
    if (component == 0) {
        str = dataSource1[row];
    } else {
        str = dataSource2[row];
    }
    
    NSLog(@"%@", str);
}

@end
