//
//  AppDelegate.h
//  lesson_42_2_PickerView_Column
//
//  Created by Yurii Bosov on 5/16/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

