//
//  main.m
//  lesson_14_PrinterProtocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Printer.h"
#import "OfficeManager.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Printer *canonLBP2900 = [[Printer alloc] init];
        canonLBP2900.currentPageCount = 10;
        canonLBP2900.name = @"LBP 2900";
        
        OfficeManager *officeManager = [OfficeManager new];
        canonLBP2900.delegate = officeManager;
        
        [canonLBP2900 startPrintWithQuantity:7];
        [canonLBP2900 startPrintWithQuantity:5];
    }
    return 0;
}
