//
//  OfficeManager.h
//  lesson_14_PrinterProtocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Printer.h"

@interface OfficeManager : NSObject <PrinterProtocol>

@end
