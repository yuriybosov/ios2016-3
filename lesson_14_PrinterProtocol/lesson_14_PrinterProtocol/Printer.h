//
//  Printer.h
//  lesson_14_PrinterProtocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Printer;

@protocol PrinterProtocol <NSObject>

- (void)printerStart:(Printer *)printer;

- (void)printerComplited:(Printer *)printer;

- (void)printer:(Printer *)printer
       tapePage:(NSUInteger)page
      fromPages:(NSUInteger)totalPages;

- (void)printerFailed:(Printer *)printer;

@end


@interface Printer : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSUInteger currentPageCount;
@property (nonatomic, weak) id<PrinterProtocol> delegate;

- (void)startPrintWithQuantity:(NSUInteger)quantity;
- (void)continuePrint;

@end
