//
//  OfficeManager.m
//  lesson_14_PrinterProtocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "OfficeManager.h"

@implementation OfficeManager

#pragma mark - PrinterProtocol

- (void)printerComplited:(Printer *)printer {
    NSLog(@"принтер %@ завершил печать, ура ура ура!!!", printer.name);
}

- (void)printerFailed:(Printer *)printer {
    NSLog(@"принтер %@ сломался...", printer.name);
    
    printer.currentPageCount = 10;
    [printer continuePrint];
}

- (void)printer:(Printer *)printer
       tapePage:(NSUInteger)page
      fromPages:(NSUInteger)totalPages {
    
    NSLog(@"печать %lu из %lu", page, totalPages);
}

@end
