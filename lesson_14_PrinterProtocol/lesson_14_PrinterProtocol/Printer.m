//
//  Printer.m
//  lesson_14_PrinterProtocol
//
//  Created by Yurii Bosov on 1/17/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "Printer.h"

@implementation Printer

- (void)startPrintWithQuantity:(NSUInteger)quantity {
    
    for (NSUInteger i = 0; i < quantity; i++) {
        if (self.currentPageCount > 0) {
            // могу печатать
            // печатаю....

            // вызываем делегата метод, что напечатали такой то лист из такого то кол-ва листов
            [self.delegate printer:self
                          tapePage:i + 1
                         fromPages:quantity];
            
            // печать одного листа выполнена, значит общее кол-во листов уменьшаем на еденицу
            self.currentPageCount--;
        } else{
            // закончилась бумага
            // вызываем метод делегата, что принтер сфейлил (закончилась бумага)
            [self.delegate printerFailed:self];
            return;
        }
    }
    
    // вызываем метод делегата, что печать завершена
    [self.delegate printerComplited:self];
}

- (void)continuePrint {
#warning TODO in HOME
    // после того, как закончилась бумага, делегат ее добавил, то нужно возобновить печать!!!
}

@end
