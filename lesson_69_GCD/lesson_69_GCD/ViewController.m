//
//  ViewController.m
//  lesson_69_GCD
//
//  Created by Yurii Bosov on 8/20/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

#pragma mark - Test Method

- (void)testMethod{
    
    NSTimeInterval start = [NSDate date].timeIntervalSince1970;
    NSLog(@"START %0.2f", start);
    
    NSMutableArray *array = [NSMutableArray new];
    for (NSInteger i = 0; i < 50000000; i++) {
        [array addObject:@(1)];
    }
    
    NSTimeInterval end = [NSDate date].timeIntervalSince1970;
    NSLog(@"END %0.2f", end);
}

- (void)testMethodComplited {
    self.view.backgroundColor = [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
}

#pragma mark - Actions

- (IBAction)buttonClicked1:(id)sender {
    // выполнить метод в главном потоке
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self testMethod];
        [self testMethodComplited];
    });
}

- (IBAction)buttonClicked2:(id)sender {
    
    // выполнить метод в бекграунд потоке
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self testMethod];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testMethodComplited];
        });
    });
}

- (IBAction)buttonClicked3:(id)sender {
    // выполнить метод один раз
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSLog(@"dispatch once");
        [self testMethodComplited];
    });
}

- (IBAction)buttonClicked4:(id)sender {
    // выполнить метод в кастомной очереди
    // типы очереди
    // DISPATCH_QUEUE_SERIAL - последовательное выполнение
    // DISPATCH_QUEUE_CONCURRENT - паралельное выполнение
    
    // 1. создание очереди
    
    static dispatch_queue_t q;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        q = dispatch_queue_create("com.appname.queuname", DISPATCH_QUEUE_SERIAL);
    });
    
    // 2. добавление задач в очередь (5 раз)
    dispatch_async(q, ^{
        [self testMethod];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testMethodComplited];
        });
    });
    
    dispatch_async(q, ^{
        [self testMethod];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testMethodComplited];
        });
    });
    
    dispatch_async(q, ^{
        [self testMethod];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testMethodComplited];
        });
    });
    
    dispatch_async(q, ^{
        [self testMethod];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testMethodComplited];
        });
    });
    
    dispatch_async(q, ^{
        [self testMethod];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testMethodComplited];
        });
    });
}

- (IBAction)buttonClicked5:(id)sender {
    // выполнить метод спустя какое то время
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self testMethod];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self testMethodComplited];
        });
    });
}

@end
