//
//  ViewController.m
//  lesson_26_CustomCells
//
//  Created by Yurii Bosov on 3/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "NewsCell.h"
#import "PhotoCell.h"
#import "NewsModel.h"
#import "PhotoModel.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UITableView *myTableView;
    
    NSMutableArray *newsArray;
    NSMutableArray *photosArray;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    newsArray = [NSMutableArray new];
    photosArray = [NSMutableArray new];
    
    // создаем несколько моделей новостей
    NewsModel *news1 = [NewsModel new];
    news1.title = @"Обменом валют занялись более дюжины компаний с малоизвестными владельцами";
    news1.text = @"Количество небанковских обменных пунктов в Украине растет. В прошлом году Национальный банк выдал лицензии на обмен валют сразу 17 небанковским компаниям. До 2016-го таким правом, помимо банков, обладали только 11 финансовых учреждений. Сеть небанковских обменников к концу прошлого года достигла 3205 точек.";
    news1.logoName = @"newsLogo1";
    [newsArray addObject:news1];
    
    NewsModel *news2 = [NewsModel new];
    news2.title = @"Украинцы могут распоряжаться деньгами, заработанными за рубежом, без ведома Нацбанка";
    news2.text = @"Национальный банк упростил зарубежные операции для украинцев, которые зарабатывают в других странах. Физлица смогут открывать банковские счета за границей и размещать на них валюту, полученную за пределами страны, без индивидуальных лицензий НБУ";
    news2.logoName = @"newsLogo2";
    [newsArray addObject:news2];
    
    // создаем несколько моделей фотографий
    PhotoModel *photoModel1 = [PhotoModel new];
    photoModel1.photoName = @"photo1";
    photoModel1.title = @"Днепр вечерний какой то красивый вид";
    [photosArray addObject:photoModel1];
    
    PhotoModel *photoModel2 = [PhotoModel new];
    photoModel2.photoName = @"photo2";
    photoModel2.title = @"Днепр вечерний так себе вид";
    [photosArray addObject:photoModel2];
    
    PhotoModel *photoModel3 = [PhotoModel new];
    photoModel3.photoName = @"photo3";
    photoModel3.title = @"Днепр вечерний не фонтан со сойдет";
    [photosArray addObject:photoModel3];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger number = 0;
    if (section == 0)
        number = newsArray.count;
    else
        number = photosArray.count;
    
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0) { // создаем новости
        
        NewsCell *newsCell = [tableView dequeueReusableCellWithIdentifier:@"NewsCell"];
        NewsModel *model = newsArray[indexPath.row];
        
        newsCell.newsTitle.text = model.title;
        newsCell.newsText.text = model.text;
        newsCell.logoView.image = [UIImage imageNamed:model.logoName];
        
        cell = newsCell;
        
    } else { // создаем фото
        PhotoCell *photoCell = [tableView dequeueReusableCellWithIdentifier:@"PhotoCell"];
        PhotoModel *model = photosArray[indexPath.row];
        
        photoCell.photoTitle.text = model.title;
        photoCell.photoView.image = [UIImage imageNamed:model.photoName];
        
        cell = photoCell;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat H = 0;
    if (indexPath.section == 0) {
        H = 100;
    } else {
        H = 250;
    }
    return H;
}

@end
