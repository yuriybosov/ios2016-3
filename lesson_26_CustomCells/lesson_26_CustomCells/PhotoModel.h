//
//  PhotoModel.h
//  lesson_26_CustomCells
//
//  Created by Yurii Bosov on 3/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *photoName;

@end
