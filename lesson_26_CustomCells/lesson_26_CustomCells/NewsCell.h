//
//  NewsCell.h
//  lesson_26_CustomCells
//
//  Created by Yurii Bosov on 3/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *logoView;
@property (nonatomic, weak) IBOutlet UILabel *newsTitle;
@property (nonatomic, weak) IBOutlet UILabel *newsText;

@end
