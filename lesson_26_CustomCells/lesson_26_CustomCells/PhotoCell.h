//
//  PhotoCell.h
//  lesson_26_CustomCells
//
//  Created by Yurii Bosov on 3/5/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *photoView;
@property (nonatomic, weak) IBOutlet UILabel *photoTitle;

@end
