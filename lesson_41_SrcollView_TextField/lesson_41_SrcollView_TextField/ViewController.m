//
//  ViewController.m
//  lesson_41_SrcollView_TextField
//
//  Created by Yurii Bosov on 4/25/17.
//  Copyright © 2017 Yurii Bosov. All rights reserved.
//

#import "ViewController.h"
#import "KeyboardToolbar.h"

@interface ViewController () <UITextFieldDelegate> {
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfPhone;
    IBOutlet UITextField *tfPassword;
    
    KeyboardToolbar *toolbar;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    toolbar = [KeyboardToolbar create];
    toolbar.textFields = @[tfEmail, tfPhone, tfPassword];
}

#pragma mark - Actions

- (IBAction)singUpButtonCliecked:(id)sender {
    [self hideKeyboard];
}

- (IBAction)hideKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    BOOL result = YES;
    
    // напишем проверку для поля ввода номера телефона. суть проверки - можно вводить только цифры.
    if (textField == tfPhone &&
        string.length > 0) {
        
        NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:@"^[0-9]+$" options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSUInteger count = [regExp numberOfMatchesInString:string options:0 range:NSMakeRange(0, string.length)];
        
        result = (count == 1);
    }
    
    return result;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    toolbar.currentTextField = textField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [toolbar returnButtonClicked];
    return NO;
}

@end
